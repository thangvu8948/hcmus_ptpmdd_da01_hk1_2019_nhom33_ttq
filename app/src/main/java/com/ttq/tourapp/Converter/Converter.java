package com.ttq.tourapp.Converter;

import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.databinding.InverseMethod;

import java.sql.Time;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Converter {
    @InverseMethod("stringToInt")
    public static String intToString(int value){
        return String.valueOf(value);
    }

    public static int stringToInt(String value){
        if(TextUtils.isEmpty(value))
            return 0;
        return Integer.parseInt(value);
    }

    @InverseMethod("stringToLong")
    public static String longToString(long value){
        return String.valueOf(value);
    }
    public static long stringToLong(String value){
        if(TextUtils.isEmpty(value))
            return 0;
        return Long.parseLong(value);
    }

    @InverseMethod("stringToNumber")
    public static String numberToString(EditText view, Number oldValue,
                                  Number value) {
        NumberFormat numberFormat = getNumberFormat(view);

        try {
            // Don't return a different value if the parsed value
            // doesn't change
            String inView = view.getText().toString();
            Number parsed =
                    numberFormat.parse(inView);
            if (parsed.equals(value)) {
                return view.getText().toString();
            }
        } catch (ParseException e) {
            // Old number was broken
        }
        return numberFormat.format(value);
    }

    public static Number stringToNumber(EditText view, Number oldValue,
                                  String value) {
        if(TextUtils.isEmpty(view.getText()))
            return 0;
        NumberFormat numberFormat = getNumberFormat(view);
        try {
            return numberFormat.parse(value);
        } catch (ParseException e) {
            Resources resources = view.getResources();
            String errStr = resources.getString(0);
            view.setError(errStr);
            return oldValue;
        }
    }
    @InverseMethod("stringToTimeLong")
    public static String timeLongToString(EditText view, long oldValue, long value){
        if(oldValue == 0){
            return "";
        }


        try {
            String inView = view.getText().toString();
            long parse = Time.valueOf(inView).getTime();
            if(parse == oldValue){
                return view.getText().toString();
            }
        }catch (Exception e){

        }
        return new Time(value).toString().substring(0, 5);
    }

    public static long stringToTimeLong(EditText view, long oldValue, String value){
        if(TextUtils.isEmpty(view.getText())){
            return 0;
        }

        return Time.valueOf(value).getTime();
    }

    @InverseMethod("stringToDateLong")
    public static String dateLongToString(EditText view, long oldValue, long value){
        if(oldValue == 0){
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try {
            String inView = view.getText().toString();
            long parse = sdf.parse(inView).getTime();
            if(parse == oldValue){
                return view.getText().toString();
            }
        } catch (ParseException e) {
            // Old value was broken
        }
        return sdf.format(new Date(value));
    }

    public static long stringToDateLong(EditText view, long oldValue, String value){
        if(TextUtils.isEmpty(view.getText()))
            return 0;

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            return sdf.parse(value).getTime();
        } catch (ParseException e) {
            return oldValue;
        }
    }

    private static NumberFormat getNumberFormat(View view) {
        Resources resources= view.getResources();
        Locale locale = resources.getConfiguration().locale;
        NumberFormat format =
                NumberFormat.getNumberInstance(locale);
        if (format instanceof DecimalFormat) {
            DecimalFormat decimalFormat = (DecimalFormat) format;
            decimalFormat.setGroupingUsed(false);
        }
        return format;
    }
}
