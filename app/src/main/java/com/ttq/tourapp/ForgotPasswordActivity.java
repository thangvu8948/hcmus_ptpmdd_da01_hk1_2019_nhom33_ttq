package com.ttq.tourapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ttq.tourapp.Fragment.FragmentForgotPasswordEmail;
import com.ttq.tourapp.Fragment.FragmentForgotPasswordPhone;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

public class ForgotPasswordActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
    }

    public void forgotPassword(View view) {

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction =fragmentManager.beginTransaction();

        FragmentForgotPasswordEmail forgotPasswordEmail = new FragmentForgotPasswordEmail();

        Fragment fragment = null;

        switch (view.getId()){
        case R.id.forgotPassViaEmail:
            fragment = new FragmentForgotPasswordEmail();
            break;
        case R.id.forgotPassViaPhone:
            fragment = new FragmentForgotPasswordPhone();
            break;
        }

        fragmentTransaction.replace(R.id.fraForgot,fragment);
        fragmentTransaction.commit();

    }
}
