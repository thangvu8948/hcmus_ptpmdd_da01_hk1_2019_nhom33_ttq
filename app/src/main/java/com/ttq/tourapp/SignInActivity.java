package com.ttq.tourapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.interfaces.APIServices;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.SignInEmailRequest;
import com.ttq.tourapp.models.SignInEmailResponse;
import com.ttq.tourapp.models.SignInFBRequest;
import com.ttq.tourapp.models.SignInFBResponse;
import com.ttq.tourapp.models.SignInGGRequest;
import com.ttq.tourapp.models.SignInGGResponse;
import com.ttq.tourapp.models.UserInfoResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SignInActivity extends AppCompatActivity {

    private static final int RC_SIGN_IN = 0;
    TextView mForgotpass;
    EditText mEmailPhone;
    EditText mPassword;
    //   @Bind(R.id.btnSignIn)
    Button mEmailSignInButton;
    LoginButton mFBSignInButton;
    APIServices service;
    CallbackManager callbackManager;
    SignInButton mGGSignInButton;
    //  Button mSignInButton;

    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_in);

        mEmailPhone = findViewById(R.id.edtEmail);
        mPassword = findViewById(R.id.edtpswd);
        mForgotpass = findViewById(R.id.forgotPass);
        mEmailSignInButton = findViewById(R.id.btnSignInEmailPhone);
        mFBSignInButton = findViewById(R.id.btnSignInFacebook);

        mFBSignInButton.setReadPermissions("email");
        mGGSignInButton = findViewById(R.id.btnSignInGoogle);

        //Use Email or Phone to signin
        mEmailSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attempLogin();
            }
        });
        //Use FB Sign In
        mFBSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attempFBLogin();
            }
        });
        //Use GG SignIn
        String serverClientId = "";
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getResources().getString(R.string.server_client_id))
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGGSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attempGGSignIn();
            }
        });


        Retrofit retrofit = APIClient.getInstance().getAdapter();
        service = retrofit.create(APIServices.class);

        mForgotpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPassword();
            }
        });

    }

    private void forgotPassword() {
        Intent intent = new Intent(SignInActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    private void attempGGSignIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

/*    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        callbackManager.onActivityResult(requestCode,resultCode,data);
        super.onActivityResult(requestCode, resultCode, data);
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void SignIn(String email, String password) {
        UserService service = APIClient.getInstance().getAdapter().create(UserService.class);
        SignInEmailRequest signInEmailRequest = new SignInEmailRequest();
        signInEmailRequest.setEmailPhone(email);
        signInEmailRequest.setPassword(password);

        Call<SignInEmailResponse> signInEmailResponseCall = service.getToken(signInEmailRequest);

        signInEmailResponseCall.enqueue(new Callback<SignInEmailResponse>() {
            @Override
            public void onResponse(Call<SignInEmailResponse> call, Response<SignInEmailResponse> response) {
                int statusCode = response.code();
                try {
                    if (statusCode == 200){
                        SignInEmailResponse signInEmailResponse = response.body();
                        String token = signInEmailResponse.getToken();
                        String userId = signInEmailResponse.getUserId();
                        SharedPrefs.getInstance().put(SharedPrefs.ACCESS_TOKEN, token);
                        SharedPrefs.getInstance().put(SharedPrefs.USER_ID, userId);
                        APIClient.getInstance().setAccessToken(token);

                        getUserInfo();
                    }else {
                        String errorBody = response.errorBody().string();
                        String errorMsg = new JSONObject(errorBody).getString("message");
                        Toast.makeText(SignInActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SignInEmailResponse> call, Throwable throwable) {
                Log.d("Login Activity", "onFailure" + throwable.getMessage());
            }
        });
    }
    public void onClick_SignUnButton(View view){
        Intent intent = new Intent(SignInActivity.this,SignUpActivity.class);
        startActivity(intent);
    }
    private void attempFBLogin() {
        try {
            callbackManager = CallbackManager.Factory.create();
            final AccessToken[] accessToken = {null};
            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
/*                        accessToken[0] = AccessToken.getCurrentAccessToken();
                        Log.d("accesstoken", accessToken[0].getToken());
                        Toast.makeText(SignInActivity.this, accessToken[0].getToken(), Toast.LENGTH_LONG).show();*/

                            SignInFBRequest signInFBRequest = new SignInFBRequest();
                            signInFBRequest.setAccessToken(AccessToken.getCurrentAccessToken().getToken());

                            Call<SignInFBResponse> signInFBResponseCall = service.getTokenByFB(signInFBRequest);

                            signInFBResponseCall.enqueue(new Callback<SignInFBResponse>() {
                                @Override
                                public void onResponse(Call<SignInFBResponse> call, Response<SignInFBResponse> response) {
                                    int statusCode = response.code();

                                    SignInFBResponse signInEmailResponse = response.body();
                                    String token = response.body().getToken();

/*                                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = sharedPref.edit();
                                editor.putString(getString(R.string.saved_access_token), response.body().getToken());
                                editor.commit();
                                editor.apply();*/

                                    SharedPrefs.getInstance().put(SharedPrefs.ACCESS_TOKEN, token);
                                    getUserInfo();
                                    Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                    return;
                                }

                                @Override
                                public void onFailure(Call<SignInFBResponse> call, Throwable throwable) {
                                    Log.d("Login Activity", "onFailure" + throwable.getMessage());
                                }
                            });
                        }

                        @Override
                        public void onCancel() {
                            Log.d("cancel", "cancel");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Log.d("err", error.toString());
                        }
                    });
        } catch (Exception e)  {

        }



    }

    String emailPhone = "";
    String password = "";

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            String token = account.getIdToken();
            SignInGGRequest signInGGRequest = new SignInGGRequest();
            signInGGRequest.setAccessToken(token);
            Call<SignInGGResponse> signInGGResponseCall = service.getTokenByGG(signInGGRequest);
            signInGGResponseCall.enqueue(new Callback<SignInGGResponse>() {

                @Override
                public void onResponse(Call<SignInGGResponse> call, Response<SignInGGResponse> response) {
                    int statusCode = response.code();
                    switch (statusCode) {
                        case 200:
                            SignInGGResponse signInGGResponse = response.body();
                            String s = response.body().getToken();
                            Log.d("Login Activity", "onResponse" + statusCode);

                            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(getString(R.string.saved_access_token), response.body().getToken());
                            editor.commit();
                            editor.apply();

                            Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                            return;

                        case 400:
                            Toast.makeText(SignInActivity.this, "Error 400", Toast.LENGTH_LONG).show();
                            break;
                        default:
                            Toast.makeText(SignInActivity.this, "Error Occured", Toast.LENGTH_LONG).show();

                    }
                }

                @Override
                public void onFailure(Call<SignInGGResponse> call, Throwable t) {
                    Toast.makeText(SignInActivity.this, "Failed", Toast.LENGTH_LONG).show();
                }
            });
            // Signed in successfully, show authenticated UI.
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("Google Sign In Error", "signInResult:failed code=" + e.getStatusCode());
        }
    }

    private void getUserInfo(){
        String token = APIClient.getInstance().getAccessToken();
        Call<UserInfoResponse> call = APIClient.getInstance().getService().userInfo(token);

        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                try {
                    int statusCode = response.code();
                    if(statusCode == 200){
                        UserInfoResponse infoResponse = response.body();

                        SharedPrefs.getInstance().put(SharedPrefs.USER_INFO, infoResponse);
                        APIClient.getInstance().setUserId(String.valueOf(infoResponse.getId().intValue()));
                        Intent intent = new Intent(SignInActivity.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }else{
                        String errorBody = response.errorBody().string();
                        String errorMsg = new JSONObject(errorBody).getString("message");
                        Toast.makeText(SignInActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    Toast.makeText(SignInActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                } catch (JSONException e) {
                    Toast.makeText(SignInActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                Toast.makeText(SignInActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    void collectInput() {
        emailPhone = mEmailPhone.getText().toString();
        password = mPassword.getText().toString();
    }

    void attempLogin() {
        collectInput();
        SignIn(emailPhone, password);
    }
}
