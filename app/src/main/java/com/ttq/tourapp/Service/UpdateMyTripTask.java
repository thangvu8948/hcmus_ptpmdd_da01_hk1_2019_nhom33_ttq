package com.ttq.tourapp.Service;

import android.os.AsyncTask;

import com.ttq.tourapp.models.HistoryTour;

import java.util.ArrayList;

public class UpdateMyTripTask extends AsyncTask<Void, Void, ArrayList<HistoryTour>> {
    private ArrayList<HistoryTour> mData;
    public UpdateMyTripTask(ArrayList<HistoryTour> data) {
        mData = data;
    }
    @Override
    protected ArrayList<HistoryTour> doInBackground(Void... voids) {

        return mData;
    }
}
