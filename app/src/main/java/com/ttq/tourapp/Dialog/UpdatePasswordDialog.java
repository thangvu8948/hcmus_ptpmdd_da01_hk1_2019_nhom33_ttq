package com.ttq.tourapp.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputEditText;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.R;
import com.ttq.tourapp.interfaces.APIServices;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.UpdatePasswordRequest;
import com.ttq.tourapp.models.UpdatePasswordResponse;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePasswordDialog extends DialogFragment {

    APIServices service;
    private TextInputEditText mOldPassword;
    private TextInputEditText mNewPassword;
    private TextInputEditText mNewPasswordConfirm;
    private Button mCancelButton;
    private Button mSubmitButton;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.popup_changepassword, null);
        service = APIClient.getInstance().getAdapter().create(APIServices.class);

        mOldPassword = view.findViewById(R.id.oldPass);
        mNewPassword = view.findViewById(R.id.newPass);
        mNewPasswordConfirm = view.findViewById(R.id.newPassAgain);
        mCancelButton = view.findViewById(R.id.btnCancel);
        mSubmitButton = view.findViewById(R.id.btnSubmit);
        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doChangePassword();
            }
        });


        builder.setView(view);
        return builder.create();
    }

    private void doChangePassword() {

        String oldPass = mOldPassword.getText().toString();
        String newPass = mNewPassword.getText().toString();
        String mConfirmPass = mNewPasswordConfirm.getText().toString();
        if (oldPass == null || newPass == null || mConfirmPass == null) {
            Toast.makeText(getActivity(), "Vui lòng nhập đủ các trường", Toast.LENGTH_LONG).show();
            return;
        }
        if (!mNewPassword.getText().toString().equals(mNewPasswordConfirm.getText().toString())) {
            Toast.makeText(getActivity(), "Xác nhận mật khẩu không chính xác", Toast.LENGTH_LONG).show();
            return;
        }

        UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest();
        int userId = SharedPrefs.getInstance().get(SharedPrefs.USER_ID, Integer.class);
        updatePasswordRequest.setUserId(String.valueOf(userId));
        updatePasswordRequest.setCurrentPassword(oldPass);
        updatePasswordRequest.setNewPassword(newPass);
        String accesstoken = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);
        Log.d("token", accesstoken);
        Call<UpdatePasswordResponse> updatePasswordCall = service.updatePassword(accesstoken, "application/json", updatePasswordRequest);
        try {
            updatePasswordCall.enqueue(new Callback<UpdatePasswordResponse>() {
                @Override
                public void onResponse(Call<UpdatePasswordResponse> call, Response<UpdatePasswordResponse> response) {
                    int statusCode = response.code();
                    if (statusCode == 200) {
                        Toast.makeText(getActivity(), "đổi mật khẩu thành công", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Đổi mật khẩu thất bại. Error: " + statusCode + "\n" + response.message(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<UpdatePasswordResponse> call, Throwable t) {
                    Toast.makeText(getActivity(), "Lỗi kết nối", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Có lỗi xảy ra", Toast.LENGTH_LONG).show();
        }


    }

}
