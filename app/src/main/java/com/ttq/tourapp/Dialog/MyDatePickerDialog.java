package com.ttq.tourapp.Dialog;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;

import java.util.Calendar;

public class MyDatePickerDialog {
    private Context context = null;
    private Calendar calendar;
    private DatePickerDialog datePickerDialog;
    private MyDatePickerListen listen;

    public MyDatePickerDialog(Context context, MyDatePickerListen listen) {
        this.calendar = Calendar.getInstance();
        this.context = context;
        this.listen = listen;

        initDialog();
    }

    private void initDialog() {
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        listen.onDateSet(year, monthOfYear, dayOfMonth);
                    }
                }, year, month, day);
    }

    public void show() {
        if (datePickerDialog == null) {
            return;
        }
        datePickerDialog.show();
    }

    public interface MyDatePickerListen {
        void onDateSet(int year, int monthOfYear, int dayOfMonth);
    }
}