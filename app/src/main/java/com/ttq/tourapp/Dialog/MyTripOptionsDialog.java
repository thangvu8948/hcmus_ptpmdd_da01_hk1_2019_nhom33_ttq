package com.ttq.tourapp.Dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.ttq.tourapp.Activity.SearchUser;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.R;
import com.ttq.tourapp.ReviewTourActivity;
import com.ttq.tourapp.TourDetailActivity;
import com.ttq.tourapp.UpdateTourActivity;
import com.ttq.tourapp.models.Extra;
import com.ttq.tourapp.models.RemoveTourRequest;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.UpdateTourRequest;

import org.json.JSONObject;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyTripOptionsDialog extends DialogFragment {

    //Variables
    UserService service;
    Button BackButton;
    Button FollowButton;
    Button InviteButton;
    Button DetailButton;
    Button RemoveButton;
    Button ReviewButton;
    Button UpdateButton;
    Bundle recvBundle;
    boolean isDeleted = false;
    ListenerMyTrip listener;
    public MyTripOptionsDialog(Bundle bundle) {
        recvBundle = bundle;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_mytrip_options, null);

        init(view);

        BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        DetailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TourDetailActivity.class);
                int id = Integer.parseInt(Objects.requireNonNull(recvBundle.getString("tourId")));
                intent.putExtra(Extra.TOUR_ID, id);
                startActivity(intent);
                dismiss();
            }
        });

        InviteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SearchUser.class);
                intent.putExtras(recvBundle);
                startActivity(intent);
            }
        });
        ReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ReviewTourActivity.class);
                intent.putExtras(recvBundle);
                startActivity(intent);
            }
        });
        RemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isHost = recvBundle.getBoolean("isHost");
                if (isHost) {
                    HandleRemoveTour();
                } else {
                    Toast.makeText(getContext(), "Chức năng này chỉ dành cho chủ tour", Toast.LENGTH_SHORT).show();
                }

            }
        });
        UpdateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isHost = recvBundle.getBoolean("isHost");
                if (isHost) {
                    //do update tour
                    Intent intent = new Intent(getActivity(), UpdateTourActivity.class);
                    intent.putExtras(recvBundle);
                    startActivity(intent);
                } else {
                    Toast.makeText(getContext(), "Chức năng này chỉ dành cho chủ tour", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setView(view);
        return builder.create();
    }

    void init(View view) {
        service = APIClient.getInstance().getAdapter().create(UserService.class);
        BackButton = view.findViewById(R.id.btnBack);
        InviteButton = view.findViewById(R.id.btnInvite);
        FollowButton = view.findViewById(R.id.btnFollow);
        DetailButton = view.findViewById(R.id.btnDetail);
        ReviewButton = view.findViewById(R.id.btnRate);
        RemoveButton = view.findViewById(R.id.btnRemove);
        UpdateButton = view.findViewById(R.id.btnUpdate);
    }

    void HandleRemoveTour() {
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setMessage(R.string.ConfirmRemoveTour)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DoRemoveTour();
                    }
                })
                .setNegativeButton("No",null)
                .setIcon(R.drawable.ic_warning_black_48dp)
                .show();
    }

    private void DoRemoveTour() {
        String token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);
        final String id = recvBundle.getString("tourId");

        final RemoveTourRequest request = new RemoveTourRequest();
        request.setId(id);
        request.setStatus(-1);
        final int position = recvBundle.getInt("position");
        listener = (ListenerMyTrip)getTargetFragment();
        Call<JSONObject> call = service.RemoveTour(token, request);
        call.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                int code = response.code();
                if (code == 200) {
                    Toast.makeText(getContext(), "Cập nhật thành công", Toast.LENGTH_SHORT).show();
                    isDeleted = true;
                    listener.updateRemove(isDeleted, position);

                } else {
                    Toast.makeText(getContext(), "Cập nhật thất bại. Error: " + code +"\n" + response.message() + "\nTourId: " + id, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi kết nối", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public interface ListenerMyTrip {
        void updateRemove(boolean isDeleted, int position);
    }
}
