package com.ttq.tourapp.Dialog;

import android.app.TimePickerDialog;
import android.content.Context;
import android.widget.TimePicker;

import java.util.Calendar;

public class MyTimePickerDialog {
    private Context context = null;
    private Calendar calendar;
    private TimePickerDialog timePickerDialog;
    private MyTimePickerListen listen;

    public MyTimePickerDialog(Context context, MyTimePickerListen listen) {
        this.calendar = Calendar.getInstance();
        this.context = context;
        this.listen = listen;

        initDialog();
    }

    public void show() {
        if (timePickerDialog == null) {
            return;
        }
        timePickerDialog.show();
    }

    private void initDialog() {
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);

        timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        listen.onTimeSet(hourOfDay, minute);
                    }
                }, hour, min, false);
    }

    public interface MyTimePickerListen {
        void onTimeSet(int hourOfDay, int minute);
    }
}
