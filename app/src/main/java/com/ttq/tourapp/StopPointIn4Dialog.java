package com.ttq.tourapp;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.textfield.TextInputEditText;
import com.ttq.tourapp.Converter.Converter;
import com.ttq.tourapp.Dialog.MyDatePickerDialog;
import com.ttq.tourapp.Dialog.MyTimePickerDialog;
import com.ttq.tourapp.models.StopPoint;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class StopPointIn4Dialog extends DialogFragment{
    private TextInputEditText mSPNameEditText;
    private TextInputEditText mAddressEditText;
    private TextInputEditText mLeaveTimeEditText, mLeaveDateEditText, mMinCostEditText, mMaxCostEditText;
    private TextInputEditText mArriveTimeEditText, mArriveDateEditText;
    private ImageButton mArriveDateButton, mLeaveDateButton;
    private Spinner mServiceSpinner, mProvinceSpinner;

    private long mArriveTime = 0 , mArriveDate = 0, mLeaveTime = 0, mLeaveDate = 0;
    private String mName, mAddress;
    private int mServiceType = 0, mProvinceId = 0;
    private long mArriveAt = 0, mLeaveAt = 0;

    private MyTimePickerDialog mArriveTimePicker = null, mLeaveTimePicker = null;
    private MyDatePickerDialog mArriveDatePicker = null, mLeaveDatePicker = null;
    private StopPointIn4DialogListener mListener;
    private StopPoint stopPoint;

    private SimpleDateFormat mTimeFormat = new SimpleDateFormat("HH:mm");
    private SimpleDateFormat mDateFormat = new SimpleDateFormat("MM/dd/yyyy");

    public StopPointIn4Dialog(){this.stopPoint = new StopPoint();}

    public StopPointIn4Dialog(StopPoint stopPoint){
        this.stopPoint = stopPoint;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_add_stop_point_dialog, null);

        builder.setView(view)
                .setPositiveButton("OK", null)
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.cancel();
                    }
                });

        mTimeFormat = new SimpleDateFormat("HH:mm");
        mDateFormat = new SimpleDateFormat("dd-MM-yyyy");

        mMinCostEditText = view.findViewById(R.id.edtMinCost);
        mMinCostEditText.setText(String.valueOf(stopPoint.getMinCost()));

        mMaxCostEditText = view.findViewById(R.id.edtMaxCost);
        mMaxCostEditText.setText(String.valueOf(stopPoint.getMaxCost()));

        mSPNameEditText = view.findViewById(R.id.edtSPName);
        mAddressEditText = view.findViewById(R.id.edtSPAddress);

        mArriveTimeEditText = view.findViewById(R.id.edtArriveTime);
        mArriveTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setArriveTime();
            }
        });

        mArriveDateButton = view.findViewById(R.id.btnArriveDate);
        mArriveDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setArriveDate();
            }
        });

        mArriveDateEditText = view.findViewById(R.id.edtArriveDate);
        mArriveDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setArriveDate();
            }
        });

        mLeaveTimeEditText = view.findViewById(R.id.edtLeaveTime);
        mLeaveTimeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeaveTime();
            }
        });

        mLeaveDateEditText = view.findViewById(R.id.edtLeaveDate);
        mLeaveDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeaveDate();
            }
        });

        mLeaveDateButton = view.findViewById(R.id.btnLeaveDate);
        mLeaveDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setLeaveDate();
            }
        });

        mServiceSpinner = view.findViewById(R.id.spnServiceType);
        mServiceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mServiceType = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        mProvinceSpinner = view.findViewById(R.id.spnProvinceId);
        mProvinceSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mProvinceId = position;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        if(stopPoint != null){
            if(stopPoint.getArriveAt() != 0){

                Date arriveDate = new Date(stopPoint.getArriveAt());
                String strArriveTime = mTimeFormat.format(arriveDate);
                String strArriveDate = mDateFormat.format(arriveDate);

                try {
                    mArriveTime = mTimeFormat.parse(strArriveTime).getTime();
                    mArriveDate = mDateFormat.parse(strArriveDate).getTime();
                    mArriveTimeEditText.setText(strArriveTime);
                    mArriveDateEditText.setText(strArriveDate);

                    mMinCostEditText.setText(String.valueOf(stopPoint.getMinCost()));
                    mMaxCostEditText.setText(String.valueOf(stopPoint.getMaxCost()));

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if(stopPoint.getLeaveAt() != 0){
                Date leaveDate = new Date(stopPoint.getLeaveAt());
                String strLeaveTime = mTimeFormat.format(leaveDate);
                String strLeaveDate = mDateFormat.format(leaveDate);

                try {
                    mLeaveTime = mTimeFormat.parse(strLeaveTime).getTime();
                    mLeaveDate = mDateFormat.parse(strLeaveDate).getTime();
                    mLeaveTimeEditText.setText(strLeaveTime);
                    mLeaveDateEditText.setText(strLeaveDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }


            mSPNameEditText.setText(stopPoint.getName());
            mAddressEditText.setText(stopPoint.getAddress());
            mProvinceSpinner.setSelection(stopPoint.getProvinceId());
            mProvinceId = stopPoint.getProvinceId();
            mServiceSpinner.setSelection(stopPoint.getServiceTypeId());
            mServiceType = stopPoint.getServiceTypeId();
        }

        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();

        final AlertDialog dialog = (AlertDialog) getDialog();
        if(dialog != null){
            Button positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE);
            positiveButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mName = mSPNameEditText.getText().toString();
                    mAddress = mAddressEditText.getText().toString();

                    mArriveAt = mArriveTime + mArriveDate + 28800000;
                    mLeaveAt = mLeaveTime + mLeaveDate + 28800000;


                    long min = 0, max = 0;
                    if(!TextUtils.isEmpty(mMinCostEditText.getText()) && !TextUtils.isEmpty(mMaxCostEditText.getText())){
                        min = Long.parseLong(mMinCostEditText.getText().toString());
                        max = Long.parseLong(mMaxCostEditText.getText().toString());
                    }

                    if(mName.length() == 0 ){
                        Toast.makeText(getActivity(), "Tên điểm dừng không được để trống", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(mProvinceId == 0){
                        Toast.makeText(getActivity(), "Xin chọn Province", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(mServiceType == 0){
                        Toast.makeText(getActivity(), "Xin chọn Service type", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(mAddress.length() == 0){
                        Toast.makeText(getActivity(), "Địa chỉ không được để trống", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(mArriveAt == 0 || mLeaveAt == 0){
                        Toast.makeText(getActivity(), "Thời gian không được để trống", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if(min > max){
                        Toast.makeText(getActivity(), "Chi phí thấp phải nhỏ hơn chi phí cao", Toast.LENGTH_SHORT).show();
                        return;
                    }


                    stopPoint.setName(mName);
                    stopPoint.setAddress(mAddress);
                    stopPoint.setArriveAt(mArriveAt);
                    stopPoint.setLeaveAt(mLeaveAt);
                    stopPoint.setServiceTypeId(mServiceType);
                    stopPoint.setProvinceId(mProvinceId);
                    stopPoint.setMinCost(min);
                    stopPoint.setMaxCost(max);
                    mListener.create(stopPoint);
                    dialog.dismiss();
                }
            });
        }
    }

    public void setArriveTime(){
        if (mArriveTimePicker == null) {
            mArriveTimePicker = new MyTimePickerDialog(getActivity(), new MyTimePickerDialog.MyTimePickerListen() {
                @Override
                public void onTimeSet(int hourOfDay, int minute) {
                    Time time = new Time(hourOfDay, minute, 0);
                    String strTime = time.toString().substring(0, 5);
                    mArriveTimeEditText.setText(strTime);
                    try {
                        mArriveTime = mTimeFormat.parse(strTime).getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        mArriveTimePicker.show();
    }

    public void setLeaveTime(){
        if (mLeaveTimePicker == null) {

            mLeaveTimePicker = new MyTimePickerDialog(getActivity(), new MyTimePickerDialog.MyTimePickerListen() {
                @Override
                public void onTimeSet(int hourOfDay, int minute) {
                    Time time = new Time(hourOfDay, minute, 0);
                    String strTime = time.toString().substring(0, 5);
                    mLeaveTimeEditText.setText(strTime);
                    try {
                        mLeaveTime = mTimeFormat.parse(strTime).getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        mLeaveTimePicker.show();
    }

    public void setArriveDate(){
        if (mArriveDatePicker == null) {
            mArriveDatePicker = new MyDatePickerDialog(getActivity(), new MyDatePickerDialog.MyDatePickerListen() {
                @Override
                public void onDateSet(int year, int monthOfYear, int dayOfMonth) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    try {
                        mArriveDate = sdf.parse(String.format("%d-%d-%d",dayOfMonth,monthOfYear + 1,year)).getTime();
                        mArriveDateEditText.setText(Converter.dateLongToString(mArriveDateEditText, mArriveDate, mArriveDate));
                    } catch (ParseException e) {
                        Toast.makeText(getContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            });
        }
        mArriveDatePicker.show();
    }

    public void setLeaveDate(){
        if (mLeaveDatePicker == null) {
            mLeaveDatePicker = new MyDatePickerDialog(getActivity(), new MyDatePickerDialog.MyDatePickerListen() {
                @Override
                public void onDateSet(int year, int monthOfYear, int dayOfMonth) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    try {
                        mLeaveDate = sdf.parse(String.format("%d-%d-%d",dayOfMonth,monthOfYear + 1,year)).getTime();
                        mLeaveDateEditText.setText(Converter.dateLongToString(mLeaveDateEditText, mLeaveDate, mLeaveDate));
                    } catch (ParseException e) {
                        Toast.makeText(getContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            });
        }
        mLeaveDatePicker.show();
    }

    public interface StopPointIn4DialogListener{
        void create(StopPoint stopPoint);
        void cancel();
    }

    public void setOnListener(StopPointIn4DialogListener listener){
        this.mListener = listener;
    }

}
