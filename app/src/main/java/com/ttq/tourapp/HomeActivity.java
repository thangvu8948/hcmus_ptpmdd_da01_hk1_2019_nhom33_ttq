package com.ttq.tourapp;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.ttq.tourapp.Fragment.FragmentAdapter;
import com.ttq.tourapp.Fragment.ListTourFragment;
import com.ttq.tourapp.Fragment.ListTourV2Fragment;
import com.ttq.tourapp.Fragment.MyTripFragment;
import com.ttq.tourapp.Fragment.NotifyFragment;
import com.ttq.tourapp.Fragment.StopPointFragment;
import com.ttq.tourapp.Fragment.UserDetailFragment;

//import static com.ttq.tourapp.SignInActivity.getUserInfo;

public class HomeActivity extends AppCompatActivity {

    ViewPager viewPager;
    TabLayout tabLayout;
    FragmentAdapter fragmentAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //getUserInfo();
        initItems();
    }
/*
    public void getUserInfo() {
        String token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);
        UserService service = APIClient.getInstance().getAdapter().create(UserService.class);
        Call<Users> userInfoResponseCall = service.userInfo(token);
        userInfoResponseCall.enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    //      SharedPrefs.getInstance().put(SharedPrefs.USER_INFO, response);
                    Toast.makeText(getApplicationContext(), "Get user info success", Toast.LENGTH_SHORT);

                } else {
                    Toast.makeText(getApplicationContext(), "Get user info failed", Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Connection error", Toast.LENGTH_SHORT);
            }
        });

    }
*/

    private void initItems() {
        viewPager = findViewById(R.id.page_viewer);
        tabLayout = findViewById(R.id.tabs);

        fragmentAdapter = new FragmentAdapter((getSupportFragmentManager()));
        fragmentAdapter.addFragment(new ListTourV2Fragment(), "ListTourV2");
        //fragmentAdapter.addFragment(new ListTourFragment(), "ListTour");
        fragmentAdapter.addFragment(new MyTripFragment(), "History");
        fragmentAdapter.addFragment(new NotifyFragment(), "Notifications");
        fragmentAdapter.addFragment(new StopPointFragment(), "StopPoints");
        fragmentAdapter.addFragment(new UserDetailFragment(), "User");
        viewPager.setAdapter(fragmentAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setTabsFromPagerAdapter(fragmentAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            switch (i) {
                case 0:
                    tabLayout.getTabAt(i).setIcon(R.drawable.ic_list_black_24dp);
                    tabLayout.getTabAt(i).setText("");
                    break;
                case 1:
                    tabLayout.getTabAt(i).setIcon(R.drawable.ic_history_black_24dp);
                    tabLayout.getTabAt(i).setText("");
                    break;
                case 2:
                    tabLayout.getTabAt(i).setIcon(R.drawable.ic_notifications_black_24dp);
                    tabLayout.getTabAt(i).setText("");
                    break;
                case 3:
                    tabLayout.getTabAt(i).setIcon(android.R.drawable.ic_menu_mapmode);
                    tabLayout.getTabAt(i).setText("");
                    break;
                case 4:
                    tabLayout.getTabAt(i).setIcon(R.drawable.ic_perm_identity_black_24dp);
                    tabLayout.getTabAt(i).setText("");
                    break;
            }
        }
    }
}
