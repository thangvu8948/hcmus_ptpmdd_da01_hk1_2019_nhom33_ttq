package com.ttq.tourapp.NetWork;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    private static APIClient instance;

    private UserService userService;

    private Retrofit adapter;

    private String accessToken;

    private String userId;

    private APIClient() {
        adapter = new Retrofit.Builder()
                .baseUrl("http://35.197.153.192:3000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

         userService = adapter.create(UserService.class);
    }

    public static APIClient getInstance() {
        if (instance == null)
            instance = new APIClient();
        return instance;
    }

    public UserService getService(){
        return userService;
    }

    public Retrofit getAdapter(){
        return adapter;
    }

    public String getUserId(){return userId;}

    public String getAccessToken() {
        return accessToken;
    }

    public void setUserId(String id){
        this.userId = id;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
