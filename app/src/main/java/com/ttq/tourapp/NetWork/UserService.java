package com.ttq.tourapp.NetWork;

import com.ttq.tourapp.models.AddMemberRequest;
import com.ttq.tourapp.models.AllStopPointsRequest;
import com.ttq.tourapp.models.CreateTourRequest;
import com.ttq.tourapp.models.EditInfoRequest;
import com.ttq.tourapp.models.GetCommentListResponse;
import com.ttq.tourapp.models.GetReviewListResponse;
import com.ttq.tourapp.models.HistoryTourResponse;
import com.ttq.tourapp.models.Invitation;
import com.ttq.tourapp.models.ListTourResponse;
import com.ttq.tourapp.models.RemoveTourRequest;
import com.ttq.tourapp.models.RequestClass.FeedbackService;
import com.ttq.tourapp.models.RequestClass.RemoveStopPointsRequest;
import com.ttq.tourapp.models.RequestClass.SendCommentResquest;
import com.ttq.tourapp.models.RequestClass.SendReviewResquest;
import com.ttq.tourapp.models.RequestClass.SetStopPointRequest;
import com.ttq.tourapp.models.RequestClass.SuggestedStopPointsRequest;
import com.ttq.tourapp.models.ResponseInvitationRequest;
import com.ttq.tourapp.models.SearchMyTripResponse;
import com.ttq.tourapp.models.SearchUserResponse;
import com.ttq.tourapp.models.SignInEmailRequest;
import com.ttq.tourapp.models.SignInEmailResponse;
import com.ttq.tourapp.models.SignInFBRequest;
import com.ttq.tourapp.models.SignInFBResponse;
import com.ttq.tourapp.models.SignInGGRequest;
import com.ttq.tourapp.models.SignInGGResponse;
import com.ttq.tourapp.models.SignUpEmailRequest;
import com.ttq.tourapp.models.UpdateTourRequest;
import com.ttq.tourapp.models.UserInfoResponse;

import org.json.JSONObject;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;


import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface UserService {

    @POST("/user/login")
    Call<SignInEmailResponse> getToken(@Body SignInEmailRequest signInEmailRequest);

    @POST("/user/register")
    Call<JSONObject> signUpByEmail(@Body SignUpEmailRequest signInEmailRequest);

    @POST("/user/login/by-facebook")
    Call<SignInFBResponse> getTokenByFB(@Body SignInFBRequest signInFBRequest);

    @POST("/user/login/by-facebook")
    Call<SignInGGResponse> getTokenByGG(@Body SignInGGRequest signInGGRequest);

    @GET("/tour/list")
    Call<ListTourResponse> getListTour(
            @Header("authorization") String authorization,
            @Query("rowPerPage") String rowPerPage,
            @Query("pageNum") String pageNum);

    @GET("/tour/list")
    Call<ResponseBody> getListTour(@Header("Authorization") String token,
                                   @Query("pageNum") int pagIndex,
                                   @Query("rowPerPage") int rowPerPage);


    @POST("/tour/create")
    Call<ResponseBody> createTour(@Header("authorization") String token,
                                  @Body CreateTourRequest createTourRequest);

    @Headers("Content-Type: application/json")
    @POST("/tour/set-stop-points")
    Call<ResponseBody> setStopPoints(@Header("Authorization") String token,
                                   @Body SetStopPointRequest setStopPointRequest);

    @Headers("Content-Type: application/json")
    @POST("/tour/set-stop-points")
    Call<ResponseBody> removeStopPoints(@Header("Authorization") String token,
                                        @Body RemoveStopPointsRequest removeStopPointsRequest);

    @GET("/tour/remove-stop-point")
    Call<ResponseBody> removeStopPoint(@Header("Authorization") String token,
                                       @Field("tourId") int tourId,
                                       @Field("stopPointId") int stopPointId);

    @GET("/tour/get/service-detail")
    Call<ResponseBody> getServiceDetail(@Header("Authorization") String token,
                                        @Query("serviceId") Number id);


    @GET("/tour/get/feedback-point-stats")
    Call<ResponseBody> getServiceFeedbackPoint(@Header("Authorization") String token,
                                        @Query("serviceId") Number id);

    @POST("/tour/add/feedback-service")
    Call<ResponseBody> sendFeedbackService(@Header("Authorization") String token,
                                           @Body FeedbackService feedbackService);

    @GET("/tour/get/feedback-service")
    Call<ResponseBody> getFeedbackServices(@Header("Authorization") String token,
                                           @Query("serviceId") Number id,
                                           @Query("pageIndex") int pageIndex,
                                           @Query("pageSize") int pageSize);

    @POST("/user/edit-info")
    Call<JSONObject> editInfo(@Header("Authorization") String Authorization,
                              @Header("Content-Type") String contentType,
                              @Body EditInfoRequest editInfoRequest);
    @GET("/user/info")
    Call<UserInfoResponse> userInfo(@Header("Authorization") String Authorization);


    @POST("/tour/suggested-destination-list")
    Call<ResponseBody> getSuggestStopPoints(@Header("Authorization") String token,
                                            @Body SuggestedStopPointsRequest suggested);
    @POST("/tour/suggested-destination-list")
    Call<ResponseBody> getStopPoints(@Header("Authorization") String token,
                                            @Body AllStopPointsRequest suggested);
    @GET("/tour/search/service")
    Call<ResponseBody> searchStopPoints(@Header("Authorization") String token,
                                        @Query("searchKey") String searchKey,
                                        @Query("pageIndex") int pageIndex,
                                        @Query("pageSize") int pageSize);
    @Multipart
    @POST("/user/update-avatar")
    Call<JSONObject> updateAvatar(@Header("Authorization") String token,
                                  @Part MultipartBody.Part file);

    @GET("/tour/history-user")
    Call<HistoryTourResponse> getHistoryTour(
            @Header("authorization") String authorization,
            @Query("pageIndex") String rowPerPage,
            @Query("pageSize") String pageNum);

    @GET("/tour/info")
    Call<ResponseBody> getTourInfo(@Header("authorization") String Authorization,
                                 @Query("tourId") int tourId);

    @GET("/tour/get/invitation")
    Call<Invitation> getInvitation(@Header("authorization") String authorization,
                                   @Query("pageIndex") String page,
                                   @Query("pageSize")String pageSize);
    @POST("/tour/response/invitation")
    Call<JSONObject> responseInvitation(@Header("authorization") String authorization,
                                        @Header("Content-Type") String contentType,
                                        @Body ResponseInvitationRequest responseInvitationRequest);

    @GET("/user/search")
    Call<SearchUserResponse> searchUser(@Query("searchKey") String searchKey,
                                        @Query("pageIndex") int pageIndex,
                                        @Query("pageSize") String pageSize);
    @POST("/tour/add/member")
    Call<JSONObject> addMember(@Header("authorization") String authorization,
                               @Body AddMemberRequest addMemberRequest);

    @POST("/tour/comment")
    Call<ResponseBody> sendComment(@Header("authorization") String token,
                                   @Body SendCommentResquest sendCommentResquest);

    @GET("/tour/comment-list")
    Call<ResponseBody> getCommentList(@Header("Authorization") String token,
                                                @Query("tourId") int id,
                                                @Query("pageIndex") int pageIndex,
                                                @Query("pageSize") int pageSize);

    @POST("/tour/add/review")
    Call<ResponseBody> sendReview(@Header("authorization") String token,
                                  @Body SendReviewResquest sendReviewResquest);

    @GET("/tour/get/review-list")
    Call<GetReviewListResponse> getReviewList(@Header("Authorization") String token,
                                              @Query("tourId") int id,
                                              @Query("pageIndex") int pageIndex,
                                              @Query("pageSize") String pageSize);

    @GET("/tour/get/review-point-stats")
    Call<ResponseBody> getReviewPoint(@Header("Authorization") String token,
                                      @Query("tourId") int tourId);




    @POST("/tour/update-tour")
    Call<JSONObject> UpdateTour(@Header("Authorization") String token,
                                @Body UpdateTourRequest updateTourRequest);
    @POST("/tour/update-tour")
    Call<JSONObject> RemoveTour(@Header("Authorization") String token,
                                @Body RemoveTourRequest removeTourRequest);

    @FormUrlEncoded
    @POST("/tour/clone")
    Call<ResponseBody> cloneTour(@Header("Authorization") String token,
                                 @Field("tourId") int id);
    @GET("/tour/search-history-user")
    Call<SearchMyTripResponse> searchMyTrip(@Header("Authorization") String token,
                                            @Query("searchKey") String searchKey,
                                            @Query("pageIndex") int pageIndex,
                                            @Query("pageSize") int pageSize);
}
