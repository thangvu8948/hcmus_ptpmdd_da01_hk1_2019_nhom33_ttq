package com.ttq.tourapp;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.UserInfoResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

/*      SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String accessToken = sharedPref.getString(getString(R.string.saved_access_token), null);*/
        String accessToken = SharedPrefs.getInstance().get("access_token", String.class);
        if (accessToken == null || accessToken.length() == 0) {
            Intent intent = new Intent(this, SignInActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return;
        } else {
            APIClient.getInstance().setAccessToken(accessToken);
            Intent intent = new Intent(this, HomeActivity.class);

            //Intent intent = new Intent(this, AddStopPointActivity.class);
            /*Intent intent = new Intent(this, SetStopPointsActivity.class);
            ArrayList<StopPoint> stopPoints = new ArrayList<>();
            stopPoints.add(new StopPoint());
            stopPoints.add(new StopPoint());
            stopPoints.add(new StopPoint());
            intent.putExtra(Extra.TOUR_ID, "3860");
            intent.putExtra(Extra.TOUR_NAME, "Tour Name");
            Bundle bundle = new Bundle();
            bundle.putSerializable(Extra.BUNDLE_ARRAY_LIST, stopPoints);
            intent.putExtra(Extra.BUNDLE, bundle);
*/
            getUserInfo();
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
            return;
        }
    }

    private void getUserInfo(){
        String token = APIClient.getInstance().getAccessToken();
        Call<UserInfoResponse> call = APIClient.getInstance().getService().userInfo(token);

        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                try {
                    int statusCode = response.code();
                    if(statusCode == 200){
                        UserInfoResponse infoResponse = response.body();

                        SharedPrefs.getInstance().put(SharedPrefs.USER_INFO, infoResponse);
                        APIClient.getInstance().setUserId(String.valueOf(infoResponse.getId().intValue()));
                        Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }else{
                        String errorBody = response.errorBody().string();
                        String errorMsg = new JSONObject(errorBody).getString("message");
                        Toast.makeText(SplashActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    Toast.makeText(SplashActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                } catch (JSONException e) {
                    Toast.makeText(SplashActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                Toast.makeText(SplashActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
