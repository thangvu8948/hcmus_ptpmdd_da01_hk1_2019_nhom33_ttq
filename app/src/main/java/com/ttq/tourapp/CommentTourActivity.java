package com.ttq.tourapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.models.Extra;
import com.ttq.tourapp.models.GetCommentListResponse;
import com.ttq.tourapp.models.RequestClass.SendCommentResquest;
import com.ttq.tourapp.models.CommentAdapter;
import com.ttq.tourapp.models.Comment;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.UserInfoResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentTourActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    Button mSendComment;
    EditText mComment;
    ArrayList<Comment> listComment;
    int id;
    int userId;
    RatingBar ratingBar;
    String token;
    int nowPage;
    boolean isLastPage;
    boolean isScrolling;
    LinearLayoutManager layoutManager;

    private CommentAdapter commentAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_tour);

        isScrolling = false;
        id = getIntent().getIntExtra(Extra.TOUR_ID, -1);

        nowPage = 1;


        recyclerView = findViewById(R.id.ListComment);
        mSendComment = findViewById(R.id.btnSendComment);
        mComment = findViewById(R.id.edtComment);
        listComment = new ArrayList<>();
        ratingBar = findViewById(R.id.ratingBar);

        userId = getUserId();

        token = APIClient.getInstance().getAccessToken();

        isLastPage = false;
        mSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendComment();
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int currentItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int lastVisibleItem = layoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItemCount + lastVisibleItem) == totalItemCount) {
                    isScrolling = false;
                    nowPage = nowPage + 1;
                    ShowComment();
                }
            }
        });

        initView();

        ShowComment();


    }

    private int getUserId() {
        return Integer.parseInt(APIClient.getInstance().getUserId());
    }


    private void initView() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(layoutManager);
        commentAdapter = new CommentAdapter(
                listComment,
                getApplicationContext()
        );
        recyclerView.setAdapter(commentAdapter);
    }

    private void SendComment() {
        if (mComment.getText().toString().isEmpty()) {
            return;
        }


        String cmt = mComment.getText().toString();
        mComment.setText("");
        SendCommentResquest sendCommentResquest = new SendCommentResquest(id, cmt);
        Call<ResponseBody> responseBodyCall = APIClient.getInstance().getService().sendComment(token, sendCommentResquest);
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200) {
                    //Toast.makeText(CommentTourActivity.this,"Thành công",Toast.LENGTH_SHORT).show();
                    ShowComment();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


    private void ShowComment() {

        Call<ResponseBody> getCommentListResponseCall = APIClient.getInstance().getService().getCommentList(token, id, nowPage, 20);
        getCommentListResponseCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                try {
                    if (code == 200) {
                        String body = response.body().string();
                        JSONArray array = new JSONArray(new JSONObject(body).getString("commentList"));
                        for(int i = 0; i < array.length(); i++){
                            JSONObject object = array.getJSONObject(i);
                            Comment comment = new Comment();
                            comment.setUserId(object.getString("id"));
                            comment.setName(object.getString("name"));
                            comment.setComment(object.getString("comment"));
                            comment.setCreatedOn(object.getLong("createdOn"));
                            comment.setAvatar(object.getString("avatar"));
                            listComment.add(comment);
                        }
                        commentAdapter.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
