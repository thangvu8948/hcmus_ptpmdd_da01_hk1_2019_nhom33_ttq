package com.ttq.tourapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;

import com.ttq.tourapp.models.GetReviewListResponse;
import com.ttq.tourapp.models.RequestClass.SendReviewResquest;
import com.ttq.tourapp.models.Review;
import com.ttq.tourapp.models.ReviewAdapter;
import com.ttq.tourapp.models.SharedPrefs;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewTourActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    UserService service;
    Button mSendReview;
    EditText mReview;
    ArrayList<Review> listReview;
    String id = "";
    RatingBar ratingBar;
    String token;
    int nowPage;
    boolean isLastPage;
    boolean isScrolling;
    ReviewAdapter reviewAdapter;
    LinearLayoutManager layoutManager;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_review_tour);

        Intent intent = getIntent();
        isScrolling = false;
        Bundle bundle = intent.getExtras();
        id = bundle.getString("tourId");
        nowPage = 1;

        progressBar = findViewById(R.id.reviewProgress);
        recyclerView = findViewById(R.id.ListReview);
        mSendReview = findViewById(R.id.btnSendReview);
        mReview = findViewById(R.id.edtReview);
        listReview = new ArrayList<>();
        ratingBar = findViewById(R.id.ratingBar);

        token = APIClient.getInstance().getAccessToken();

        isLastPage = false;
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);

        recyclerView.setLayoutManager(layoutManager);
        reviewAdapter = new ReviewAdapter(
                listReview,
                getApplicationContext()
        );
        recyclerView.setAdapter(reviewAdapter);
        mSendReview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendReview();
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int currentItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int lastVisibleItem = layoutManager.findFirstVisibleItemPosition();
                if(isScrolling && (currentItemCount + lastVisibleItem) == totalItemCount){
                    isScrolling = false;
                    nowPage=nowPage+1;
                    progressBar.setVisibility(View.VISIBLE);
                    ShowReview();
                }
            }
        });

        ShowReview();


    }

    private void initView() {
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);

        recyclerView.setLayoutManager(layoutManager);
        reviewAdapter = new ReviewAdapter(
                listReview,
                getApplicationContext()
        );
        recyclerView.setAdapter(reviewAdapter);
    }

    private void SendReview() {
        if(mReview.getText().toString().isEmpty())
        {
            return;
        }

        int rating = (int)(Math.floor(ratingBar.getRating()));
        SendReviewResquest sendReviewResquest = new SendReviewResquest(Integer.parseInt(id),(int)rating,mReview.getText().toString());
        Call<ResponseBody> responseBodyCall = APIClient.getInstance().getService().sendReview(token,sendReviewResquest);
        responseBodyCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code()==200)
                {
                    //Toast.makeText(ReviewTourActivity.this,"Thành công",Toast.LENGTH_SHORT).show();
                    ShowReview();

                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);

            }
        });
    }


    private void ShowReview() {

        Call<GetReviewListResponse> getReviewListResponseCall = APIClient.getInstance().getService().getReviewList(token,Integer.valueOf(id),nowPage,"10");
        getReviewListResponseCall.enqueue(new Callback<GetReviewListResponse>() {
            @Override
            public void onResponse(Call<GetReviewListResponse> call, Response<GetReviewListResponse> response) {
                if(response.code()==200)
                {

                    listReview.addAll(response.body().getReviewsArrayList());
                    reviewAdapter.notifyDataSetChanged();
                    if(listReview.isEmpty())
                    {
                        isLastPage = true;

                    }
                }
                progressBar.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onFailure(Call<GetReviewListResponse> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);

            }
        });
    }
}
