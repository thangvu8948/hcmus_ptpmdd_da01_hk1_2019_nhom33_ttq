package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class HistoryTourResponse {
    @SerializedName("total")
    private int total;
    @SerializedName("tours")
    private ArrayList<HistoryTour> tours;


    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<HistoryTour> getTours() {
        return tours;
    }

    public void setTours(ArrayList<HistoryTour> tours) {
        this.tours = tours;
    }
}
