package com.ttq.tourapp.models;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.ttq.tourapp.BR;

public class StopPointDetail extends BaseObservable {
    private Number id;
    private String name;
    private String address;
    private long minCost;
    private long maxCost;
    private float rating;

    public StopPointDetail() {
    }

    public StopPointDetail(Number id, String name, String address, long minCost, long maxCost, float rating) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.minCost = minCost;
        this.maxCost = maxCost;
        this.rating = rating;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getMinCost() {
        return minCost;
    }

    public void setMinCost(long minCost) {
        this.minCost = minCost;
    }

    public long getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(long maxCost) {
        this.maxCost = maxCost;
    }

    @Bindable
    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
        notifyPropertyChanged(com.ttq.tourapp.BR.rating);
    }
}
