package com.ttq.tourapp.models;

public class ListTourRequest {
    public String rowPerPage;
    public String pageNum;
    private String authorization;
/*
    public String orderBy;
    public String isDesc;
*/

    public ListTourRequest(String rowPerPage, String pageNum) {
        this.rowPerPage = rowPerPage;
        this.pageNum = pageNum;
    }

    public String getRowPerPage() {
        return rowPerPage;
    }
/*
    public String getOrderBy() {
        return orderBy;
    }

    public String getDesc() {
        return isDesc;
    }*/

    public void setRowPerPage(String rowPerPage) {
        this.rowPerPage = rowPerPage;
    }

    public String getPageNum() {
        return pageNum;
    }

/*    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public void setDesc(String desc) {
        isDesc = desc;
    }*/

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }
}
