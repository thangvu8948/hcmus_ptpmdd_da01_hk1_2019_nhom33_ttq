package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

public class Review {
    @SerializedName("userId")
    private  String userId;

    @SerializedName("name")
    private  String name;

    @SerializedName("review")
    private  String review;

    @SerializedName("avatar")
    private  String avatar;

    @SerializedName("point")
    private  String point;

    @SerializedName("createdOn")
    private  String createdOn;

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getReview() {
        return review;
    }

    public String getPoint() {
        return point;
    }

    public String getCreatedOn() {
        return createdOn;
    }
}
