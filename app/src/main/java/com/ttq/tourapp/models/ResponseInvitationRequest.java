package com.ttq.tourapp.models;

public class ResponseInvitationRequest {
    private String tourId;
    private boolean isAccepted;

    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public boolean isAccepted() {
        return isAccepted;
    }

    public void setAccepted(boolean accepted) {
        isAccepted = accepted;
    }
}
