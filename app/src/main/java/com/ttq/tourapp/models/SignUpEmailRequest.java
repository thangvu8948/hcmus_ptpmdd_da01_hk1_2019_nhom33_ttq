package com.ttq.tourapp.models;

public class SignUpEmailRequest {
    private String password;
    private String fullName;
    private String email;
    private String phone;

    public SignUpEmailRequest() {
        this.password = "";
        this.fullName = "";
        this.email = "";
        this.phone = "";
    }

    public SignUpEmailRequest(String password, String fullName, String email, String phone) {
        this.password = password;
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}