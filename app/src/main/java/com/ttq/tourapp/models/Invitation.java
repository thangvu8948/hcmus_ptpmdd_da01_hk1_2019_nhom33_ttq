package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Invitation {
    @SerializedName("total")
    private int total;
    @SerializedName("tours")
    private ArrayList<TourInviteInfo> tours;


    public ArrayList<TourInviteInfo> getTours() {
        return tours;
    }

    public void setTours(ArrayList<TourInviteInfo> tours) {
        this.tours = tours;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
