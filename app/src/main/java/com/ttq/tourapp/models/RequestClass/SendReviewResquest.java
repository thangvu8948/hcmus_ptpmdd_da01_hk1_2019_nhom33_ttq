package com.ttq.tourapp.models.RequestClass;

public class SendReviewResquest {
    private Integer tourId;
    private Integer point;
    private String review;

    public SendReviewResquest(Integer tourId, Integer point, String review) {
        this.tourId = tourId;
        this.point = point;
        this.review = review;
    }
}

