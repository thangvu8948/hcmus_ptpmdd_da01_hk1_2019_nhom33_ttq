package com.ttq.tourapp.models.RequestClass;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SuggestedStopPointsRequest {
    @SerializedName("hasOneCoordinate")
    @Expose
    private boolean hasOneCoordinate;
    @SerializedName("coordList")
    @Expose
    private Coordinate coordList;

    public SuggestedStopPointsRequest(Coordinate coordList) {
        this.hasOneCoordinate = true;
        this.coordList = coordList;
    }
}
