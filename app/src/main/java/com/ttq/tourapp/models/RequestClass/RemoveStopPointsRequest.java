package com.ttq.tourapp.models.RequestClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RemoveStopPointsRequest {
    @SerializedName("tourId")
    @Expose
    private int tourId;
    @SerializedName("deleteIds")
    @Expose
    ArrayList<Number> deleteIds;

    public RemoveStopPointsRequest(int tourId, ArrayList<Number> deleteIds) {
        this.tourId = tourId;
        this.deleteIds = deleteIds;
    }

    public RemoveStopPointsRequest(){
        this.deleteIds = new ArrayList<>();
    }

    public int getTourId() {
        return tourId;
    }

    public void setTourId(int tourId) {
        this.tourId = tourId;
    }

    public ArrayList<Number> getDeleteIds() {
        return deleteIds;
    }

    public void setDeleteIds(ArrayList<Number> deleteIds) {
        this.deleteIds = deleteIds;
    }
}
