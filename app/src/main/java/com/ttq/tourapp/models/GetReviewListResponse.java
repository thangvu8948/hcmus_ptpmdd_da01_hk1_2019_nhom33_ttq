package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetReviewListResponse {

    @SerializedName("reviewList")
    private ArrayList<Review> reviewsArrayList;

    public ArrayList<Review> getReviewsArrayList() {
        return reviewsArrayList;
    }
}
