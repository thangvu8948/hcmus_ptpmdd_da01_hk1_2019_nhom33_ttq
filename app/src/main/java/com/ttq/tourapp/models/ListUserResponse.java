package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListUserResponse {
    @SerializedName("total")
    private String total;
    @SerializedName("users")
    private ArrayList<Users> users;

    public String getTotal() {
        return total;
    }

    public ArrayList<Users> getUsers() {
        return users;
    }
}
