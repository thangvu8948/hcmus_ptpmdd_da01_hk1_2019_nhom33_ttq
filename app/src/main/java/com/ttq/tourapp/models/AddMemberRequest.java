package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

public class AddMemberRequest {
    @SerializedName("tourId")
    private String tourId;
    @SerializedName("invitedUserId")
    private String invitedUserId;
    @SerializedName("isInvited")
    private boolean isInvited;

    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public String getInvitedUserId() {
        return invitedUserId;
    }

    public void setInvitedUserId(String invitedUserId) {
        this.invitedUserId = invitedUserId;
    }

    public boolean getIsInvited() {
        return isInvited;
    }

    public void setIsInvited(boolean isInvited) {
        this.isInvited = isInvited;
    }
}
