package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

public class VerifyCodeResponse {
    @SerializedName("type")
    private String type;
    @SerializedName("expiredOn")
    private String expiredOn;

    public String getType() {
        return type;
    }

    public String getExpiredOn() {
        return expiredOn;
    }


}

