package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

public class SignInGGResponse {

    @SerializedName("userId")
    private String userId;
    @SerializedName("token")
    private String token;

    public String getUserId() {
        return userId;
    }

    public String getToken() {
        return token;
    }
}
