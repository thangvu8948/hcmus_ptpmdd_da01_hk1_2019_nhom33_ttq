package com.ttq.tourapp.models.RequestClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ttq.tourapp.models.StopPoint;

import java.util.ArrayList;

public class SetStopPointRequest {
    @SerializedName("tourId")
    @Expose
    int tourId;
    @SerializedName("stopPoints")
    @Expose
    ArrayList<StopPoint> stopPoints;

    public SetStopPointRequest(){
        this.tourId = -1;
        this.stopPoints = new ArrayList<>();
    }

    public SetStopPointRequest(int tourId, ArrayList<StopPoint> stopPoints) {
        this.tourId = tourId;
        this.stopPoints = stopPoints;
    }
}
