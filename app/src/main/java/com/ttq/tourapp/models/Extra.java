package com.ttq.tourapp.models;

public class Extra {
    public static final String TOUR_ID = "TOUR_ID";
    public static final String TOUR_NAME = "TOUR_NAME";
    public static final String BUNDLE = "BUNDLE";
    public static final String BUNDLE_ARRAY_LIST = "BUNDLE_ARRAY_LIST";
    public static final String SERVICE_ID = "SERVICE_ID";
    public static final String STOPPOINT_NAME = "STOPPOINT_NAME";
    public static final String STOPPOINT_ADDRESS = "STOPPOINT_ADDRESS";
    public static final String STOPPOINT_MIN = "STOPPOINT_MIN";
    public static final String STOPPOINT_MAX = "STOPPOINT_MAX";
    public static final String HOST_ID = "HOST_ID";
}
