package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

public class UpdateTourRequest {
    private String id; //Id of tour
    private String name; //Name of tour
    private long startDate;
    private long endDate;
    private float sourceLat;
    private float sourceLong;
    private float desLat;
    private float desLong;
    private int adults;
    private int childs;
    private int minCost;
    private int maxCost;
    private boolean isPrivate;
    private String avatar;
    private int status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public float getSourceLat() {
        return sourceLat;
    }

    public void setSourceLat(float sourceLat) {
        this.sourceLat = sourceLat;
    }

    public float getDesLat() {
        return desLat;
    }

    public void setDesLat(float desLat) {
        this.desLat = desLat;
    }

    public float getSourceLong() {
        return sourceLong;
    }

    public void setSourceLong(float sourceLong) {
        this.sourceLong = sourceLong;
    }

    public float getDesLong() {
        return desLong;
    }

    public void setDesLong(float desLong) {
        this.desLong = desLong;
    }

    public int getAdults() {
        return adults;
    }

    public void setAdults(int adults) {
        this.adults = adults;
    }

    public int getChilds() {
        return childs;
    }

    public void setChilds(int childs) {
        this.childs = childs;
    }

    public int getMinCost() {
        return minCost;
    }

    public void setMinCost(int minCost) {
        this.minCost = minCost;
    }

    public int getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(int maxCost) {
        this.maxCost = maxCost;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
