package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GetCommentListResponse {
    @SerializedName("commentList")
    private ArrayList<Comment> commentArrayList;

    public ArrayList<Comment> getCommentArrayList() {
        return commentArrayList;
    }
}
