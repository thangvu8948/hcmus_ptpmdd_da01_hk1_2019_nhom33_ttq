package com.ttq.tourapp.models.RequestClass;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedbackTour {
    @SerializedName("tourId")
    @Expose
    private Number id;
    @SerializedName("review")
    @Expose
    private String feedback;
    @SerializedName("point")
    @Expose
    private int point;

    public FeedbackTour(Number id, String feedback, int point) {
        this.id = id;
        this.feedback = feedback;
        this.point = point;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }
}
