package com.ttq.tourapp.models.RequestClass;

public class SendCommentResquest {
    private int tourId;
    private String comment;

    public SendCommentResquest(int tourId, String comment) {
        this.tourId = tourId;
        this.comment = comment;
    }
}
