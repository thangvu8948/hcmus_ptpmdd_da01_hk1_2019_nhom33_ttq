package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

public class SignInEmailResponse {

    @SerializedName("userId")
    private String userId;
    @SerializedName("emailVerified")
    private boolean emailVerified;
    @SerializedName("phoneVerified")
    private boolean phoneVerified;
    @SerializedName("token")
    private String token;

    public String getUserId() {
        return userId;
    }

    public String getToken() {
        return token;
    }
}
