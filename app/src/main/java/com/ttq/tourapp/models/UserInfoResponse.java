package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

public class UserInfoResponse {
    @SerializedName("id")
    private Number id;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("address")
    private String address;
    @SerializedName("dob")
    private String DOB;
    @SerializedName("gender")
    private Number gender;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("typeLogin")
    private Number typeLogin;
    @SerializedName("createdOn")
    private String createdOn;

    public Number getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public Number getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Number getTypeLogin() {
        return typeLogin;
    }

    public void setTypeLogin(Number typeLogin) {
        this.typeLogin = typeLogin;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }
}
