package com.ttq.tourapp.models;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class StopPointMarker implements ClusterItem {
    private LatLng position;
    private String title;
    private String snippet;
    private StopPoint stopPoint;

    public StopPointMarker(){
        title = "Test";
        stopPoint = new StopPoint();
        snippet = "T";
    }

    public StopPointMarker(StopPoint stopPoint){
        this.stopPoint = stopPoint;
        this.position = new LatLng(stopPoint.getLat(), stopPoint.getLog());
        this.title = stopPoint.getName();

        switch (stopPoint.getServiceTypeId()){
            case ServiceType.RESTAURANT:
                this.snippet = "Restaurant";
                break;
            case ServiceType.HOTEL:
                this.snippet = "Hotel";
                break;
            case ServiceType.REST_STATION:
                this.snippet = "Rest Station";
                break;
            case ServiceType.OTHER:
                this.snippet = "Other";
                break;
        }
    }

    public StopPoint getStopPoint() {
        return stopPoint;
    }

    public void setStopPoint(StopPoint stopPoint) {
        this.stopPoint = stopPoint;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    @Override
    public LatLng getPosition() {
        return position;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getSnippet() {
        return snippet;
    }
}
