package com.ttq.tourapp.models;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.google.gson.annotations.SerializedName;
import com.ttq.tourapp.BR;

public class CreateTourRequest extends BaseObservable {
    private String name;
    private long startDate;
    private long endDate;
    private long minCost;
    private long maxCost;
    private boolean isPrivate;
    private int adults;
    private int childs;
    private String avatar;

    public CreateTourRequest() {
        this.name = "";
        this.startDate = 0;
        this.endDate = 0;
        this.minCost = 0;
        this.maxCost = 0;
        this.isPrivate = false;
        this.adults = 0;
        this.childs = 0;
        this.avatar = null;
    }

    public CreateTourRequest(String name, long startDate, long endDate, long minCost, long maxCost, boolean isPrivate, int adults, int childs, String avatar) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.minCost = minCost;
        this.maxCost = maxCost;
        this.isPrivate = isPrivate;
        this.adults = adults;
        this.childs = childs;
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    @Bindable
    public long getStartDate() {
        return startDate;
    }

    @Bindable
    public long getEndDate() {
        return endDate;
    }

    public long getMinCost() {
        return minCost;
    }

    public long getMaxCost() {
        return maxCost;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public int getAdults() {
        return adults;
    }

    public int getChilds() {
        return childs;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
        notifyPropertyChanged(com.ttq.tourapp.BR.startDate);
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
        notifyPropertyChanged(com.ttq.tourapp.BR.endDate);
    }

    public void setMinCost(long minCost) {
        this.minCost = minCost;
    }

    public void setMaxCost(long maxCost) {
        this.maxCost = maxCost;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public void setAdults(int adults) {
        this.adults = adults;
    }

    public void setChilds(int childs) {
        this.childs = childs;
    }
}
