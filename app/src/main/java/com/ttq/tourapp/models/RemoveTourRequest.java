package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

public class RemoveTourRequest {
    @SerializedName("id")
    private String id;
    @SerializedName("status")
    private int status;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
