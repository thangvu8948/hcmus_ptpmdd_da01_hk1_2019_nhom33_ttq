package com.ttq.tourapp.models;

public class GetOTPRequest {
    private String type;
    private String value;

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public GetOTPRequest(String type, String value) {
        this.type = type;
        this.value = value;
    }
}
