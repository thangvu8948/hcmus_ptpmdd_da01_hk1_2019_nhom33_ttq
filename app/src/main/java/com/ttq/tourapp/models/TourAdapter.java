package com.ttq.tourapp.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ttq.tourapp.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class TourAdapter extends BaseAdapter {

    Context myContext;
    int myLayout;
    ArrayList<Tour> myListTour;

    public TourAdapter(Context context, int layout, ArrayList<Tour> listTour)
    {
        myContext = context;
        myLayout = layout;
        myListTour = listTour;
    }
    @Override
    public int getCount() {
        return myListTour.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(myLayout,null);
        ImageView avatar = convertView.findViewById(R.id.avatar);
        TextView name = convertView.findViewById(R.id.name);
        TextView cost = convertView.findViewById(R.id.cost);
        TextView date = convertView.findViewById(R.id.date);
        TextView adult = convertView.findViewById(R.id.adultschilds);


        avatar.setTag(myListTour.get(position).getAvatar());





        name.setText(myListTour.get(position).getName());
        cost.setText("Min: "+myListTour.get(position).getMinCost()+" Max: "+myListTour.get(position).getMinCost());
        date.setText("Start: "+myListTour.get(position).getStartDate()+" End: "+myListTour.get(position).getEndDate());
        adult.setText("Adults: "+myListTour.get(position).getAdults()+" Childs: "+myListTour.get(position).getChilds());
        return convertView;
    }

}

