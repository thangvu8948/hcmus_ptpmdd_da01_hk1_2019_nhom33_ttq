package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

public class UserInfo {
    @SerializedName("id")
    private String id;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("dob")
    private String dob;
    @SerializedName("gender")
    private Number gender;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("email_verified")
    private boolean emailVerified;
    @SerializedName("phone_verified")
    private boolean phoneVerified;

    public UserInfo() {
        this.id = "";
        this.fullName = "";
        this.email = "";
        this.phone = "";
        this.dob = "";
        this.gender = 1;
        this.avatar = "";
        this.emailVerified = false;
        this.phoneVerified = false;
    }

    public UserInfo(String id, String fullName, String email, String phone, String dob, Number gender, String avatar, boolean emailVerified, boolean phoneVerified) {
        this.id = id;
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
        this.dob = dob;
        this.gender = gender;
        this.avatar = avatar;
        this.emailVerified = emailVerified;
        this.phoneVerified = phoneVerified;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public boolean isPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Number getGender() {
        return gender;
    }

    public void setGender(Number gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }
}
