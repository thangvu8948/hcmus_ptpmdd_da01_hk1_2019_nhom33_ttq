package com.ttq.tourapp.models;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StopPoint implements Serializable {
    @SerializedName("id")
    @Expose
    private Number id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("lat")
    @Expose
    private double lat;
    @SerializedName("long")
    @Expose
    private double log;
    @SerializedName("minCost")
    @Expose
    private long minCost;
    @SerializedName("maxCost")
    @Expose
    private long maxCost;
    @SerializedName("arrivalAt")
    @Expose
    private long arriveAt;
    @SerializedName("leaveAt")
    @Expose
    private long leaveAt;
    @SerializedName("serviceTypeId")
    @Expose
    private int serviceTypeId;
    @SerializedName("provinceId")
    @Expose
    private int provinceId;

    public Number getServiceId() {
        return serviceId;
    }

    public void setServiceId(Number serviceId) {
        this.serviceId = serviceId;
    }

    @SerializedName("serviceId")
    @Expose
    private Number serviceId;

    public StopPoint() {
    }

    public StopPoint(Number id, String name, String address, double lat, double log, long minCost, long maxCost, long arriveAt, long leaveAt, int serviceTypeId, int provinceId) {
        this.id = null;
        this.name = name;
        this.address = address;
        this.lat = lat;
        this.log = log;
        this.minCost = minCost;
        this.maxCost = maxCost;
        this.arriveAt = arriveAt;
        this.leaveAt = leaveAt;
        this.serviceTypeId = serviceTypeId;
        this.provinceId = provinceId;
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLog() {
        return log;
    }

    public void setLog(double log) {
        this.log = log;
    }

    public long getMinCost() {
        return minCost;
    }

    public void setMinCost(long minCost) {
        this.minCost = minCost;
    }

    public long getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(long maxCost) {
        this.maxCost = maxCost;
    }

    public long getArriveAt() {
        return arriveAt;
    }

    public void setArriveAt(long arriveAt) {
        this.arriveAt = arriveAt;
    }

    public long getLeaveAt() {
        return leaveAt;
    }

    public void setLeaveAt(long leaveAt) {
        this.leaveAt = leaveAt;
    }

    public int getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(int serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }
}
