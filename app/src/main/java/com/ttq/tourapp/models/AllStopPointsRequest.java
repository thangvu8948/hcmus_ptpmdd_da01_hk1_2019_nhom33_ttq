package com.ttq.tourapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ttq.tourapp.models.RequestClass.Coordinate;

import java.util.ArrayList;

public class AllStopPointsRequest {
    @SerializedName("hasOneCoordinate")
    @Expose
    private boolean hasOneCoordinate;
    @SerializedName("coordList")
    @Expose
    private ArrayList<CoordinateSet> coordList;

    public AllStopPointsRequest() {
        this.hasOneCoordinate = false;
        coordList = new ArrayList<>();
        this.coordList.add(new CoordinateSet(23.980056, 85.577677, 23.588665, 163.065945));
        this.coordList.add(new CoordinateSet(-12.609835, 163.707522,-13.928084, 75.526301));
    }
}
