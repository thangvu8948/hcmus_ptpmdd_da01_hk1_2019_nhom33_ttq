package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SearchUserResponse {
    @SerializedName("total")
    private int total;
    @SerializedName("users")
    private ArrayList<UserInfoResponse> users;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<UserInfoResponse> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<UserInfoResponse> users) {
        this.users = users;
    }
}
