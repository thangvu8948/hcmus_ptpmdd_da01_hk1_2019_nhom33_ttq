package com.ttq.tourapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ttq.tourapp.models.RequestClass.Coordinate;

import java.util.ArrayList;

import javax.crypto.Cipher;

public class CoordinateSet {
    @SerializedName("coordinateSet")
    @Expose
    private ArrayList<Coordinate> coordinates;

    public CoordinateSet(double lat1, double long1, double lat2, double long2) {
        coordinates = new ArrayList<>();
        coordinates.add(new Coordinate(lat1, long1));
        coordinates.add(new Coordinate(lat2, long2));
    }

    public ArrayList<Coordinate> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(ArrayList<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }
}
