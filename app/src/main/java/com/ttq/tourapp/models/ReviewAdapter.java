package com.ttq.tourapp.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ttq.tourapp.R;
import com.ttq.tourapp.Service.DownloadImageTask;

import java.util.ArrayList;



public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {
    ArrayList<Review> myReviewList;
    Context myContext;

    public ReviewAdapter(ArrayList<Review> myReviewList, Context myContext) {
        this.myReviewList = myReviewList;
        this.myContext = myContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.review_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (myReviewList.get(position).getAvatar() != null)
        {
            DownloadImageTask downloadAvatar = new DownloadImageTask(holder.mAvatar);
            downloadAvatar.execute(myReviewList.get(position).getAvatar());

        }



        holder.mReview.setText( myReviewList.get(position).getReview());
        holder.mName.setText( myReviewList.get(position).getName());
        holder.mPoint.setRating( Integer.parseInt(myReviewList.get(position).getPoint()));

    }

    @Override
    public int getItemCount() {
        return myReviewList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView mAvatar;
        TextView mName;
        RatingBar mPoint;
        TextView mReview;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mAvatar = itemView.findViewById(R.id.avatar);
            mName = itemView.findViewById(R.id.name);
            mPoint = itemView.findViewById(R.id.ratingBar2);
            mReview = itemView.findViewById(R.id.review);
        }
    }
}