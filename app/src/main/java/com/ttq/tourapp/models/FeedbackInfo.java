package com.ttq.tourapp.models;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FeedbackInfo {
    private Number id;
    private String name;
    private String feedback;
    private int rating;
    private String created;

    public FeedbackInfo() {
    }

    public FeedbackInfo(Number id, String name, String feedback, int rating, long created) {
        this.id = id;
        this.name = name;
        this.feedback = feedback;
        this.rating = rating;
        setCreated(created);
    }

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int point) {
        this.rating = point;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public void setCreated(long created){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        Date date = new Date(created);

        this.created = sdf.format(date);
    }
}
