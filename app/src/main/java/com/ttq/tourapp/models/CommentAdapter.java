package com.ttq.tourapp.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ttq.tourapp.R;
import com.ttq.tourapp.Service.DownloadImageTask;

import java.util.ArrayList;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {
    ArrayList<Comment> myCommentList;
    Context myContext;

    public CommentAdapter(ArrayList<Comment> myCommentList, Context myContext) {
        this.myCommentList = myCommentList;
        this.myContext = myContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.comment_layout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (myCommentList.size()==0) {
            holder.mName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            holder.mName.setText(R.string.empty_comment);

        } else {
            if(myCommentList.get(position).getAvatar()!=null)
            {
                DownloadImageTask downloadAvatar = new DownloadImageTask(holder.mAvatar);
                downloadAvatar.execute(myCommentList.get(position).getAvatar());
            }

            if (myCommentList.get(position).getName() == null) {

            }
            holder.mName.setText(myCommentList.get(position).getName());
            holder.mComment.setText(myCommentList.get(position).getComment());

        }

    }

    @Override
    public int getItemCount() {
        return myCommentList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView mAvatar;
        TextView mName;
        TextView mComment;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mAvatar = itemView.findViewById(R.id.avatar);
            mName = itemView.findViewById(R.id.name);
            mComment = itemView.findViewById(R.id.comment);
        }
    }
}

