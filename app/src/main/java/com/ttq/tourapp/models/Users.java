package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

public class Users {
    @SerializedName("id")
    private String id;
    @SerializedName("fullName")
    private String fullName;
    @SerializedName("email")
    private String email;
    @SerializedName("phone")
    private String phone;
    @SerializedName("gender")
    private String gender;
    @SerializedName("typeLogin")
    private String typeLogin;
    @SerializedName("avatar")
    private String avatar;
    @SerializedName("dob")
    private String dob;

    public void setId(String id) {
        this.id = id;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setTypeLogin(String typeLogin) {
        this.typeLogin = typeLogin;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getId() {
        return id;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getGender() {
        return gender;
    }

    public String getTypeLogin() {
        return typeLogin;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getDob() {
        return dob;
    }
}
