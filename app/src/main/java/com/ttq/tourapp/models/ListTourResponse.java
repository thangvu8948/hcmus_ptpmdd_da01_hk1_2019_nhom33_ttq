package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ListTourResponse {
    /*

     */
    @SerializedName("total")
    private String total;
    @SerializedName("tours")
    private ArrayList<Tour> tours;

    public ArrayList<Tour> getTours() {
        return tours;
    }

    public String getTotal() {
        return total;
    }
}
