package com.ttq.tourapp.models;

public final class ServiceType {
    public static final int RESTAURANT = 1;
    public static final int HOTEL = 2;
    public static final int REST_STATION = 3;
    public static final int OTHER = 4;
}
