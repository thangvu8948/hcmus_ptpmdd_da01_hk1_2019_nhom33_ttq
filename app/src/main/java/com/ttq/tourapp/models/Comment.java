package com.ttq.tourapp.models;

import com.google.gson.annotations.SerializedName;
import com.ttq.tourapp.Adapter.MyBindingAdapter;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Comment {
    private  String userId;

    private  String name;

    private  String comment;

    private String createdOn;

    private String avatar;

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(long createdOn) {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd/MM/yyyy");
        Date date = new Date(createdOn);

        this.createdOn = sdf.format(date);
    }
}
