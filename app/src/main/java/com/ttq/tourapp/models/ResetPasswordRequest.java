package com.ttq.tourapp.models;

public class ResetPasswordRequest {
    private String userId;
    private String newPassword;
    private String verifyCode;

    public String getUserId() {
        return userId;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public ResetPasswordRequest(String userId, String newPassword, String verifyCode) {
        this.userId = userId;
        this.newPassword = newPassword;
        this.verifyCode = verifyCode;
    }
}
