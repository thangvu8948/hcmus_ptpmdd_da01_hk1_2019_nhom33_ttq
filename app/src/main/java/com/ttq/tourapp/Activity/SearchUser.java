package com.ttq.tourapp.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ttq.tourapp.Adapter.SearchUserAdapter;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.R;
import com.ttq.tourapp.models.SearchUserResponse;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.UserInfo;
import com.ttq.tourapp.models.UserInfoResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUser extends AppCompatActivity {
    RecyclerView mRecyclerView;
    ArrayList<UserInfoResponse> data;
    SearchUserAdapter mRcvAdapter;
    UserService service;
    SearchView mSearchView;
    ProgressBar progressBar;
    String searchKey = "";
    int pageIndex = 1;
    int perPage = 10;
    boolean isScrolling = false;
    String tourId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getIntent().getExtras();
        tourId = bundle.getString("tourId", "0");
        setContentView(R.layout.activity_search_user);
        progressBar = findViewById(R.id.progress_bar_search_user);
        mRecyclerView = findViewById(R.id.listUser);
        mSearchView = findViewById(R.id.seachUserBar);
        data = new ArrayList<>();
        service = APIClient.getInstance().getAdapter().create(UserService.class);

        Toast.makeText(this, tourId , Toast.LENGTH_SHORT).show();

        mRcvAdapter = new SearchUserAdapter(this, data, tourId);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);


        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mRcvAdapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int currentItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int lastVisibleItem = layoutManager.findFirstVisibleItemPosition();
                if(isScrolling && (currentItemCount + lastVisibleItem) == totalItemCount){
                    isScrolling = false;
                    loadMore();
                }
            }
        });
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                data.clear();
                pageIndex = 1;
                searchKey = mSearchView.getQuery().toString();
                searchUser(searchKey, pageIndex, perPage);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void loadMore() {
        progressBar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pageIndex++;        // Tắng số trang
                searchUser(searchKey, pageIndex, perPage);
            }
        }, 2000);
    }

    private void searchUser(String searchKey, int pageIndex, int perPage) {
        String token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);

        Call<SearchUserResponse> call = service.searchUser(searchKey, pageIndex, String.valueOf(perPage));
        call.enqueue(new Callback<SearchUserResponse>() {
            @Override
            public void onResponse(Call<SearchUserResponse> call, Response<SearchUserResponse> response) {
                int code = response.code();
                if (code == 200) {
                    data.addAll(response.body().getUsers());
                    progressBar.setVisibility(View.INVISIBLE);
                    mRcvAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getApplicationContext(), "Lỗi khi tìm kiếm", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<SearchUserResponse> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Lỗi kết nối", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
