package com.ttq.tourapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.taufiqrahman.reviewratings.BarLabels;
import com.ttq.tourapp.Adapter.FeedbackAdapter;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.databinding.ActivityStopPointDetailBinding;
import com.ttq.tourapp.models.Extra;
import com.ttq.tourapp.models.FeedbackInfo;
import com.ttq.tourapp.models.RequestClass.FeedbackService;
import com.ttq.tourapp.models.StopPointDetail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StopPointDetailActivity extends BaseActivity {

    private ActivityStopPointDetailBinding binding;
    private StopPointDetail detail;

    int colors[] = new int[]{
            Color.parseColor("#1AB826"),
            Color.parseColor("#1AB826"),
            Color.parseColor("#1AB826"),
            Color.parseColor("#1AB826"),
            Color.parseColor("#1AB826")};

    int raters[] = new int[]{0, 0, 0, 0, 0};

    @Override
    void onCreateActivity() {
        binding = DataBindingUtil.setContentView(StopPointDetailActivity.this, R.layout.activity_stop_point_detail);

        Intent intent = getIntent();

        detail = new StopPointDetail();
        detail.setId(intent.getIntExtra(Extra.SERVICE_ID, -1));
        detail.setName(intent.getStringExtra(Extra.STOPPOINT_NAME));
        detail.setAddress(intent.getStringExtra(Extra.STOPPOINT_ADDRESS));
        detail.setMinCost(intent.getLongExtra(Extra.STOPPOINT_MIN, 0));
        detail.setMaxCost(intent.getLongExtra(Extra.STOPPOINT_MAX, 0));
        getFeedbackPoint();

        init();
    }

    private void init(){
        if(binding == null)
            return;

        binding.setDetail(detail);

        binding.showFeedbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFeedbackBottomSheet();
            }
        });

        binding.ratingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogRating();
            }
        });
    }

    private void showFeedbackBottomSheet() {
        FeedbackBottomDialogFragment dialogFragment = FeedbackBottomDialogFragment.newInstance(detail.getId(), true);

        dialogFragment.show(getSupportFragmentManager(),
                FeedbackBottomDialogFragment.TAG);
    }


    private void getServiceDetail() {
        String accessToken = APIClient.getInstance().getAccessToken();

        Call<ResponseBody> call = APIClient.getInstance().getService().getServiceDetail(accessToken, detail.getId());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                showProgressLoading();
                int statusCode = response.code();
                try {
                    if (statusCode == 200) {
                        String body = response.body().string();
                        JSONObject object = new JSONObject(body);
                        detail.setName(object.getString("name"));
                        detail.setAddress(object.getString("address"));
                        //detail.setContact(object.getString("contact"));
                        detail.setMinCost(object.getLong("minCost"));
                        detail.setMaxCost(object.getLong("maxCost"));
                        //detail.setServiceTypeId(object.getInt("serviceTypeId"));
                    } else {
                        String errorBody = response.errorBody().string();
                        String errorMsg = new JSONObject(errorBody).getString("message");
                        Toast.makeText(StopPointDetailActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                    Toast.makeText(StopPointDetailActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getFeedbackPoint() {
        showProgressLoading();

        String accessToken = APIClient.getInstance().getAccessToken();
        Call<ResponseBody> call = APIClient.getInstance().getService().getServiceFeedbackPoint(accessToken, detail.getId());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                try {
                    if (statusCode == 200) {
                        float rating = 0f;
                        long totalReview = 0;
                        int totalPoint = 0;
                        String body = response.body().string();
                        JSONArray array = new JSONArray(new JSONObject(body).getString("pointStats"));
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            int point = object.getInt("point");
                            raters[5 - point] = object.getInt("total");

                            totalReview+= raters[5 - point];
                            totalPoint+= raters[5 - point] * point;
                        }
                        if(totalReview == 0){
                            detail.setRating(0.0f);
                        }else{
                            rating = (1.0f*totalPoint) / totalReview;
                            detail.setRating(Float.parseFloat(new DecimalFormat("##.#").format(rating).replace(',', '.')));
                        }

                        binding.ratingReviews.createRatingBars(100, BarLabels.STYPE1, colors, raters);
                        binding.setTotal(totalReview);
                    } else {
                        String errorBody = response.errorBody().string();
                        String errorMsg = new JSONObject(errorBody).getString("message");
                        Toast.makeText(StopPointDetailActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(StopPointDetailActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                dismissProgressLoading();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(StopPointDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showDialogRating() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(StopPointDetailActivity.this);
        builder.setTitle("Đánh giá điểm dừng");
        //builder.setMessage("Hãy điền thông tin");

        View view = LayoutInflater.from(StopPointDetailActivity.this).inflate(R.layout.layout_rating_stop_point, null);

        final RatingBar ratingBar = view.findViewById(R.id.ratingBar);
        final EditText editText = view.findViewById(R.id.edtComment);

        builder.setView(view);
        builder.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton("Gửi", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(!TextUtils.isEmpty(editText.getText()) && ratingBar.getRating() > 0){
                    FeedbackService feedback = new FeedbackService(detail.getId(), editText.getText().toString(), (int)ratingBar.getRating());
                    sendFeedback(feedback);
                }else{
                    Toast.makeText(getApplicationContext(), "Hãy điền thông tin và số sao", Toast.LENGTH_SHORT).show();
                }
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void sendFeedback(final FeedbackService feedback){
        showProgressLoading();

        String token = APIClient.getInstance().getAccessToken();

        Call<ResponseBody> call = APIClient.getInstance().getService().sendFeedbackService(token,feedback);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                try{
                    if(statusCode == 200){
                        getFeedbackPoint();
                    }else{
                        String errorBody = response.errorBody().string();
                        String error = new JSONObject(errorBody).getString("message");
                        Toast.makeText(getApplicationContext(), error, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    dismissProgressLoading();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                dismissProgressLoading();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
