package com.ttq.tourapp.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;

import com.ttq.tourapp.Adapter.BaseViewHolder;
import com.ttq.tourapp.databinding.LayoutListMemberItemBinding;

public class MemberInfoViewHolder extends BaseViewHolder<LayoutListMemberItemBinding> {
    public MemberInfoViewHolder(@NonNull LayoutListMemberItemBinding dataBinding) {
        super(dataBinding);
    }

    @Override
    public View getLayoutParent() {
        return mDataBinding.layoutParent;
    }

}
