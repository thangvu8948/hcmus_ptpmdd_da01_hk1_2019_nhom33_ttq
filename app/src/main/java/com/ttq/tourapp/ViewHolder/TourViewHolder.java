package com.ttq.tourapp.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;

import com.ttq.tourapp.Adapter.BaseViewHolder;
import com.ttq.tourapp.databinding.LayoutTourBinding;

public class TourViewHolder extends BaseViewHolder<LayoutTourBinding> {
    public TourViewHolder(@NonNull LayoutTourBinding dataBinding) {
        super(dataBinding);
    }

    @Override
    public View getLayoutParent() {
        return mDataBinding.layoutParent;
    }

    @Override
    public View getLayoutChecked() {
        return mDataBinding.layoutChecked;
    }
}
