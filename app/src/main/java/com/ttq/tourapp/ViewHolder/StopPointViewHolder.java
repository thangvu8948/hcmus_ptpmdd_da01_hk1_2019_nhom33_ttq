package com.ttq.tourapp.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;

import com.ttq.tourapp.Adapter.BaseViewHolder;
import com.ttq.tourapp.databinding.LayoutStopPointBinding;

public class StopPointViewHolder extends BaseViewHolder<LayoutStopPointBinding> {
    public StopPointViewHolder(@NonNull LayoutStopPointBinding dataBinding) {
        super(dataBinding);
    }

    @Override
    public View getLayoutParent() {
        return mDataBinding.layoutParent;
    }

    @Override
    public View getLayoutChecked() {
        return mDataBinding.layoutChecked;
    }
}
