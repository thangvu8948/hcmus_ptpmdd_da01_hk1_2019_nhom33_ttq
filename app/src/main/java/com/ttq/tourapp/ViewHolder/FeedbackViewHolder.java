package com.ttq.tourapp.ViewHolder;

import android.view.View;

import androidx.annotation.NonNull;

import com.ttq.tourapp.Adapter.BaseViewHolder;
import com.ttq.tourapp.databinding.LayoutFeedBackItemBinding;

public class FeedbackViewHolder extends BaseViewHolder<LayoutFeedBackItemBinding> {
    public FeedbackViewHolder(@NonNull LayoutFeedBackItemBinding dataBinding) {
        super(dataBinding);
    }

    @Override
    public LayoutFeedBackItemBinding getDataBinding() {
        return mDataBinding;
    }

    @Override
    public View getLayoutParent() {
        return mDataBinding.layoutParent;
    }
}
