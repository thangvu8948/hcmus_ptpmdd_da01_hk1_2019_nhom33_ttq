package com.ttq.tourapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ttq.tourapp.interfaces.APIServices;
import com.ttq.tourapp.models.GetOTPRequest;
import com.ttq.tourapp.models.ResetPasswordRequest;
import com.ttq.tourapp.models.ResetPasswordResponse;
import com.ttq.tourapp.models.VerifyCodeResponse;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResetPasswordActivity extends AppCompatActivity {

    EditText mNewPassword;
    EditText mVerifyCode;

    Button mGetVerifyCode;
    Button mResetPassword;

    APIServices service;

    private String userId;
    private String value;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        Intent intent = getIntent();
        userId = intent.getStringExtra("userId");
        value = intent.getStringExtra("value");
        type = intent.getStringExtra("type");

        mNewPassword = findViewById(R.id.edtNewPassword);
        mVerifyCode = findViewById(R.id.edtVerifyCode);

        mGetVerifyCode = findViewById(R.id.btnGetVerifyCode);
        mResetPassword = findViewById(R.id.btnResetPassword);

        mGetVerifyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://35.197.153.192:3000")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
                service = retrofit.create(APIServices.class);

                GetOTPRequest getOTPRequest = new GetOTPRequest(type,value);
                Call<VerifyCodeResponse> otpCodeResponseCall = service.getOTPCode(getOTPRequest);
                 otpCodeResponseCall.enqueue(new Callback<VerifyCodeResponse>() {
                     @Override
                     public void onResponse(Call<VerifyCodeResponse> call, Response<VerifyCodeResponse> response) {
                         int statusCode = response.code();
                         if(statusCode == 200)
                         {
                             Toast.makeText(ResetPasswordActivity.this,"Đã gửi",Toast.LENGTH_SHORT).show();
                         }
                     }

                     @Override
                     public void onFailure(Call<VerifyCodeResponse> call, Throwable t) {

                     }
                 });
                mResetPassword.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest(userId,mNewPassword.getText().toString(),mVerifyCode.getText().toString());
                        Call<JSONObject> resetPasswordResponseCall = service.resetPassword(resetPasswordRequest);
                        resetPasswordResponseCall.enqueue(new Callback<JSONObject>() {
                            @Override
                            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                                int statusCode = response.code();
                                if(statusCode == 200)
                                {
                                    Toast.makeText(ResetPasswordActivity.this,"Thành công",Toast.LENGTH_SHORT).show();

                                }
                            }

                            @Override
                            public void onFailure(Call<JSONObject> call, Throwable t) {

                            }
                        });
                    }
                });
            }
        });

    }
}
