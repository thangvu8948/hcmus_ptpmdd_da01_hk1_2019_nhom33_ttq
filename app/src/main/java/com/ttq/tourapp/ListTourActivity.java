package com.ttq.tourapp;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ttq.tourapp.interfaces.APIServices;
import com.ttq.tourapp.models.ListTourRequest;
import com.ttq.tourapp.models.ListTourResponse;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.Tour;
import com.ttq.tourapp.models.TourAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListTourActivity extends AppCompatActivity {

    ListView listViewTour;
    APIServices service;
    ArrayList<Tour> list = new ArrayList<>();
    private ArrayAdapter<Tour> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_tour);
        Button show = findViewById(R.id.btnShow);
        listViewTour = findViewById(R.id.ListTour);
        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowListTour();
            }
        });


        Toast.makeText(ListTourActivity.this, SharedPrefs.getInstance().get("access_token", String.class), Toast.LENGTH_SHORT).show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://35.197.153.192:3000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(APIServices.class);

    }

    public void ShowListTour()
    {

        final EditText rowperPage = findViewById(R.id.rowPerPage);
        EditText pageNumber = findViewById(R.id.pageNum);
        final String RowPerPage = rowperPage.getText().toString();
        String PageNum = pageNumber.getText().toString();
        String token = SharedPrefs.getInstance().get("access_token", String.class);
        final ListTourRequest listTourRequest = new ListTourRequest(RowPerPage, PageNum);
        Call<ListTourResponse> ListTourRequestCall = service.getListTour(token, RowPerPage, PageNum);

        Toast.makeText(ListTourActivity.this, ListTourRequestCall.request().header("authoriztion"), Toast.LENGTH_SHORT).show();
        ListTourRequestCall.enqueue(new Callback<ListTourResponse>() {
            @Override
            public void onResponse(Call<ListTourResponse> call, Response<ListTourResponse> response) {
                if (response.code() == 200) {
                    list = response.body().getTours();
                    try
                    {
                        TourAdapter adapter = new TourAdapter(
                                ListTourActivity.this,
                                R.layout.tour_layout,
                                list
                        );
                        listViewTour.setAdapter(adapter);
                        listViewTour.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Show Tour
                            }
                        });
                    }
                    catch (Exception e) {
                        Log.d("Lisst", "onResponse: " + e);
                    }

                }
            }

            @Override
            public void onFailure(Call<ListTourResponse> call, Throwable t) {
                Log.d("Lis Tour Activity", "tonFailure" + t.getMessage());
            }
        });

    }

}
