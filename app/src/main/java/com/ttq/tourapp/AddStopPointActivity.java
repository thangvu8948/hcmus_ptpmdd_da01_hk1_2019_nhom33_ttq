package com.ttq.tourapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.models.Extra;
import com.ttq.tourapp.models.RequestClass.Coordinate;
import com.ttq.tourapp.models.RequestClass.SuggestedStopPointsRequest;
import com.ttq.tourapp.models.ServiceType;
import com.ttq.tourapp.models.StopPoint;
import com.ttq.tourapp.models.StopPointMarker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddStopPointActivity extends MapActivity<StopPointMarker> {
    public static AddStopPointActivity instance;
    private ProgressDialog mProgressDialog;

    private ArrayList<StopPoint> suggestedStopPoints = null;
    private ArrayList<StopPointMarker> markerStopPoints = null;
    private ArrayList<StopPoint> selectedStopPoints = null;
    private int tourId;
    private String tourName;
    private SearchView mSearchView;
    private FloatingActionButton mMyLocationButton;

    private StopPointMarker searchMarker = null;
    private ArrayList<LatLng> listAdded = null;

    private LinearLayout infoLayout, editLayout, addLayout, removeLayout;
    private BottomSheetDialog bottomSheetDialog;
    private StopPoint selectedStopPoint;

    @Override
    int getLayoutId() {
        return R.layout.activity_add_stop_point;
    }

    @Override
    void onCreateActivity() {
        instance = this;

        Intent intent = getIntent();
        tourId = intent.getIntExtra(Extra.TOUR_ID, -1);
        tourName = intent.getStringExtra(Extra.TOUR_NAME);

        listAdded = new ArrayList<>();
        selectedStopPoints = getData();
        if(selectedStopPoints != null){
            for(int i = 0; i < selectedStopPoints.size(); i++){
                listAdded.add(new LatLng(selectedStopPoints.get(i).getLat(), selectedStopPoints.get(i).getLog()));
            }
        }

        markerStopPoints = new ArrayList<>();
        suggestedStopPoints = new ArrayList<>();

        mMyLocationButton = (FloatingActionButton) findViewById(R.id.btnMyLocation);
        mSearchView = (SearchView) findViewById(R.id.svLocation);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                showProgressLoading();

                Address address = searchLocation(query);
                setZoom(12f);
                animateCamera(address);

                if (address != null) {
                    StopPoint stopPoint = new StopPoint();
                    stopPoint.setLog(address.getLatitude());
                    stopPoint.setLog(address.getLongitude());
                    stopPoint.setServiceTypeId(ServiceType.OTHER);

                    if(searchMarker != null)
                        mClusterManager.removeItem(searchMarker);

                    searchMarker = new StopPointMarker();
                    searchMarker.setPosition(new LatLng(address.getLatitude(), address.getLongitude()));
                    searchMarker.setTitle(query);

                    mClusterManager.addItem(searchMarker);

                    dismissProgressLoading();
                }else {
                    dismissProgressLoading();
                    Toast.makeText(AddStopPointActivity.this, "Không tìm thấy", Toast.LENGTH_SHORT).show();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        createBottomSheetDialog();
    }

    private void createBottomSheetDialog(){
        if(bottomSheetDialog == null){
            View view = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_stop_point_option, null);
            editLayout = view.findViewById(R.id.editLinearLayout);
            editLayout.setVisibility(View.GONE);
            removeLayout = view.findViewById(R.id.removeLinearLayout);
            removeLayout.setVisibility(View.GONE);
            infoLayout = view.findViewById(R.id.infoLinearLayout);
            addLayout = view.findViewById(R.id.addLinearLayout);
            infoLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AddStopPointActivity.this, StopPointDetailActivity.class);
                    if(selectedStopPoint.getServiceId() != null)
                        intent.putExtra(Extra.SERVICE_ID, selectedStopPoint.getServiceId().intValue());
                    intent.putExtra(Extra.STOPPOINT_NAME, selectedStopPoint.getName());
                    intent.putExtra(Extra.STOPPOINT_ADDRESS, selectedStopPoint.getAddress());
                    intent.putExtra(Extra.STOPPOINT_MIN, selectedStopPoint.getMinCost());
                    intent.putExtra(Extra.STOPPOINT_MAX, selectedStopPoint.getMaxCost());
                    startActivity(intent);

                    bottomSheetDialog.dismiss();
                }
            });

            addLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(selectedStopPoint == null)
                        return;

                    setStopPointInfo(selectedStopPoint);
                    bottomSheetDialog.dismiss();
                }
            });

            bottomSheetDialog = new BottomSheetDialog(this);
            bottomSheetDialog.setContentView(view);
        }
    }

    @Override
    public void onMyMarkerClick(Marker marker) {
       // setStopPointInfo((StopPoint)marker.getTag());
    }

    @Override
    void onMapClick(LatLng latLng) {

    }

    @Override
    void onMapLongClick(final LatLng latLng) {
        // Tạo điểm dừng mới
        StopPoint stopPoint = new StopPoint();
        stopPoint.setLat(latLng.latitude);
        stopPoint.setLog(latLng.longitude);

        setStopPointInfo(stopPoint);
    }

    private ArrayList<StopPoint> getData(){
        ArrayList<StopPoint> data;
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra(Extra.BUNDLE);
        if(bundle == null)
            return new ArrayList<>();

        data = (ArrayList<StopPoint>)bundle.getSerializable(Extra.BUNDLE_ARRAY_LIST);
        return data;
    }

    private void getSuggestedStopPoints(){
        showProgressLoading();

        LatLng center = getMap().getCameraPosition().target;
        String token = APIClient.getInstance().getAccessToken();
        SuggestedStopPointsRequest suggested = new SuggestedStopPointsRequest(new Coordinate(center.latitude, center.longitude));

        Call<ResponseBody> call = APIClient.getInstance().getService().getSuggestStopPoints(token, suggested);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    handlingSuggestedStopPoints(response);
                } catch (IOException | JSONException e) {
                    Toast.makeText(AddStopPointActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    dismissProgressLoading();
                }

                dismissProgressLoading();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(AddStopPointActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handlingSuggestedStopPoints(Response<ResponseBody> response) throws IOException, JSONException {
        int statusCode = response.code();
        if(statusCode == 200){
            String body = response.body().string();
            JSONArray array = new JSONArray(new JSONObject(body).getString("stopPoints"));
            mClusterManager.clearItems();
            for (int i = 0; i < array.length(); i++){
                JSONObject object = array.getJSONObject(i);

                StopPoint stopPoint = new StopPoint();
                stopPoint.setId(null);
                stopPoint.setServiceId(object.getInt("id"));
                stopPoint.setName(object.getString("name"));
                stopPoint.setAddress(object.getString("address"));
                stopPoint.setProvinceId(object.getInt("provinceId"));
                stopPoint.setLat(object.getDouble("lat"));
                stopPoint.setLog(object.getDouble("long"));
                stopPoint.setMinCost(object.getLong("minCost"));
                stopPoint.setMaxCost(object.getLong("maxCost"));
                stopPoint.setServiceTypeId(object.getInt("serviceTypeId"));
                suggestedStopPoints.add(stopPoint);
            }

            mClusterManager.addItems(getMarkerStopPoints());
            mClusterManager.cluster();
        }else{
            String errorBody = response.errorBody().string();
            if(statusCode == 400){
                JSONArray array = new JSONArray(new JSONObject(errorBody).getString("message"));
                if (array.length() > 0) {
                    JSONObject json = array.getJSONObject(0);
                    String errorMsg = json.getString("msg");
                    Toast.makeText(AddStopPointActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                }
            }else{
                String errorMsg = new JSONObject(errorBody).getString("message");
                Toast.makeText(AddStopPointActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void setStopPointInfo(StopPoint stopPoint){
        if(listAdded.contains(new LatLng(stopPoint.getLat(),stopPoint.getLog()))){
            Toast.makeText(AddStopPointActivity.this, "Điểm dừng này đã được thêm trước đó", Toast.LENGTH_SHORT).show();
            return;
        }

        StopPointIn4Dialog dialog = new StopPointIn4Dialog(stopPoint);
        dialog.show(getSupportFragmentManager(), "stop point information dialog");
        dialog.setOnListener(new StopPointIn4Dialog.StopPointIn4DialogListener() {
            @Override
            public void create(StopPoint stopPoint) {
                selectedStopPoints.add(stopPoint);
                listAdded.add(new LatLng(stopPoint.getLat(),stopPoint.getLog()));
                Toast.makeText(AddStopPointActivity.this, String.format("Added: %d stop point(s)", selectedStopPoints.size()), Toast.LENGTH_LONG).show();
            }

            @Override
            public void cancel() {
                Toast.makeText(AddStopPointActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClusterItemClick(StopPointMarker item) {
        selectedStopPoint = item.getStopPoint();
        bottomSheetDialog.show();

        //StopPoint stopPoint = item.getStopPoint();
        //setStopPointInfo(stopPoint);
    }

    private void addStopPointAsMarker(StopPoint stopPoint){
        Marker marker = addMarker(new LatLng(stopPoint.getLat(), stopPoint.getLog()));
        marker.setTitle(stopPoint.getName());
        marker.setTag(stopPoint);

        switch (stopPoint.getServiceTypeId()){
            case ServiceType.RESTAURANT:
                marker.setIcon(getMarkerIconFromResource(R.drawable.img_restaurant));
                marker.setSnippet("Restaurant");
                break;
            case ServiceType.HOTEL:
                marker.setIcon(getMarkerIconFromResource(R.drawable.img_hotel));
                marker.setSnippet("Hotel");
                break;
            case ServiceType.REST_STATION:
                marker.setIcon(getMarkerIconFromResource(R.drawable.img_rest_station));
                marker.setSnippet("Rest Station");
            default:
                marker.setIcon(getMarkerIconFromResource(R.drawable.img_flag));
                marker.setSnippet("Other");
        }

        marker.showInfoWindow();
    }

    public void onClick_showStopPointsButton(View view) {
        showProgressLoading();

        Intent intent = new Intent(AddStopPointActivity.this, SetStopPointsActivity.class);
        intent.putExtra(Extra.TOUR_ID, tourId);
        intent.putExtra(Extra.TOUR_NAME, tourName);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Extra.BUNDLE_ARRAY_LIST, selectedStopPoints);
        intent.putExtra(Extra.BUNDLE, bundle);
        startActivity(intent);

        dismissProgressLoading();
    }

    public void onClick_myLocationButton(View view){
        showProgressLoading();

        getDeviceLocation(new DeviceLocationListener() {
            @Override
            public void onGetDeviceLocation(Location location) {
                animateCamera(location);

                dismissProgressLoading();
            }
        });
    }

    public void onClick_suggestedStopPointButton(View view){
        getSuggestedStopPoints();
    }

    private ArrayList<StopPointMarker> getMarkerStopPoints(){
        ArrayList<StopPointMarker> markers = new ArrayList<>();

        for(StopPoint item : suggestedStopPoints){
            boolean isDuplicate = false;
            for(StopPointMarker marker : markers){
                if(item.getLat() == marker.getPosition().latitude
                    && item.getLog() == marker.getPosition().longitude){
                    isDuplicate = true;
                    break;
                }
            }

            if(!isDuplicate){
                StopPointMarker marker = new StopPointMarker(item);
                markers.add(marker);
            }
        }

        return markers;
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private BitmapDescriptor getMarkerIconFromResource(int id){
        int height = 70;
        int width = 70;

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), id);
        Bitmap smallMarker = Bitmap.createScaledBitmap(bitmap, width, height, false);
        BitmapDescriptor smallMarkerIcon = BitmapDescriptorFactory.fromBitmap(smallMarker);

        return smallMarkerIcon;
    }

    void showProgressLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
        mProgressDialog = new ProgressDialog(AddStopPointActivity.this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.show();
    }

    void updateMessageProgressDialog(String message) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.setMessage(message);
        }
    }

    void dismissProgressLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }


}
