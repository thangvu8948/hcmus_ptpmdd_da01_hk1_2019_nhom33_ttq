package com.ttq.tourapp;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.view.ActionMode;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ttq.tourapp.Adapter.AdapterUtil;
import com.ttq.tourapp.Adapter.CommonAdapter;
import com.ttq.tourapp.Adapter.StopPointAdapter;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.databinding.ActivitySetStopPointsBinding;
import com.ttq.tourapp.models.Extra;
import com.ttq.tourapp.models.RequestClass.RemoveStopPointsRequest;
import com.ttq.tourapp.models.RequestClass.SetStopPointRequest;
import com.ttq.tourapp.models.StopPoint;
import com.ttq.tourapp.models.TourDetail;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetStopPointsActivity extends BaseActivity {
    ActivitySetStopPointsBinding binding;
    private ArrayList<StopPoint> stopPoints;
    private int tourId;
    private String tourName;
    private String hostId = null;
    private MaterialToolbar toolbar;
    private StopPointAdapter mAdapter;
    private AdapterUtil<StopPoint> mAdapterUtil;

    private LinearLayout infoLayout, editLayout, addLayout, removeLayout;
    private BottomSheetDialog bottomSheetDialog;
    private StopPoint selectedStopPoint;
    private int selectedIndex = -1;

    //private StopPointAdapter mAdapter;
    @Override
    void onCreateActivity() {
        Intent intent = getIntent();
        tourId = intent.getIntExtra(Extra.TOUR_ID, -1);
        tourName = intent.getStringExtra(Extra.TOUR_NAME);
        hostId = intent.getStringExtra(Extra.HOST_ID);
        stopPoints = getData();

        mAdapter = new StopPointAdapter(SetStopPointsActivity.this, stopPoints);
        mAdapter.setOnClickListener(new CommonAdapter.OnClickListener<StopPoint>() {
           @Override
           public void onItemClick(View view, StopPoint item, int pos) {
               if (mAdapter.getSelectedItemCount() > 0) {
                   mAdapterUtil.enableActionMode(pos);
               } else
               {
                   selectedIndex = pos;
                   selectedStopPoint = item;
                   bottomSheetDialog.show();
               }
           }

           @Override
           public void onItemLongClick(View view, StopPoint item, int pos) {
               mAdapterUtil.enableActionMode(pos);
           }
        });

        mAdapterUtil = new AdapterUtil<>(mAdapter);
        mAdapterUtil.setAdaterListener(new AdapterUtil.AdapterListener<StopPoint>() {
            @Override
            public ActionMode getActionMode(ActionMode.Callback callback) {
                return startSupportActionMode(callback);
            }

            @Override
            public void onDeletedItems(CommonAdapter adapter) {
                List<Integer> selectedItemPositions = mAdapter.getSelectedItems();
                ArrayList<Number> deleteIds = new ArrayList<>();

                for(int i = 0; i < selectedItemPositions.size(); i++){
                    Number id = ((StopPoint)adapter.getItem(i)).getId();
                    deleteIds.add(id);
                }

                requestRemoveStopPoints(deleteIds);
            }
        });


        binding = DataBindingUtil.setContentView(SetStopPointsActivity.this, R.layout.activity_set_stop_points);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(SetStopPointsActivity.this));

        binding.recyclerView.setAdapter(mAdapter);

        if(hostId == null || !hostId.equals(APIClient.getInstance().getUserId())){
            binding.addStopPointButton.setVisibility(View.GONE);
        }
        initToolBar();
        createBottomSheetDialog();
    }

    private void createBottomSheetDialog(){
        if(bottomSheetDialog == null){
            View view = LayoutInflater.from(this).inflate(R.layout.bottom_sheet_stop_point_option, null);
            infoLayout = view.findViewById(R.id.infoLinearLayout);
            editLayout = view.findViewById(R.id.editLinearLayout);
            removeLayout = view.findViewById(R.id.removeLinearLayout);
            addLayout = view.findViewById(R.id.addLinearLayout);
            addLayout.setVisibility(View.GONE);
            infoLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(SetStopPointsActivity.this, StopPointDetailActivity.class);
                    if(selectedStopPoint.getServiceId() != null) {
                        intent.putExtra(Extra.SERVICE_ID, selectedStopPoint.getServiceId().intValue());
                        intent.putExtra(Extra.STOPPOINT_NAME, selectedStopPoint.getName());
                        intent.putExtra(Extra.STOPPOINT_ADDRESS, selectedStopPoint.getAddress());
                        intent.putExtra(Extra.STOPPOINT_MIN, selectedStopPoint.getMinCost());
                        intent.putExtra(Extra.STOPPOINT_MAX, selectedStopPoint.getMaxCost());
                        startActivity(intent);
                    }else{
                        Toast.makeText(SetStopPointsActivity.this, "Điểm dừng này chưa được tạo, không thể xem chi tiết", Toast.LENGTH_SHORT).show();
                    }
                    bottomSheetDialog.dismiss();
                }
            });

            editLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(selectedStopPoint == null)
                        return;

                    updateStopPointInfo(selectedStopPoint);
                    bottomSheetDialog.dismiss();
                }
            });

            removeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(selectedStopPoint == null)
                        return;

                    if(selectedStopPoint.getId() != null){
                        requestRemoveStopPoint(selectedStopPoint.getId().intValue());
                    }

                    mAdapter.getItems().remove(selectedIndex);
                    mAdapter.notifyDataSetChanged();
                    selectedIndex = -1;
                    bottomSheetDialog.dismiss();
                }
            });


            bottomSheetDialog = new BottomSheetDialog(this);
            bottomSheetDialog.setContentView(view);
        }
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_save:
                try {
                    requestSetStopPoints();
                }catch (Exception e){
                    Toast.makeText(SetStopPointsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreatePanelMenu(int featureId, @NonNull Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_list_stop_point, menu);

        return super.onCreatePanelMenu(featureId, menu);
    }

    private void initToolBar(){
        toolbar = binding.appBarLayout.findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Set StopPoints");
        getSupportActionBar().setHomeAsUpIndicator(getDrawable(R.drawable.ic_arrow_back));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private ArrayList<StopPoint> getData(){
        ArrayList<StopPoint> data;
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra(Extra.BUNDLE);
        if(bundle == null)
            return new ArrayList<>();

        data = (ArrayList<StopPoint>)bundle.getSerializable(Extra.BUNDLE_ARRAY_LIST);
        return data;
    }

    private void updateStopPointInfo(StopPoint stopPoint){
        StopPointIn4Dialog dialog = new StopPointIn4Dialog(stopPoint);
        dialog.show(getSupportFragmentManager(), "stop point information dialog");
        dialog.setOnListener(new StopPointIn4Dialog.StopPointIn4DialogListener() {
            @Override
            public void create(StopPoint stopPoint) {
                Toast.makeText(SetStopPointsActivity.this,"Updated", Toast.LENGTH_LONG).show();
                mAdapterUtil.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void cancel() {
                Toast.makeText(SetStopPointsActivity.this, "Cancel", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void requestRemoveStopPoint(int stopPointId){
        showProgressLoading();
        String accessToken = APIClient.getInstance().getAccessToken();

        Call<ResponseBody> call = APIClient.getInstance().getService().removeStopPoint(accessToken, tourId, stopPointId);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                if(statusCode == 200){
                    Toast.makeText(SetStopPointsActivity.this, "Xoa thành công", Toast.LENGTH_SHORT).show();
                }else{
                    String errorBody = null;
                    try {
                        errorBody = response.errorBody().string();
                        String errorMsg = new JSONObject(errorBody).getString("message");
                        Toast.makeText(SetStopPointsActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(SetStopPointsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressLoading();
                Toast.makeText(SetStopPointsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void requestSetStopPoints(){
        showProgressLoading();
        String accessToken = APIClient.getInstance().getAccessToken();
        SetStopPointRequest serviceStopPoints = new SetStopPointRequest(tourId, stopPoints);

        Call<ResponseBody> call = APIClient.getInstance().getService().setStopPoints(accessToken, serviceStopPoints);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    handlingSetStopPoints(response);
                } catch (JSONException | IOException e) {
                    Toast.makeText(SetStopPointsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressLoading();
                Toast.makeText(SetStopPointsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void handlingSetStopPoints(Response<ResponseBody> response) throws JSONException, IOException {
        int statusCode = response.code();
        Log.d("Set Stop Points: ", "handlingSetStopPoints: " + statusCode);
        if(statusCode == 200){
            if(TourDetailActivity.instance != null)
                TourDetailActivity.instance.finish();

            if(AddStopPointActivity.instance != null)
                AddStopPointActivity.instance.finish();

            Toast.makeText(SetStopPointsActivity.this, "Thêm thành công", Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(SetStopPointsActivity.this, TourDetailActivity.class);
            intent.putExtra(Extra.TOUR_ID, tourId);
            // Làm sao để qua tab history
            startActivity(intent);


            finish();
        }else{
            String errorBody = response.errorBody().string();
            String errorMsg = new JSONObject(errorBody).getString("message");
            Toast.makeText(SetStopPointsActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    private void requestRemoveStopPoints(final ArrayList<Number> deleteIds){
        showProgressLoading();

        String token = APIClient.getInstance().getAccessToken();
        RemoveStopPointsRequest request = new RemoveStopPointsRequest(tourId, deleteIds);

        Call<ResponseBody> call = APIClient.getInstance().getService().removeStopPoints(token, request);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    int statusCode = response.code();
                    if(statusCode == 200){
                        Toast.makeText(SetStopPointsActivity.this, String.format("Deleted %d stop point(s)", deleteIds.size()), Toast.LENGTH_SHORT).show();
                    }else{
                        String errorBody = response.errorBody().string();
                        String msg = new JSONObject(errorBody).getString("message");
                        Toast.makeText(SetStopPointsActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    Toast.makeText(SetStopPointsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                dismissProgressLoading();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(SetStopPointsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onClick_addStopPoint(View view) {
        Intent intent = new Intent(SetStopPointsActivity.this, AddStopPointActivity.class);
        intent.putExtra(Extra.TOUR_ID, tourId);
        intent.putExtra(Extra.TOUR_NAME, tourName);

        Bundle bundle = new Bundle();
        bundle.putSerializable(Extra.BUNDLE_ARRAY_LIST, stopPoints);
        intent.putExtra(Extra.BUNDLE, bundle);

        startActivity(intent);
        finish();
    }
}
