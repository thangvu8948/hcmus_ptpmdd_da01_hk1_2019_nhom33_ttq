package com.ttq.tourapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public abstract class BaseActivity extends AppCompatActivity {
    boolean isNetworkConneted = false;
    private ProgressDialog mProgressDialog = null;
    private ConnectivityManager.OnNetworkActiveListener onNetworkActiveListener = null;

    abstract void onCreateActivity();

    void setOnNetworkActiveListener(ConnectivityManager.OnNetworkActiveListener onNetworkActiveListener) {
        this.onNetworkActiveListener = onNetworkActiveListener;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isNetworkConneted = isNetworkConneted();
        onCreateActivity();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mProgressDialog = null;
    }

    void showProgressLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.show();
    }

    void updateMessageProgressDialog(String message) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.setMessage(message);
        }
    }

    void dismissProgressLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    boolean isNetworkConneted() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        assert connectivityManager != null;
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        isNetworkConneted = networkInfo != null && networkInfo.isConnected();

        return isNetworkConneted;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
