package com.ttq.tourapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.ttq.tourapp.models.StopPointMarker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

abstract class MapActivity<T extends ClusterItem> extends FragmentActivity implements OnMapReadyCallback {

    public static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final float DEFAULT_ZOOM = 100f;
    private static final String KEY_LOCATION = "LOCATION";
    private final String TAG = "Google Map";

    // Map component
    GoogleMap mMap;
    SupportMapFragment mMapFragment;
    ClusterManager<T> mClusterManager = null;

    private Context mContext;
    private LatLng mDefaultLocation;
    private boolean mLocationPermissionGranted;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Geocoder mGeocoder;
    private Location mDeviceLocation;

    private float mZoom;

    abstract int getLayoutId();

    abstract void onCreateActivity();

    abstract void onMapClick(LatLng latLng);

    abstract void onMapLongClick(LatLng latLng);

    public void onMyMarkerClick(Marker marker){
        Toast.makeText(mContext, marker.getTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        mContext = MapActivity.this;
        mGeocoder = new Geocoder(mContext);
        mZoom = DEFAULT_ZOOM;
        mDeviceLocation = null;

        // Set default location
        if (savedInstanceState != null) {
            mDefaultLocation = savedInstanceState.getParcelable(KEY_LOCATION);
        } else {
            // ref: https://www.latlong.net/place/ho-chi-minh-city-vietnam-333.html
            LatLng hcmCity = new LatLng(10.762622, 106.660172);
            mDefaultLocation = hcmCity;
        }

        getLocationPermission();

        initMap();      // --> OnMapReady

        onCreateActivity();
    }

    private boolean isEnabledNetworkService() {
        boolean isEnabled = true;
        return isEnabled;
    }

    private boolean isEnabledLocationService() {
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        boolean isEnabled = false;

        try {
            isEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return isEnabled;
    }

    private void openLocationServerDialog() {
        new AlertDialog.Builder(mContext)
                .setMessage("Location not enabled")
                .setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        mContext.startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(R.string.Cancel, null)
                .show();
    }

    private void initMap() {
        if (mLocationPermissionGranted) {
            mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_fragment);
            assert mMapFragment != null;
            mMapFragment.getMapAsync(MapActivity.this); // this call onMapReady
        }
    }

    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }

        if (mLocationPermissionGranted) {
            mMap.setMyLocationEnabled(true);
        } else {
            mMap.setMyLocationEnabled(false);
        }
    }

    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                    initMap();
                } else {
                    // Trở về màn hình chính
                    finish();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady: Map is ready");
        Toast.makeText(getApplicationContext(), "Map is Ready", Toast.LENGTH_SHORT).show();
        mMap = googleMap;

        mClusterManager = new ClusterManager<T>(this, mMap);
        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<T>() {
            @Override
            public boolean onClusterItemClick(T t) {
                MapActivity.this.onClusterItemClick(t);
                return true;
            }
        });


        mMap.setOnCameraIdleListener(mClusterManager);
        mMap.setOnMarkerClickListener(mClusterManager);



        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                MapActivity.this.onMapClick(latLng);
            }
        });

        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                MapActivity.this.onMapLongClick(latLng);
            }
        });



        if(mMap == null)
            return;

        // Update UI
        updateLocationUI();

        // Check location service enabled;
        if (!isEnabledLocationService()) {
            // Go to default location
            moveCamera(mDefaultLocation);
            openLocationServerDialog();
        } else {
            getDeviceLocation();
        }
    }

    private void getDeviceLocation(){
        if(isEnabledLocationService() && isEnabledNetworkService()) {
            if (mFusedLocationProviderClient == null) {
                mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext);
            }

            Task<Location> task = mFusedLocationProviderClient.getLastLocation();
            task.addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if(location != null){
                        mDeviceLocation = location;
                        setZoom(16f);
                        moveCamera(mDeviceLocation);
                    }
                }
            });
        }
    }

    public void getDeviceLocation(final DeviceLocationListener listener){
        if(isEnabledLocationService() && isEnabledNetworkService()) {
            if (mFusedLocationProviderClient == null) {
                mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(mContext);
            }

            Task<Location> task = mFusedLocationProviderClient.getLastLocation();
            task.addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if(location != null){
                        listener.onGetDeviceLocation(location);
                    }
                }
            });
        }
    }

    public Address searchLocation(String location){
        List<Address> addressList = null;
        Address result = null;
        if(location != null && !location.equals("")){
            try{
                addressList = mGeocoder.getFromLocationName(location, 2);
                if(addressList != null && addressList.size() > 0){
                    result = addressList.get(0);
                }
            } catch (IOException e) {
                Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
        return result;
    }

    public Marker addMarker(LatLng latLng){
        return mMap.addMarker(new MarkerOptions().position(latLng));
    }

    public VisibleRegion getVisibleRegion(){
        return mMap.getProjection().getVisibleRegion();
    }

    public GoogleMap getMap(){return mMap;}

    public interface DeviceLocationListener{
        void onGetDeviceLocation(Location location);
    }

    /**
     *  Camera Wrap Function
     */
    public void setZoom(float zoom){
        mZoom = zoom;
    }

    public void moveCamera(LatLng latLng){
        if(latLng == null)
            return;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, mZoom));
    }

    public void moveCamera(Address address){
        if(address == null)
            return;
        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
        moveCamera(latLng);
    }

    public void moveCamera(Location location){
        if(location == null)
            return;

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        moveCamera(latLng);
    }

    public void animateCamera(LatLng latLng){
        if(latLng == null)
            return;
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, mZoom));
    }

    public void animateCamera(Address address){
        if(address == null)
            return;
        LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
        animateCamera(latLng);
    }

    public void animateCamera(Location location){
        if(location == null)
            return;

        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        animateCamera(latLng);
    }

    /**
     *  Cluster Utils
    */
    public void addMarker(T marker){
        mClusterManager.addItem(marker);
    }

    public void addMarkers(Collection<T> markers){
        mClusterManager.addItems(markers);
    }

    public void clearMarker(){
        mClusterManager.clearItems();
    }

    public void removeMarker(T marker){
        mClusterManager.removeItem(marker);
    }

    public void onClusterItemClick(T item){}
}
