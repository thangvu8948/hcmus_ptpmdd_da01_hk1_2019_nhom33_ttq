package com.ttq.tourapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.ttq.tourapp.Adapter.FeedbackAdapter;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.databinding.FeedbackBinding;
import com.ttq.tourapp.models.FeedbackInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedbackBottomDialogFragment extends BottomSheetDialogFragment {
    private Number id;
    private boolean isFeedbackServices;
    public static final String TAG = "FeedbackBottomDialog";
    private final String FEEDBACK = "feedbackList";
    private final String REVIEW = "reviewList";

    private int pageIndex = 1;
    private final int PAGE_SIZE = 10;
    private boolean isScrolling = false;
    private int totalItemCount, lastVisibleItem, currentItemCount;

    private FeedbackBinding binding;
    private FeedbackAdapter adapter;
    private ArrayList<FeedbackInfo> data;
    public static FeedbackBottomDialogFragment newInstance(Number id, boolean isFeedbackServices){
        return new FeedbackBottomDialogFragment(id, isFeedbackServices);
    }

    public FeedbackBottomDialogFragment(Number id, boolean isFeedbackServices) {
        this.id = id;
        this.isFeedbackServices = isFeedbackServices;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        data = new ArrayList<>();
        getFeedback();
        adapter = new FeedbackAdapter(getContext(), data);


        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);

        binding = DataBindingUtil.inflate(inflater, R.layout.layout_bottom_sheet_feedback, container, false);
        binding.recyclerView.setLayoutManager(linearLayoutManager);
        binding.recyclerView.setAdapter(adapter);
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                currentItemCount = linearLayoutManager.getChildCount();
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                if(isScrolling && (currentItemCount + lastVisibleItem) == totalItemCount){
                    isScrolling = false;
                    loadMore();
                }
            }
        });

        return binding.getRoot();
    }

    private void loadMore() {
        binding.progressBar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pageIndex++;
                getFeedback();
            }
        }, 3000);
    }

    private ArrayList<FeedbackInfo> getData() {
        ArrayList<FeedbackInfo> list = new ArrayList<>();

        FeedbackInfo feedback = new FeedbackInfo(1, "Duy Quang", "Very good", 4, 1577344765901L);
        list.add(feedback);
        list.add(feedback);
        list.add(feedback);
        list.add(feedback);
        list.add(feedback);
        list.add(feedback);
        return list;
    }

    private void getFeedback(){
        String token = APIClient.getInstance().getAccessToken();

        Call<ResponseBody> call = APIClient.getInstance().getService().getFeedbackServices(token, id, pageIndex, PAGE_SIZE);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int statusCode = response.code();
                try{
                    if(statusCode == 200){
                        String body = response.body().string();
                        if(isFeedbackServices){
                            JSONArray array = new JSONArray(new JSONObject(body).getString(FEEDBACK));
                            for(int i = 0; i < array.length(); i++){
                                JSONObject jsonObject = array.getJSONObject(i);
                                FeedbackInfo info = new FeedbackInfo();
                                info.setId(jsonObject.getInt("id"));
                                info.setName(jsonObject.getString("name"));
                                info.setFeedback(jsonObject.getString("feedback"));
                                info.setRating(jsonObject.getInt("point"));
                                info.setCreated(jsonObject.getLong("createdOn"));

                                data.add(info);
                                adapter.notifyDataSetChanged();
                            }
                        }
                        else{
                            JSONArray array = new JSONArray(new JSONObject(body).getString(REVIEW));
                            for(int i = 0; i < array.length(); i++){
                                JSONObject jsonObject = array.getJSONObject(i);
                                FeedbackInfo info = new FeedbackInfo();
                                info.setId(jsonObject.getInt("id"));
                                info.setName(jsonObject.getString("name"));
                                info.setFeedback(jsonObject.getString("review"));
                                info.setRating(jsonObject.getInt("point"));
                                info.setCreated(jsonObject.getLong("createdOn"));

                                data.add(info);
                                adapter.notifyDataSetChanged();
                            }
                        }
                        binding.progressBar.setVisibility(View.GONE);

                    }else{
                        String errorBody = response.errorBody().string();
                        Toast.makeText(getContext(), new JSONObject(errorBody).getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
