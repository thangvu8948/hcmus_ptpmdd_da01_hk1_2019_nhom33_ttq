package com.ttq.tourapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ttq.tourapp.Activity.SearchUser;
import com.ttq.tourapp.Adapter.CommonAdapter;
import com.ttq.tourapp.Adapter.MemberInfoAdapter;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.databinding.ActivityListMemberBinding;
import com.ttq.tourapp.models.Extra;
import com.ttq.tourapp.models.MemberInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListMemberActivity extends BaseActivity {
    private ActivityListMemberBinding binding;
    private ArrayList<MemberInfo> data;
    private MemberInfoAdapter adapter;
    private int tourId;
    @Override
    void onCreateActivity() {
        data = new ArrayList<>();
        tourId = getIntent().getIntExtra(Extra.TOUR_ID, -1);

        adapter = new MemberInfoAdapter(ListMemberActivity.this, data);
        adapter.setOnClickListener(new CommonAdapter.OnClickListener<MemberInfo>() {
            @Override
            public void onItemClick(View view, MemberInfo item, int pos) {

            }

            @Override
            public void onItemLongClick(View view, MemberInfo item, int pos) {

            }
        });

        getMember();

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ListMemberActivity.this, RecyclerView.VERTICAL, false);

        binding = DataBindingUtil.setContentView(ListMemberActivity.this, R.layout.activity_list_member);
        binding.recyclerView.setLayoutManager(linearLayoutManager);
        binding.recyclerView.setAdapter(adapter);

        String hostId = getIntent().getStringExtra(Extra.HOST_ID);
        String userId = APIClient.getInstance().getUserId();
        if(!hostId.equals(userId))
            binding.addMember.setVisibility(View.GONE);

        binding.addMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressLoading();
                Bundle bundle = new Bundle();
                bundle.putString("tourId", String.valueOf(tourId));
                Intent intent = new Intent(ListMemberActivity.this, SearchUser.class);
                intent.putExtras(bundle);
                startActivity(intent);
                dismissProgressLoading();
            }
        });
    }

    private void getMember(){
        String token = APIClient.getInstance().getAccessToken();

        Call<ResponseBody> call = APIClient.getInstance().getService().getTourInfo(token, tourId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                try {
                    if(code == 200){
                        String body = response.body().string();
                        JSONObject json = new JSONObject(body);
                        JSONArray array = new JSONArray(json.getString("stopPoints"));
                        for (int i = 0; i < array.length(); i++){
                            JSONObject object = array.getJSONObject(i);
                            MemberInfo info = new MemberInfo();
                            info.setId(object.getInt("id"));
                            info.setHost(object.getBoolean("isHost"));
                            info.setPhone(object.getString("phone"));
                            info.setName(object.getString("name"));

                            data.add(info);
                        }
                        adapter.notifyDataSetChanged();
                    }else{
                        String errorBody = response.errorBody().string();
                        String errorMsg = new JSONObject(errorBody).getString("message");
                        Toast.makeText(ListMemberActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                    Toast.makeText(ListMemberActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(ListMemberActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
