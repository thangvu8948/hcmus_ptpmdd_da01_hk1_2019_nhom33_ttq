package com.ttq.tourapp;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.databinding.ActivitySignUpBinding;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.SignInEmailRequest;
import com.ttq.tourapp.models.SignInEmailResponse;
import com.ttq.tourapp.models.SignInFBRequest;
import com.ttq.tourapp.models.SignInFBResponse;
import com.ttq.tourapp.models.SignUpEmailRequest;
import com.ttq.tourapp.models.UserInfoResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity {
    ActivitySignUpBinding binding;
    private SignUpEmailRequest mUser;
    CallbackManager callbackManager;

    @Override
    void onCreateActivity() {
        binding = DataBindingUtil.setContentView(SignUpActivity.this, R.layout.activity_sign_up);
        binding.btnSignInFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attempFBLogin();
            }
        });
        mUser = new SignUpEmailRequest();
        binding.setUser(mUser);
    }

    public void onClick_SignUpButton(View view) {
        mUser.setPassword(binding.edtPassword.getText().toString());
        showProgressLoading();

        Call<JSONObject> SignUpEmailResponseCall = APIClient.getInstance().getService().signUpByEmail(mUser);
        SignUpEmailResponseCall.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                try {
                    handlingSignUpResponse(response);
                } catch (JSONException e) {
                    Toast.makeText(SignUpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                } catch (IOException e) {
                    Toast.makeText(SignUpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                dismissProgressLoading();
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable throwable) {
                Toast.makeText(SignUpActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                dismissProgressLoading();
            }
        });
    }

    private void handlingSignUpResponse(Response<JSONObject> response) throws JSONException, IOException {
        int statusCode = response.code();
        Log.d("Sign Up", "onResponse" + statusCode);

        if (statusCode == 200) {
            JSONObject signUpEmailResponse = response.body();
            Log.d("Sign Up", String.format("status code: %s", statusCode));

            // Sign Up
            SignInByEmail(mUser.getEmail(), mUser.getPassword());
        } else {
            String errorBody = response.errorBody().string();
            if (statusCode == 400) {
                JSONArray array = new JSONArray(new JSONObject(errorBody).getString("message"));
                if(array.length() > 0){
                    JSONObject json = array.getJSONObject(0);;
                    String errorMsg = json.getString("msg");

                    Toast.makeText(SignUpActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                    Log.d("Sign Up", String.format("status code: %s", statusCode));
                }
            } else if (statusCode == 503) {
                String errorMsg = new JSONObject(errorBody).getString("message");
                Toast.makeText(SignUpActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                Log.d("Sign Up", String.format("status code: %s", statusCode));
            }
        }

    }

    public void onClick_LogInTextView(View view) {
        finish();
    }

    private void SignInByEmail(String email, String password){
        SignInEmailRequest request = new SignInEmailRequest();
        request.setEmailPhone(email);
        request.setPassword(password);

        Call<SignInEmailResponse> call = APIClient.getInstance().getService().getToken(request);

        call.enqueue(new Callback<SignInEmailResponse>() {
            @Override
            public void onResponse(Call<SignInEmailResponse> call, Response<SignInEmailResponse> response) {
                int statusCode = response.code();
                Log.d("Sign In", String.format("status code: %s", statusCode));

                try {
                    handlingSignInResponse(response);
                } catch (IOException e) {
                    Toast.makeText(SignUpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                } catch (JSONException e) {
                    Toast.makeText(SignUpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<SignInEmailResponse> call, Throwable t) {
                Toast.makeText(SignUpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handlingSignInResponse(Response<SignInEmailResponse> response) throws IOException, JSONException {
        int statusCode = response.code();
        if (statusCode == 200){
            SignInEmailResponse signInEmailResponse = response.body();
            String token = signInEmailResponse.getToken();
            String userId = signInEmailResponse.getUserId();
            SharedPrefs.getInstance().put(SharedPrefs.ACCESS_TOKEN, token);
            SharedPrefs.getInstance().put(SharedPrefs.USER_ID, userId);
            APIClient.getInstance().setAccessToken(token);

            getUserInfo();
        }else {
            String errorBody = response.errorBody().string();
            String errorMsg = new JSONObject(errorBody).getString("message");
            Toast.makeText(SignUpActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    private void getUserInfo(){
        String token = APIClient.getInstance().getAccessToken();
        Call<UserInfoResponse> call = APIClient.getInstance().getService().userInfo(token);

        call.enqueue(new Callback<UserInfoResponse>() {
            @Override
            public void onResponse(Call<UserInfoResponse> call, Response<UserInfoResponse> response) {
                int statusCode = response.code();
                Log.d("Sign In", String.format("status code: %s", statusCode));
                try {
                    handlingGetUserInfoResponse(response);
                } catch (IOException e) {
                    Toast.makeText(SignUpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                } catch (JSONException e) {
                    Toast.makeText(SignUpActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<UserInfoResponse> call, Throwable t) {
                Toast.makeText(SignUpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void handlingGetUserInfoResponse( Response<UserInfoResponse> response) throws IOException, JSONException {
        int statusCode = response.code();
        if(statusCode == 200){
            UserInfoResponse infoResponse = response.body();

            SharedPrefs.getInstance().put(SharedPrefs.USER_INFO, infoResponse);
            APIClient.getInstance().setUserId(String.valueOf(infoResponse.getId().intValue()));
            Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }else{
            String errorBody = response.errorBody().string();
            String errorMsg = new JSONObject(errorBody).getString("message");
            Toast.makeText(SignUpActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
        }
    }

    private void attempFBLogin() {
        try {
            callbackManager = CallbackManager.Factory.create();
            final AccessToken[] accessToken = {null};
            LoginManager.getInstance().registerCallback(callbackManager,
                    new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            SignInFBRequest signInFBRequest = new SignInFBRequest();
                            signInFBRequest.setAccessToken(AccessToken.getCurrentAccessToken().getToken());

                            Call<SignInFBResponse> signInFBResponseCall =  APIClient.getInstance().getService().getTokenByFB(signInFBRequest);

                            signInFBResponseCall.enqueue(new Callback<SignInFBResponse>() {
                                @Override
                                public void onResponse(Call<SignInFBResponse> call, Response<SignInFBResponse> response) {
                                    int statusCode = response.code();

                                    SignInFBResponse signInEmailResponse = response.body();
                                    String token = response.body().getToken();

                                    SharedPrefs.getInstance().put(SharedPrefs.ACCESS_TOKEN, token);
                                    getUserInfo(    );
                                    Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                    finish();
                                }

                                @Override
                                public void onFailure(Call<SignInFBResponse> call, Throwable throwable) {
                                    Log.d("Login Activity", "onFailure" + throwable.getMessage());
                                }
                            });
                        }

                        @Override
                        public void onCancel() {
                            Log.d("cancel", "cancel");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Log.d("err", error.toString());
                        }
                    });
        } catch (Exception e)  {

        }



    }
}

