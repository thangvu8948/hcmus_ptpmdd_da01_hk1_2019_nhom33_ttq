package com.ttq.tourapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.material.checkbox.MaterialCheckBox;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.models.HistoryTour;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.UpdateTourRequest;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateTourActivity extends AppCompatActivity {

    Button btnUpdate;
    TextInputEditText txtTourname;
    TextInputEditText txtMinCost, txtMaxCost;
    TextInputEditText txtAdults, txtChilds;
    TextInputEditText txtStart, txtEnd;
    MaterialCheckBox cbPrivate;
    RadioGroup radioGroup;
    RadioButton openBtn, closeBtn, cancelBtn, startedBtn;
    ImageButton startPick, endPick;
    String idTour = "";
    HistoryTour tour;
    void init() {
        Bundle bundle = getIntent().getExtras();
        String json = bundle.getString("tour");
        Gson gson = new Gson();
        Type type = new TypeToken<HistoryTour>() {}.getType();
        tour = gson.fromJson(json, type);
        idTour = bundle.getString("tourId");
        btnUpdate = findViewById(R.id.btnSubmitUpdateTour);

        txtTourname = findViewById(R.id.edt_name_tour_update);
        txtTourname.setText(tour.getName());

        txtMinCost = findViewById(R.id.edt_mincost_update);
        txtMinCost.setText(tour.getMinCost());

        txtMaxCost = findViewById(R.id.edt_maxcost_update);
        txtMaxCost.setText(tour.getMaxCost());

        txtAdults = findViewById(R.id.edt_update_adult);
        txtAdults.setText(tour.getAdults());

        txtChilds = findViewById(R.id.edt_update_child);
        txtChilds.setText(tour.getChilds());

        SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        Date convertedStartDate = new Date(Long.valueOf(tour.getStartDate()));
        Date convertedEndDate = new Date(Long.valueOf(tour.getEndDate()));

        String dateStart = simple.format(convertedStartDate);
        String dateEnd =  simple.format(convertedEndDate);
        txtStart = findViewById(R.id.edt_start_date_update);
        txtStart.setText(dateStart);

        txtEnd = findViewById(R.id.edt_end_date_update);
        txtEnd.setText(dateEnd);

        cbPrivate = findViewById(R.id.private_trip_check_box_update);
        cbPrivate.setChecked(tour.getIsPrivate());

        radioGroup = findViewById(R.id.groupStatus);
        cancelBtn = findViewById(R.id.radioCancel);
        openBtn = findViewById(R.id.radioOpen);
        closeBtn = findViewById(R.id.radioClosed);
        startedBtn = findViewById(R.id.radioStarted);
        switch (tour.getStatus()) {
            case 0: radioGroup.check(R.id.radioOpen); break;
            case -1: radioGroup.check(R.id.radioCancel); break;
            case 1: radioGroup.check(R.id.radioStarted); break;
            case 2: radioGroup.check(R.id.radioClosed); break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_tour);

        init();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = txtTourname.getText().toString();
                String start = txtStart.getText().toString();
                String end = txtEnd.getText().toString();

                if (name == null || start == null || end == null || name == "" || start == "" || end == "") {
                    Toast.makeText(getApplicationContext(), "Vui lòng điền tên tour và ngày bắt đầu, ngày kết thúc", Toast.LENGTH_SHORT).show();
                } else {
                    HandleUpdateTour();
                }

            }
        });
    }

    private void HandleUpdateTour() {
        String token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);

        final UpdateTourRequest request = new UpdateTourRequest();
        request.setId(idTour);
        String name = txtTourname.getText().toString();

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date startDate = new Date(), endDate = new Date();
        try {
            startDate = sdf.parse(txtStart.getText().toString());
            endDate = sdf.parse(txtEnd.getText().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long start = Long.valueOf(startDate.getTime());
        long end = Long.valueOf(endDate.getTime());
        int minCost = Integer.valueOf(txtMinCost.getText().toString());
        int maxCost = Integer.valueOf(txtMaxCost.getText().toString());
        int adults = Integer.valueOf(txtAdults.getText().toString());
        int childs = Integer.valueOf(txtChilds.getText().toString());
        final boolean isPrivate = cbPrivate.isChecked();
        int status;
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.radioCancel: status = -1; break;
            case R.id.radioOpen: status = 0; break;
            case R.id.radioStarted: status = 1; break;
            case R.id.radioClosed: status = 2; break;
            default: status = 0;
        }

        request.setName(name);
        request.setStatus(status);
        request.setAdults(adults);
        request.setChilds(childs);
        request.setEndDate(end);
        request.setStartDate(start);
        request.setMinCost(minCost);
        request.setMaxCost(maxCost);
        request.setPrivate(isPrivate);
        UserService service = APIClient.getInstance().getAdapter().create(UserService.class);

        Call<JSONObject> call = service.UpdateTour(token, request);
        call.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                int code = response.code();
                if (code == 200) {
                    Toast.makeText(getApplicationContext(), "Đã cập nhật " + isPrivate, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "Lỗi xảy ra. Error " + code + "\n" + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Lỗi kết nối", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onClick_DateEnd(View view) {
        setDatePicker(txtEnd);
    }
    public void onClick_DateStart(View view) {
        setDatePicker(txtStart);
    }
    private void setDatePicker(final EditText editText) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        editText.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

                    }
                }, day, month, year);
        datePickerDialog.show();

    }

}
