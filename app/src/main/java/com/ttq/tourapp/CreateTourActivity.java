package com.ttq.tourapp;

import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.google.android.material.appbar.MaterialToolbar;
import com.ttq.tourapp.Dialog.MyDatePickerDialog;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.databinding.ActivityCreateTourBinding;
import com.ttq.tourapp.models.CreateTourRequest;
import com.ttq.tourapp.models.Extra;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateTourActivity extends BaseActivity {
    ActivityCreateTourBinding binding;
    private CreateTourRequest createTourRequest;

    private MaterialToolbar toolbar;
    private MyDatePickerDialog mStartDateDialog, mEndDateDialog;

    @Override
    void onCreateActivity() {
        binding = DataBindingUtil.setContentView(CreateTourActivity.this, R.layout.activity_create_tour);

        createTourRequest = new CreateTourRequest();
        binding.setTourInfo(createTourRequest);
        initToolBar();
    }

    private void initToolBar(){
        toolbar = binding.appBarLayout.findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create New Tour");
        getSupportActionBar().setHomeAsUpIndicator(getDrawable(R.drawable.ic_arrow_back));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void handlingCreateTourResponse(Response<ResponseBody> response) throws JSONException, IOException {
        int statusCode = response.code();
        if (statusCode == 200) {
            String body = response.body().string();
            JSONObject object = new JSONObject(body);
            int tourID = object.getInt("id");

            Intent intent = new Intent(CreateTourActivity.this, AddStopPointActivity.class);
            intent.putExtra(Extra.TOUR_ID, tourID);
            intent.putExtra(Extra.TOUR_NAME, createTourRequest.getName());
            startActivity(intent);
            finish();
        } else {
            String errorBody = response.errorBody().string();
            if (statusCode == 400) {
                JSONArray array = new JSONArray(new JSONObject(errorBody).getString("message"));
                if (array.length() > 0) {
                    JSONObject json = array.getJSONObject(0);
                    String errorMsg = json.getString("msg");
                    Toast.makeText(CreateTourActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                }
            } else if (statusCode == 500) {
                String errorMsg = new JSONObject(errorBody).getString("message");
                Toast.makeText(CreateTourActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onClick_createTourButton(View view){
        showProgressLoading();
        //updateMessageProgressDialog("Creating");

        String token = APIClient.getInstance().getAccessToken();
        Call<ResponseBody> createTour = APIClient.getInstance().getService().createTour(token, createTourRequest);

        createTour.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                try {
                    handlingCreateTourResponse(response);
                } catch (JSONException | IOException e) {
                    Toast.makeText(CreateTourActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressLoading();
                Toast.makeText(CreateTourActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onClick_startDateButton(View view){
        if(mStartDateDialog == null){
            mStartDateDialog = new MyDatePickerDialog(CreateTourActivity.this, new MyDatePickerDialog.MyDatePickerListen() {
                @Override
                public void onDateSet(int year, int monthOfYear, int dayOfMonth) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    try {
                        createTourRequest.setStartDate(sdf.parse(String.format("%d-%d-%d",dayOfMonth,monthOfYear + 1,year)).getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        mStartDateDialog.show();
    }

    public void onClick_endDateButton(View view){
        if(mEndDateDialog == null){
            mEndDateDialog = new MyDatePickerDialog(CreateTourActivity.this, new MyDatePickerDialog.MyDatePickerListen() {
                @Override
                public void onDateSet(int year, int monthOfYear, int dayOfMonth) {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    try {
                        createTourRequest.setEndDate(sdf.parse(String.format("%d-%d-%d",dayOfMonth,monthOfYear + 1,year)).getTime());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        mEndDateDialog.show();
    }
}
