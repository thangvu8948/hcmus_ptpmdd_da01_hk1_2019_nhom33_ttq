package com.ttq.tourapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttq.tourapp.R;
import com.ttq.tourapp.ViewHolder.FeedbackViewHolder;
import com.ttq.tourapp.databinding.LayoutFeedBackItemBinding;
import com.ttq.tourapp.models.FeedbackInfo;

import java.util.ArrayList;

public class FeedbackAdapter extends CommonAdapter<FeedbackInfo, FeedbackViewHolder> {

    public FeedbackAdapter(Context context, ArrayList<FeedbackInfo> data) {
        super(context, data);
    }

    @Override
    int getItemLayoutId() {
        return R.layout.layout_feed_back_item;
    }

    @Override
    void setData(FeedbackViewHolder holder, FeedbackInfo data) {
        holder.getDataBinding().setFeedback(data);
    }

    @Override
    FeedbackViewHolder getViewHolder(LayoutInflater inflater, ViewGroup parent) {
        LayoutFeedBackItemBinding binding = LayoutFeedBackItemBinding.inflate(inflater, parent, false);

        return new FeedbackViewHolder(binding);
    }
}
