package com.ttq.tourapp.Adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;

import com.google.android.material.textfield.TextInputEditText;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MyBindingAdapter {

    @BindingAdapter("Tour:setStatus")
    public static void setTourStatus(TextView view, int status){
        String text = null;
        if(status == -1){
            text = "Trạng thái: Đã hủy";
        }else if(status == 0){
            text = "Trạng thái: Đang mở";
        }else if(status == 1){
            text = "Trạng thái: Đã bắt đầu";
        }else if(status == 2){
            text = "Trạng thái: Đã bị khóa";
        }

        if(text == null)
            view.setVisibility(View.GONE);

        view.setText(text);
    }

    @BindingAdapter("Tour:setName")
    public static void setTourName(TextView view, String name){
        if(TextUtils.isEmpty(name) || name == null)
            name = "Chưa cập nhật";

            view.setText(String.format("Tên: %s", name));
    }

    @BindingAdapter({"Tour:setAdults", "Tour:setChilds"})
    public static void setPeoples(TextView view, int adults, int childs){
        if(adults == 0 && childs == 0)
            view.setVisibility(View.GONE);

        view.setText(String.format("Người lớn: %d - Trẻ em: %d", adults, childs));
    }

    @BindingAdapter({"Tour:setStartDate", "Tour:setEndDate"})
    public static void setDate(TextView textView, long arrive, long leave){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date arriveDate = new Date(arrive);
        Date leaveDate = new Date(leave);

        String text = String.format("Thời gian: %s - %s",  sdf.format(arriveDate), sdf.format(leaveDate));
        textView.setText(text);
    }

    @BindingAdapter("android:setDate")
    public static void setDate(EditText editText, long value){
        if(value == 0){
            return;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date(value);

        editText.setText(sdf.format(date));
    }

    @BindingAdapter({"StopPoint:setArriveAt", "StopPoint:setLeaveAt"})
    public static void setDateTime(TextView view, long arrive, long leave){
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd-MM-yyyy");
        Date arriveDate = new Date(arrive);
        Date leaveDate = new Date(leave);

        String text = String.format("Thời gian: %s - %s", sdf.format(arriveDate), sdf.format(leaveDate));
        view.setText(text);
    }



    @BindingAdapter({"StopPoint:setMinCost", "StopPoint:setMaxCost"})
    public static void setCost(TextView view, long min, long max){
        if(min == 0 && max == 0){
            view.setText("Chi phí: Chưa cập nhật");
            return;
        }

        String strMin = String.format("%,d", min).replace(',', '.');
        String strMax = String.format("%,d", max).replace(',', '.');
        String text = String.format("Chi phí: %s - %s VNĐ",strMin, strMax);
        view.setText(text);
    }

    @BindingAdapter("StopPoint:setAddress")
    public static void setAddress(TextView view, String address){
        if(address == null || TextUtils.isEmpty(address))
            address = "Chưa cập nhật";
        view.setText(String.format("Địa chỉ: %s", address));
    }

    @BindingAdapter("android:setNumber")
    public static void setNumber(TextView view, long number){
        view.setText(String.format("%,d", number));
    }
}
