package com.ttq.tourapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.view.ViewGroup;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ttq.tourapp.R;

import java.util.ArrayList;
import java.util.List;

public class NotifyAdapter extends BaseAdapter {
    private Context mContext;
    private int mLayout;
    private ArrayList<String> mArrNotify;

    public NotifyAdapter(Context context, int layout, ArrayList<String> arrayNotify) {
        mContext = context;
        mLayout = layout;
        mArrNotify = arrayNotify;
    }

    @Override
    public int getCount() {
        if (mArrNotify.size() != 0 && !mArrNotify.isEmpty()) {
            return mArrNotify.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = inflater.inflate(mLayout,null);

        return convertView;
    }


}
