package com.ttq.tourapp.Adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;


public abstract class CommonAdapter<T, VH extends BaseViewHolder> extends RecyclerView.Adapter<VH>{
    private ArrayList<T> items;
    private SparseBooleanArray selectedItems;

    private int mCurrentIndexSelected = -1;
    private LayoutInflater mInflater;
    private Context mContext;
    protected OnClickListener<T> onClickListener;


    // Constructor
    public CommonAdapter(Context context, ArrayList<T> data){
        mContext = context;
        items = data;
        selectedItems = new SparseBooleanArray();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(this.mInflater == null){
            this.mInflater = LayoutInflater.from(parent.getContext());
        }

        return getViewHolder(mInflater, parent);
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, final int position) {
        final T item = items.get(position);

        setData(holder, item);

        holder.getLayoutParent().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onClickListener != null)
                    onClickListener.onItemClick(v, item, position);
            }
        });

        holder.getLayoutParent().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if(onClickListener != null)
                    onClickListener.onItemLongClick(v, item, position);
                return true;
            }
        });

        holder.getLayoutParent().setActivated(selectedItems.get(position, false));

        if(holder.getLayoutChecked() == null)
            return;

        toggleCheckedIcon(holder, position);
    }

    @Override
    public int getItemCount() {
        return items == null? 0 : items.size();
    }

    /**
     *  * Implement this method, return the specific item layout resource ID
     */
    abstract int getItemLayoutId();
    /**
     *  * Implement this method, set view holder
     */
    abstract void setData(VH holder, T data);
    abstract VH getViewHolder(LayoutInflater inflater, ViewGroup parent);

    /**
     * Util
     */
    public T getItem(int position){return  items == null? null : items.get(position);}

    public ArrayList<T> getItems(){return items;}

    private void toggleCheckedIcon(VH holder, int position) {
        if (selectedItems.get(position, false)) {
            holder.getLayoutChecked().setVisibility(View.VISIBLE);
            if (mCurrentIndexSelected == position) mCurrentIndexSelected = -1;
        } else {
            holder.getLayoutChecked().setVisibility(View.GONE);
            if (mCurrentIndexSelected == position) mCurrentIndexSelected = -1;
        }
    }

    public void toggleSelection(int pos) {
        mCurrentIndexSelected = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);
        } else {
            selectedItems.put(pos, true);
        }
        notifyItemChanged(pos);
    }

    public void clearSelections() {
        selectedItems.clear();
        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(selectedItems.size());
        for (int i = 0; i < selectedItems.size(); i++) {
            items.add(selectedItems.keyAt(i));
        }
        return items;
    }

    public void removeData(int position) {
        items.remove(position);
        mCurrentIndexSelected = -1;
    }

    public void deleteSelectedItems() {
        List<Integer> selectedItemPositions = getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            removeData(selectedItemPositions.get(i));
        }
        clearSelections();
        notifyDataSetChanged();
    }

    public void filterList(List<T> list){
        this.items = new ArrayList<>(list);
        notifyDataSetChanged();
    }

    public void resetCurrentIndex(){mCurrentIndexSelected = -1;}

    /**
     * Interface
     */
    public interface OnClickListener <T> {
        void onItemClick(View view, T item, int pos);
        void onItemLongClick(View view, T item, int pos);
    }

    public void setOnClickListener(OnClickListener<T> onClickListener) {
        this.onClickListener = onClickListener;
    }
}
