package com.ttq.tourapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ttq.tourapp.R;
import com.ttq.tourapp.StopPointDetailActivity;
import com.ttq.tourapp.interfaces.ItemClickListener;
import com.ttq.tourapp.models.Extra;
import com.ttq.tourapp.models.StopPoint;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class AllStopPointAdapter extends RecyclerView.Adapter<AllStopPointAdapter.RecyclerViewHolder> {

    Context mContext;
    ArrayList<StopPoint> data;

    public AllStopPointAdapter(Context context, ArrayList<StopPoint> data) {
        mContext = context;
        this.data = data;
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.stop_point_item, parent, false);

        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        String name = data.get(position).getName();
        final Long startDate =  data.get(position).getLeaveAt();
        Long endDate = data.get(position).getArriveAt();

        SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        Date convertedStartDate = new Date(Long.valueOf(startDate));
        Date convertedEndDate = new Date(Long.valueOf(endDate));
        final String date = simple.format(convertedStartDate) + " - " + simple.format(convertedEndDate);

        holder.txtName.setText(name);
        holder.txtDate.setText(date);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                //do something
                Intent intent = new Intent(mContext, StopPointDetailActivity.class);
                try {
                    intent.putExtra(Extra.SERVICE_ID, data.get(position).getServiceId().intValue());
                    intent.putExtra(Extra.STOPPOINT_NAME, data.get(position).getName());
                    intent.putExtra(Extra.STOPPOINT_ADDRESS, data.get(position).getAddress());
                    intent.putExtra(Extra.STOPPOINT_MIN, data.get(position).getMinCost());
                    intent.putExtra(Extra.STOPPOINT_MAX, data.get(position).getMaxCost());

                    mContext.startActivity(intent);
                } catch (Exception e) {
                    Toast.makeText(mContext, "Có lỗi", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
    @Override
    public int getItemCount() {
        return data.size();
    }

    public class RecyclerViewHolder  extends  RecyclerView.ViewHolder implements View.OnClickListener{
        private ItemClickListener itemClickListener;
        TextView txtName;
        TextView txtDate;

        public RecyclerViewHolder(View view) {
            super(view);
            txtName = view.findViewById(R.id.tvNameSP);
            txtDate = view.findViewById(R.id.tvDateSP);

            view.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }
        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }
    }
}
