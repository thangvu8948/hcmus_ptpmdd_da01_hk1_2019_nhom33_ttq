package com.ttq.tourapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.ttq.tourapp.R;
import com.ttq.tourapp.ViewHolder.StopPointViewHolder;

import com.ttq.tourapp.databinding.LayoutStopPointBinding;
import com.ttq.tourapp.models.StopPoint;
import java.util.ArrayList;

public class StopPointAdapter extends CommonAdapter<StopPoint, StopPointViewHolder> {

    public StopPointAdapter(Context context, ArrayList<StopPoint> data) {
        super(context, data);
    }

    @Override
    int getItemLayoutId() {
        return R.layout.layout_stop_point_item;
    }

    @Override
    void setData(StopPointViewHolder holder, StopPoint data) {
        holder.getDataBinding().setStopPoint(data);

        if(data.getMinCost() == 0 && data.getMaxCost() == 0){
            holder.getDataBinding().tvStopPointCost.setVisibility(View.GONE);
        }
    }

    @Override
    StopPointViewHolder getViewHolder(LayoutInflater inflater, ViewGroup parent) {
        LayoutStopPointBinding binding = LayoutStopPointBinding.inflate(inflater, parent, false);
        return new StopPointViewHolder(binding);
    }
}
