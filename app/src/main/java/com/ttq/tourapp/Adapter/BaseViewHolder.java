package com.ttq.tourapp.Adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;


public class BaseViewHolder<T extends ViewDataBinding> extends RecyclerView.ViewHolder {
    protected T mDataBinding;

    public BaseViewHolder(@NonNull T dataBinding) {
        super(dataBinding.getRoot());
        this.mDataBinding = dataBinding;
    }

    public T getDataBinding(){
        return this.mDataBinding;
    }

    public View getLayoutParent(){return null;}

    public View getLayoutChecked(){return null;}
}
