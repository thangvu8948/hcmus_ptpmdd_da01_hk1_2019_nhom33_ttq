package com.ttq.tourapp.Adapter;


import android.view.Menu;
import android.view.MenuItem;
import androidx.appcompat.view.ActionMode;
import com.ttq.tourapp.R;
import java.util.List;

public class AdapterUtil<T> {
    private ActionMode mActionMode;
    private ActionMode.Callback mActionModeCallback;
    private CommonAdapter mAdapter;

    public AdapterUtil(final CommonAdapter adapter) {
        this.mAdapter = adapter;
        this.mActionModeCallback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                mode.getMenuInflater().inflate(R.menu.menu_delete, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                int id = item.getItemId();
                if (id == R.id.menu_item_delete) {
                    if(mListener != null){
                        mListener.onDeletedItems(adapter);
                    }
                    deleteSelectedItems();
                    mode.finish();
                    return true;
                }
                return false;
            }
            @Override
            public void onDestroyActionMode(ActionMode mode) {
                mAdapter.clearSelections();
                mActionMode = null;
            }
        };
    }

////////
    public void toggleSelection(int position) {
        mAdapter.toggleSelection(position);
        int count = mAdapter.getSelectedItemCount();

        if (count == 0) {
            mActionMode.finish();
        } else {
            mActionMode.setTitle(String.valueOf(count));
            mActionMode.invalidate();
        }
    }

    public void enableActionMode(int pos) {
        if (mActionMode == null) {
            if(mListener == null)
                return;
            mActionMode = mListener.getActionMode(mActionModeCallback);
        }
        toggleSelection(pos);
    }

    public void deleteSelectedItems() {
        List<Integer> selectedItemPositions = mAdapter.getSelectedItems();
        for (int i = selectedItemPositions.size() - 1; i >= 0; i--) {
            mAdapter.removeData(selectedItemPositions.get(i));
        }
        mAdapter.notifyDataSetChanged();
    }

    public CommonAdapter getAdapter(){return mAdapter;}

    private AdapterListener<T> mListener;

    public interface AdapterListener<T>{
        ActionMode getActionMode(ActionMode.Callback callback);
        void onDeletedItems(CommonAdapter adapter);
    }

    public void setAdaterListener(AdapterListener listener){this.mListener = listener;}

    public void setActionMode(ActionMode actionMode){this.mActionMode = actionMode;}

    public ActionMode.Callback getActionModeCallBack(){return mActionModeCallback;}

    public ActionMode getActionMode(){return mActionMode;}
}