package com.ttq.tourapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttq.tourapp.R;
import com.ttq.tourapp.ViewHolder.TourViewHolder;
import com.ttq.tourapp.databinding.LayoutTourBinding;
import com.ttq.tourapp.models.TourDetail;

import java.util.ArrayList;

public class TourAdapter extends CommonAdapter<TourDetail, TourViewHolder> {
    public TourAdapter(Context context, ArrayList<TourDetail> data) {
        super(context, data);
    }

    @Override
    int getItemLayoutId() {
        return R.layout.layout_tour_item;
    }

    @Override
    void setData(TourViewHolder holder, TourDetail data) {
        holder.getDataBinding().setTour(data);
    }

    @Override
    TourViewHolder getViewHolder(LayoutInflater inflater, ViewGroup parent) {
        LayoutTourBinding binding = LayoutTourBinding.inflate(inflater, parent, false);

        return new TourViewHolder(binding);
    }
}
