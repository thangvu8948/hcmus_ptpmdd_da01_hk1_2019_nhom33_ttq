package com.ttq.tourapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.R;
import com.ttq.tourapp.Service.DownloadImageTask;
import com.ttq.tourapp.models.ResponseInvitationRequest;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.TourInviteInfo;

import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InviteAdapter extends BaseAdapter {
    Context myContext;
    int myLayout;
    Button acceptBtn, declineBtn;
    ArrayList<TourInviteInfo> inviteInfos;
    UserService service;
    public InviteAdapter(Context context, int layout, ArrayList<TourInviteInfo> listTour) {
        myContext = context;
        myLayout = layout;
        inviteInfos = listTour;
    }

    @Override
    public int getCount() {
        return inviteInfos.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        service = APIClient.getInstance().getAdapter().create(UserService.class);
        convertView = inflater.inflate(myLayout, null);
        ImageView avatar = convertView.findViewById(R.id.avatar);
        if (inviteInfos.get(position).getAvatar() != null) {
            new DownloadImageTask(avatar).execute(inviteInfos.get(position).getAvatar());
        }
        TextView message = convertView.findViewById(R.id.tv_content_notify);
        acceptBtn = convertView.findViewById(R.id.btn_accept_notify);
        declineBtn = convertView.findViewById(R.id.btn_decline_notify);
        message.setText(inviteInfos.get(position).getHostName() + " invited you to tour: " + inviteInfos.get(position).getName()) ;
        acceptBtn.setTag(position);
        declineBtn.setTag(position);
        acceptBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();
                HandleAccept(position);
            }
        });

        declineBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (Integer) v.getTag();
                HandleDecline(position);
            }
        });

        return convertView;
    }

    private void HandleDecline(final int position) {
        String token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);
        String id = String.valueOf(inviteInfos.get(position).getId());
        ResponseInvitationRequest responseInvitationRequest = new ResponseInvitationRequest();
        responseInvitationRequest.setAccepted(false);
        responseInvitationRequest.setTourId(id);
        Call<JSONObject> responseToInvitation = service.responseInvitation(token,"application/json", responseInvitationRequest);
        try {
            responseToInvitation.enqueue(new Callback<JSONObject>() {
                @Override
                public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                    int code = response.code();
                    if (code == 200) {
                        Toast.makeText(myContext, "Đã từ chối", Toast.LENGTH_SHORT).show();
                        inviteInfos.remove(position);
                        notifyDataSetChanged();
                    } else {
                        Toast.makeText(myContext, "Error: " + code, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<JSONObject> call, Throwable t) {
                    Toast.makeText(myContext, "Lỗi kết nối", Toast.LENGTH_SHORT).show();

                }
            });
        } catch (Exception er) {
            Toast.makeText(myContext, "Oopps. Something went wrong", Toast.LENGTH_SHORT).show();
        }

    }

    private void HandleAccept(final int position) {
        String token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);
        String id = String.valueOf(inviteInfos.get(position).getId());
        ResponseInvitationRequest responseInvitationRequest = new ResponseInvitationRequest();
        responseInvitationRequest.setAccepted(true);
        responseInvitationRequest.setTourId(id);
        Call<JSONObject> responseToInvitation = service.responseInvitation(token, "application/json",responseInvitationRequest);
        responseToInvitation.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                int code = response.code();
                if (code == 200) {
                    Toast.makeText(myContext, "Đã đồng ý", Toast.LENGTH_SHORT).show();
                    inviteInfos.remove(position);
                    notifyDataSetChanged();
                } else{
                    Toast.makeText(myContext, "Error: " + code, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Toast.makeText(myContext, "Lỗi kết nối", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
