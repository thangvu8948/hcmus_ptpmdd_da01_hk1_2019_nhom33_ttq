package com.ttq.tourapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ttq.tourapp.R;
import com.ttq.tourapp.ViewHolder.MemberInfoViewHolder;
import com.ttq.tourapp.databinding.LayoutListMemberItemBinding;
import com.ttq.tourapp.models.MemberInfo;

import java.util.ArrayList;

public class MemberInfoAdapter extends CommonAdapter<MemberInfo, MemberInfoViewHolder> {
    public MemberInfoAdapter(Context context, ArrayList<MemberInfo> data) {
        super(context, data);
    }

    @Override
    int getItemLayoutId() {
        return R.layout.layout_list_member_item;
    }

    @Override
    void setData(MemberInfoViewHolder holder, MemberInfo data) {
        holder.getDataBinding().setInfo(data);
    }

    @Override
    MemberInfoViewHolder getViewHolder(LayoutInflater inflater, ViewGroup parent) {
        LayoutListMemberItemBinding binding = LayoutListMemberItemBinding.inflate(inflater, parent, false);
        return new MemberInfoViewHolder(binding);
    }
}
