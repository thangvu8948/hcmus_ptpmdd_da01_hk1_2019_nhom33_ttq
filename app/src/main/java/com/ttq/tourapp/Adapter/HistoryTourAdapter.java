package com.ttq.tourapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.ttq.tourapp.Dialog.MyTripOptionsDialog;
import com.ttq.tourapp.R;
import com.ttq.tourapp.interfaces.ItemClickListener;
import com.ttq.tourapp.models.HistoryTour;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HistoryTourAdapter extends RecyclerView.Adapter<HistoryTourAdapter.RecyclerViewHolder> {

    private ArrayList<HistoryTour> data;
    private Context mContext;
    private FragmentActivity mActivity;
    private Fragment fragment;
    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.history_tour_item, parent, false);

        return new RecyclerViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull HistoryTourAdapter.RecyclerViewHolder holder, int position) {
        final String tourName = data.get(position).getName();
        String minCost = data.get(position).getMinCost();
        String maxCost = data.get(position).getMaxCost();
        String adult = data.get(position).getAdults();
        String child = data.get(position).getChilds();
        String startDate =  data.get(position).getStartDate();
        String endDate = data.get(position).getEndDate();

        String cost = minCost + " - " + maxCost + " VND";
        String people = "Người lớn" + ": " + adult + " - " + "Trẻ em" + ": " + child;

        SimpleDateFormat simple = new SimpleDateFormat("dd/MM/yyyy");
        Date convertedStartDate = new Date(Long.valueOf(startDate));
        Date convertedEndDate = new Date(Long.valueOf(endDate));

        final String date = simple.format(convertedStartDate) + " - " + simple.format(convertedEndDate);

        holder.txtName.setText(tourName);
        holder.txtCost.setText(cost);
        holder.txtPeople.setText(people);
        holder.txtDate.setText(date);

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position, boolean isLongClick) {
                Bundle bundle = new Bundle();
                bundle.putString("tourId", data.get(position).getId());
                bundle.putInt("position", position);
                bundle.putBoolean("isHost", data.get(position).isHost());
                bundle.putBoolean("isKicked", data.get(position).isKicked());
                bundle.putBoolean("isPrivate", data.get(position).getIsPrivate());
                Gson gson = new Gson();
                String tourToJsonString = gson.toJson(data.get(position));
                bundle.putString("tour", tourToJsonString);
                MyTripOptionsDialog dialog = new MyTripOptionsDialog(bundle);
                FragmentManager fm = mActivity.getSupportFragmentManager();
                dialog.setTargetFragment(fragment, 0);
                dialog.show(fm,"");
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public HistoryTourAdapter(Context context, ArrayList<HistoryTour> data, FragmentActivity activity, Fragment fragment) {
        mContext = context;
        mActivity = activity;
        this.data = data;
        this.fragment = fragment;
    }

    public class RecyclerViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtName;
        TextView txtCost;
        TextView txtPeople;
        TextView txtDate;
        private ItemClickListener itemClickListener;
        public RecyclerViewHolder(View view) {
            super(view);
            txtName = view.findViewById(R.id.tvNameH);
            txtCost = view.findViewById(R.id.tvCostH);
            txtPeople = view.findViewById(R.id.tvPeopleH);
            txtDate = view.findViewById(R.id.tvDateH);

            view.setOnClickListener(this);
        }
        //Tạo setter cho biến itemClickListenenr
        public void setItemClickListener(ItemClickListener itemClickListener)
        {
            this.itemClickListener = itemClickListener;
        }
        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }
    }

}
