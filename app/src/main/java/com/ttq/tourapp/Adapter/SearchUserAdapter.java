package com.ttq.tourapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.R;
import com.ttq.tourapp.Service.DownloadImageTask;
import com.ttq.tourapp.models.AddMemberRequest;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.UserInfoResponse;

import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.RecyclerViewHolder> {

    private ArrayList<UserInfoResponse> data;
    private String tourId = "";
    private Context mContext;
    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.search_user_item, parent, false);
        return new RecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerViewHolder holder, final int position) {
        String name = data.get(position).getFullName();
        String mail = data.get(position).getEmail();
        String phone = data.get(position).getPhone();
        holder.txtName.setText(name);
        if (mail != null && mail != "") holder.txtMail.setText(data.get(position).getEmail());
        if (phone != null && phone != "") holder.txtPhone.setText(data.get(position).getPhone());
        if (data.get(position).getAvatar() != null) {
            new DownloadImageTask(holder.avatar).execute(data.get(position).getAvatar());
        }

        holder.addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);
                UserService service = APIClient.getInstance().getAdapter().create(UserService.class);
                final AddMemberRequest request = new AddMemberRequest();
                request.setInvitedUserId(data.get(position).getId().toString());
                request.setIsInvited(true);
                request.setTourId(tourId);

                Call<JSONObject> call = service.addMember(token, request);
                call.enqueue(new Callback<JSONObject>() {
                    @Override
                    public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                        int code = response.code();
                        if (code == 200) {
                            Toast.makeText(mContext, "Đã gửi lời mời", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(mContext, "Gửi lời mời thất bại", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<JSONObject> call, Throwable t) {
                        Toast.makeText(mContext, "Lỗi kết nối", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
;    }


    public SearchUserAdapter(Context context, ArrayList<UserInfoResponse> data, String tourId) {
        this.data = data;
        this.tourId = tourId;
        this.mContext = context;
    }

    @Override
    public int getItemCount() {
        return data.size()  ;
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        TextView txtMail;
        TextView txtPhone;
        CircleImageView addBtn;
        CircleImageView avatar;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.tvNameUser);
            txtMail = (TextView) itemView.findViewById(R.id.tvMailUser);
            txtPhone = (TextView) itemView.findViewById(R.id.tvPhoneUser);
            addBtn = itemView.findViewById(R.id.addButton);
            avatar = itemView.findViewById(R.id.civSearchUserAva);
        }
    }
}
