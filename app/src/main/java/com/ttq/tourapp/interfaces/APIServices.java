package com.ttq.tourapp.interfaces;

import com.ttq.tourapp.models.GetOTPRequest;
import com.ttq.tourapp.models.ListTourResponse;
import com.ttq.tourapp.models.ListUserResponse;
import com.ttq.tourapp.models.ResetPasswordRequest;
import com.ttq.tourapp.models.SignInEmailRequest;
import com.ttq.tourapp.models.SignInEmailResponse;
import com.ttq.tourapp.models.SignInFBRequest;
import com.ttq.tourapp.models.SignInFBResponse;
import com.ttq.tourapp.models.SignInGGRequest;
import com.ttq.tourapp.models.SignInGGResponse;
import com.ttq.tourapp.models.UpdatePasswordRequest;
import com.ttq.tourapp.models.UpdatePasswordResponse;
import com.ttq.tourapp.models.VerifyCodeResponse;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIServices {

    @POST("/user/login")
    Call<SignInEmailResponse> getToken(@Body SignInEmailRequest signInEmailRequest);

    @POST("/user/login/by-facebook")
    Call<SignInFBResponse> getTokenByFB(@Body SignInFBRequest signInFBRequest);

    @POST("/user/login/by-facebook")
    Call<SignInGGResponse> getTokenByGG(@Body SignInGGRequest signInGGRequest);

    @GET("/tour/list")
    Call<ListTourResponse> getListTour(
            @Header("authorization") String authorization,
            @Query("rowPerPage") String rowPerPage,
            @Query("pageNum") String pageNum);

    @POST("/tour/create")
    Call<JSONObject> createTour(@Header("Authorization") String Authorization,
                                @Field("name") String name,
                                @Field("startDate") Number startDate,
                                @Field("endDate") Number endDate,
                                @Field("isPrivate") Boolean isPrivate,
                                @Field("adults") Number adults,
                                @Field("childs") Number childs,
                                @Field("minCost") Number minCost,
                                @Field("maxCost") Number maxCost,
                                @Field("avatar") String avatar);

    @GET("/user/search")
    Call<ListUserResponse> searchUsers(@Query("searchKey") String searchKey,
                                       @Query("pageIndex") String pageIndex,
                                       @Query("pageSize")  String pageSize);

    @POST("/user/request-otp-recovery")
    Call<VerifyCodeResponse> getOTPCode (@Body GetOTPRequest getOTPRequest);

    @POST("/user/verify-otp-recovery")
    Call<JSONObject> resetPassword(@Body ResetPasswordRequest resetPasswordRequest);

    @POST("/user/update-password")
    Call<UpdatePasswordResponse> updatePassword(@Header("Authorization") String Authorization,
                                                @Header("Content-Type") String contentType,
                                                @Body UpdatePasswordRequest updatePasswordRequest);


}