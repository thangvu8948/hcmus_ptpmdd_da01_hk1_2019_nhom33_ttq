package com.ttq.tourapp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.models.EditInfoRequest;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.UserInfoResponse;

import org.json.JSONObject;

import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditInfoActivity extends AppCompatActivity {

    EditText edtFullname;
    EditText edtEmail;
    EditText edtPhone;
    EditText edtDob;
    EditText edtAddr;
    RadioGroup groupGender;
    RadioButton radioButtonMale;
    RadioButton radioButtonFemale;
    ImageButton pickerDob;
    Button submit;
    private UserService userService;
    UserInfoResponse userInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_info);
        Toolbar toolbar = findViewById(R.id.tool_barEditInfo);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        init();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doEditInfo();
            }
        });
    }

    private void init() {


        edtEmail = findViewById(R.id.edt_email_edit);
        edtFullname = findViewById(R.id.edt_fullname_edit);
        edtPhone = findViewById(R.id.edt_phone_edit);
        edtDob = findViewById(R.id.dob_edit_text);
        edtAddr = findViewById(R.id.edt_addr_edit);
        groupGender = findViewById(R.id.groupGender);
        radioButtonMale = findViewById(R.id.radioMale);
        radioButtonFemale = findViewById(R.id.radioFemale);
        pickerDob = findViewById(R.id.dob_image_button);
        submit = findViewById(R.id.btnSubmitEditInfo);
        userInfo = SharedPrefs.getInstance().get(SharedPrefs.USER_INFO, UserInfoResponse.class);
        if (userInfo != null) {
            if (userInfo.getEmail() != null) edtEmail.setText(userInfo.getEmail());
            if (userInfo.getFullName() != null) edtFullname.setText(userInfo.getFullName());
            if (userInfo.getDOB() != null) edtDob.setText(userInfo.getDOB());
            if (userInfo.getPhone() != null) edtPhone.setText(userInfo.getPhone());
            if (userInfo.getGender() != null) {
                if (userInfo.getGender().intValue() == 0) {
                    radioButtonMale.setChecked(true);
                } else {
                    radioButtonFemale.setChecked(true);
                }
            }


        }

        userService = APIClient.getInstance().getAdapter().create(UserService.class);

    }

    public void onClick_DoB(View view) {
        setDatePicker(edtDob);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void doEditInfo() {
        final String fullname = edtFullname.getText().toString();
        final String email = edtEmail.getText().toString();
        final String addr = edtAddr.getText().toString();
        final String phone = edtPhone.getText().toString();
        final int gender = getGender();
        final String dob = edtDob.getText().toString();
        String token = "";
        token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);
        EditInfoRequest editInfoRequest = new EditInfoRequest();
        editInfoRequest.setFullName(fullname);
        editInfoRequest.setDob(dob);
        editInfoRequest.setEmail(email);
        editInfoRequest.setGender(gender);
        editInfoRequest.setPhone(phone);
        //      Toast.makeText(getApplicationContext(), token, Toast.LENGTH_SHORT).show();
        if (fullname == null || fullname.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Vui lòng nhập Tên đầy đủ", Toast.LENGTH_SHORT).show();
            return;
        }
        Call<JSONObject> editInfo = userService.editInfo(token, "application/json", editInfoRequest);
        editInfo.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                int code = response.code();
                if (code == 200) {
                    Toast.makeText(getApplicationContext(), "Cập nhật thành công", Toast.LENGTH_SHORT).show();
                    Intent returnIntent = new Intent();
                    UserInfoResponse userInfoResponse;
                    userInfoResponse = SharedPrefs.getInstance().get(SharedPrefs.USER_INFO, UserInfoResponse.class);
                    userInfoResponse.setFullName(fullname);
                    userInfoResponse.setEmail(email);
                    userInfoResponse.setGender(gender);
                    userInfoResponse.setPhone(phone);
                    userInfoResponse.setDOB(dob);
                    SharedPrefs.getInstance().put(SharedPrefs.USER_INFO, userInfoResponse);
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Cập nhật thất bại. Error " + code, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Cập nhật thất bại", Toast.LENGTH_SHORT).show();

            }
        });


    }

    private int getGender() {
        int checked = groupGender.getCheckedRadioButtonId();

        if (checked == R.id.radioMale) return 0;
        else return 1;
    }

    private void setDatePicker(final EditText editText) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        editText.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                    }
                }, year, month, day);
        datePickerDialog.show();
        //   return year + "-" + month+"-"+day;
    }
}
