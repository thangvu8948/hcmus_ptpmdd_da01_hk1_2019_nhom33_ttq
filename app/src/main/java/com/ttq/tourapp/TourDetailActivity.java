package com.ttq.tourapp;

import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.taufiqrahman.reviewratings.BarLabels;
import com.ttq.tourapp.Activity.SearchUser;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.databinding.ActivityTourDetailBinding;
import com.ttq.tourapp.models.Extra;
import com.ttq.tourapp.models.TourDetail;
import com.ttq.tourapp.models.StopPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TourDetailActivity extends BaseActivity {
    public static TourDetailActivity instance;

    private ActivityTourDetailBinding binding;


    private TourDetail detail = null;
    private ArrayList<StopPoint> stopPointArrayList = null;
    private String userId;

    int colors[] = new int[]{
            Color.parseColor("#1AB826"),
            Color.parseColor("#1AB826"),
            Color.parseColor("#1AB826"),
            Color.parseColor("#1AB826"),
            Color.parseColor("#1AB826")};

    int raters[] = new int[]{0, 0, 0, 0, 0};

    @Override
    void onCreateActivity() {
        instance = this;

        userId = APIClient.getInstance().getUserId();

        stopPointArrayList = new ArrayList<>();
        detail = new TourDetail();
        detail.setId(getIntent().getIntExtra(Extra.TOUR_ID, -1));

        binding = DataBindingUtil.setContentView(TourDetailActivity.this, R.layout.activity_tour_detail);
        binding.setUserId(userId);
        setTourInfo();
        setRating();


        binding.showListStopPointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressLoading();
                Intent intent = new Intent(TourDetailActivity.this, SetStopPointsActivity.class);

                intent.putExtra(Extra.TOUR_ID, detail.getId().intValue());
                intent.putExtra(Extra.HOST_ID, detail.getHostId());
                intent.putExtra(Extra.TOUR_NAME, detail.getName());
                Bundle bundle = new Bundle();
                bundle.putSerializable(Extra.BUNDLE_ARRAY_LIST, stopPointArrayList);
                intent.putExtra(Extra.BUNDLE, bundle);
                startActivity(intent);
                dismissProgressLoading();
            }
        });
        binding.ratingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TourDetailActivity.this, ReviewTourActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("tourId", String.valueOf(detail.getId()));
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        binding.inviteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TourDetailActivity.this, ListMemberActivity.class);
                intent.putExtra(Extra.TOUR_ID, detail.getId().intValue());
                intent.putExtra(Extra.HOST_ID, detail.getHostId());
                startActivity(intent);
            }
        });

        binding.showCmtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TourDetailActivity.this, CommentTourActivity.class);
                intent.putExtra(Extra.TOUR_ID, detail.getId().intValue());
                startActivity(intent);
            }
        });

        binding.cloneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressLoading();

                String token = APIClient.getInstance().getAccessToken();
                int id = detail.getId().intValue();

                Call<ResponseBody> call = APIClient.getInstance().getService().cloneTour(token, id);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        int code = response.code();
                        try {
                            if(code == 200){
                                int id = new JSONObject(response.body().string()).getInt("id");

                                Intent intent = new Intent(TourDetailActivity.this, TourDetailActivity.class);
                                intent.putExtra(Extra.TOUR_ID, id);
                                startActivity(intent);
                                finish();
                            }else {
                                String errorBody = response.errorBody().string();
                                String errorMsg = new JSONObject(errorBody).getString("message");
                                Toast.makeText(TourDetailActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                            Toast.makeText(TourDetailActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
            }
        });

        /*binding.goToMainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressLoading();
                Intent intent = new Intent(TourDetailActivity.this, HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });*/
    }

    private void setTourInfo() {
        String token = APIClient.getInstance().getAccessToken();
        int tourId = detail.getId().intValue();

        Call<ResponseBody> call = APIClient.getInstance().getService().getTourInfo(token, tourId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                try {
                    if(code == 200){
                        String body = response.body().string();
                        JSONObject json = new JSONObject(body);

                        detail.setHostId(json.getString("hostId"));
                        detail.setStatus(json.getInt("status"));
                        detail.setName(json.getString("name"));
                        detail.setMinCost(json.getLong("minCost"));
                        detail.setMaxCost(json.getLong("maxCost"));
                        detail.setAdults(json.getInt("adults"));
                        detail.setChilds(json.getInt("childs"));
                        detail.setPrivate(json.getBoolean("isPrivate"));
                        detail.setStartDate(json.getLong("startDate"));
                        detail.setEndDate(json.getLong("endDate"));

                        binding.setDetail(detail);
                        JSONArray array = new JSONArray(json.getString("stopPoints"));
                        for (int i = 0; i < array.length(); i++){
                            JSONObject object = array.getJSONObject(i);
                            StopPoint stopPoint = new StopPoint();
                            stopPoint.setId(object.getInt("id"));
                            stopPoint.setName(object.getString("name"));
                            stopPoint.setAddress(object.getString("address"));
                            stopPoint.setProvinceId(object.getInt("provinceId"));
                            stopPoint.setServiceId(object.getInt("serviceId"));
                            stopPoint.setLat(object.getDouble("lat"));
                            stopPoint.setLog(object.getDouble("long"));
                            stopPoint.setArriveAt(object.getLong("arrivalAt"));
                            stopPoint.setLeaveAt(object.getLong("leaveAt"));
                            stopPoint.setMinCost(object.getLong("minCost"));
                            stopPoint.setMaxCost(object.getLong("maxCost"));
                            stopPoint.setServiceTypeId(object.getInt("serviceTypeId"));

                            stopPointArrayList.add(stopPoint);
                        }
                    }else{
                        String errorBody = response.errorBody().string();
                        String errorMsg = new JSONObject(errorBody).getString("message");
                        Toast.makeText(TourDetailActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                    Toast.makeText(TourDetailActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TourDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setRating(){
        String token = APIClient.getInstance().getAccessToken();
        int tourId = detail.getId().intValue();

        Call<ResponseBody> call = APIClient.getInstance().getService().getReviewPoint(token, tourId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                try{
                    if(code == 200){
                        float rating = 0f;
                        long totalReview = 0;
                        int totalPoint = 0;
                        String body = response.body().string();
                        JSONArray array = new JSONArray(new JSONObject(body).getString("pointStats"));
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            int point = object.getInt("point");
                            raters[5 - point] = object.getInt("total");

                            totalReview+= raters[5 - point];
                            totalPoint+= raters[5 - point] * point;
                        }
                        if(totalReview == 0)
                            binding.setRating(0.0f);
                        else{
                            rating = (1.0f*totalPoint) / totalReview;
                            rating = Float.parseFloat(new DecimalFormat("##.#").format(rating).replace(',', '.'));
                            binding.setRating(rating);
                        }
                        binding.setTotalReview(totalReview);
                        binding.ratingReviews.createRatingBars(100, BarLabels.STYPE1, colors, raters);

                        dismissProgressLoading();
                    }else{
                        String errorBody = response.errorBody().string();
                        String errorMsg = new JSONObject(errorBody).getString("message");
                        Toast.makeText(TourDetailActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                    Toast.makeText(TourDetailActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(TourDetailActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
