package com.ttq.tourapp.Fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.ttq.tourapp.Adapter.CommonAdapter;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.R;
import com.ttq.tourapp.TourDetailActivity;
import com.ttq.tourapp.databinding.ListTourBinding;
import com.ttq.tourapp.models.Extra;
import com.ttq.tourapp.models.TourDetail;
import com.ttq.tourapp.Adapter.TourAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListTourV2Fragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ProgressDialog mProgressDialog = null;
    private ListTourBinding binding;
    private ArrayList<TourDetail> data;
    private TourAdapter adapter;

    private final int ROW_PER_PAGE = 20;
    private int pageIndex = 1;
    private boolean isScrolling;

    private TourDetail selectedTour;

    private BottomSheetDialog bottomSheetDialog;

    public static ListTourV2Fragment newInstance(String param1, String param2) {
        ListTourV2Fragment fragment = new ListTourV2Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        data = new ArrayList<>();
        getListTour();
        adapter = new TourAdapter(getContext(), data);
        adapter.setOnClickListener(new CommonAdapter.OnClickListener<TourDetail>() {
            @Override
            public void onItemClick(View view, TourDetail item, int pos) {
                selectedTour = item;
                bottomSheetDialog.show();
            }

            @Override
            public void onItemLongClick(View view, TourDetail item, int pos) {

            }
        });

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list_tour_v2, container, false);
        binding.recyclerView.setLayoutManager(linearLayoutManager);
        binding.recyclerView.setAdapter(adapter);

        /**
         *  Bắt sự kiện kéo hết trang
        * */
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL){
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int currentItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();
                if(isScrolling && (currentItemCount + lastVisibleItem) == totalItemCount){
                    isScrolling = false;
                    loadMore();
                }
            }
        });

        createBottomSheetDialog();
        return binding.getRoot();
    }

    /**
     * Tải thêm tour nếu load hết trang. Bằng cách pageIndex++
     */
    private void loadMore() {
        binding.progressBar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pageIndex++;        // Tắng số trang
                getListTour();
            }
        }, 2000);
    }

    private void getListTour(){
        String token = APIClient.getInstance().getAccessToken();

        Call<ResponseBody> call = APIClient.getInstance().getService().getListTour(token, pageIndex, ROW_PER_PAGE);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                int code = response.code();
                try {
                    if(code == 200){
                        String body = response.body().string();
                        JSONArray array = new JSONArray(new JSONObject(body).getString("tours"));

                        for(int i = 0; i < array.length(); i++){
                            JSONObject object = array.getJSONObject(i);

                            TourDetail info = new TourDetail();
                            info.setId(object.getInt("id"));
                            info.setStatus(object.getInt("status"));
                            info.setName(object.getString("name"));
                            info.setAdults(object.getInt("adults"));
                            info.setChilds(object.getInt("childs"));
                            info.setPrivate(object.getBoolean("isPrivate"));

                            String start = object.getString("startDate");
                            String end = object.getString("endDate");
                            String min = object.getString("minCost");
                            String max = object.getString("maxCost");


                            // Có mấy khứa để null @@@@@@@@@
                            if(start.equals("null"))
                                info.setStartDate(0L);
                            else
                                info.setStartDate(Long.parseLong(start));

                            if(end.equals("null"))
                                info.setEndDate(0L);
                            else
                                info.setEndDate(Long.parseLong(end));

                            if(min.equals("null"))
                                info.setMinCost(0L);
                            else
                                info.setMinCost(Long.parseLong(min));

                            if(max.equals("null"))
                                info.setMaxCost(0L);
                            else
                                info.setMaxCost(Long.parseLong(max));

                            data.add(info);
                        }
                        adapter.notifyDataSetChanged();
                    }else {
                        String errrorBody = response.body().string();
                        Toast.makeText(getContext(), new JSONObject(errrorBody).getString("message"), Toast.LENGTH_SHORT).show();
                    }

                    // Loadd xong ẩn progressBar
                    binding.progressBar.setVisibility(View.GONE);
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                    // Loadd xong ẩn progressBar
                    binding.progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     *  Menu chọn khi click 1 tour
    * */
    private void createBottomSheetDialog() {
        if (bottomSheetDialog == null) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.bottom_sheet_tour_option, null);

            LinearLayout removeLayout = view.findViewById(R.id.removeLinearLayout);
            removeLayout.setVisibility(View.GONE);

            LinearLayout infoLayout = view.findViewById(R.id.infoLinearLayout);
            infoLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProgressLoading();
                    // Chuyển sang màn hình xem chi tiết tour
                    Intent intent = new Intent(getActivity(), TourDetailActivity.class);
                    intent.putExtra(Extra.TOUR_ID, selectedTour.getId().intValue());
                    startActivity(intent);
                    dismissProgressLoading();
                    bottomSheetDialog.dismiss();
                }
            });

            LinearLayout addLayout = view.findViewById(R.id.addLinearLayout);
            addLayout.setVisibility(View.VISIBLE);
            addLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (selectedTour == null)
                        return;

                    showProgressLoading();

                    String token = APIClient.getInstance().getAccessToken();
                    int id = selectedTour.getId().intValue();

                    Call<ResponseBody> call = APIClient.getInstance().getService().cloneTour(token, id);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            int code = response.code();
                            try {
                                if (code == 200) {
                                    int id = new JSONObject(response.body().string()).getInt("id");

                                    Intent intent = new Intent(getContext(), TourDetailActivity.class);
                                    intent.putExtra(Extra.TOUR_ID, id);
                                    startActivity(intent);

                                } else {
                                    String errorBody = response.errorBody().string();
                                    String errorMsg = new JSONObject(errorBody).getString("message");
                                    Toast.makeText(getContext(), errorMsg, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException | IOException e) {
                                e.printStackTrace();
                                Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                dismissProgressLoading();
                            }

                            dismissProgressLoading();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {

                        }
                    });

                    bottomSheetDialog.dismiss();
                }
            });

            bottomSheetDialog = new BottomSheetDialog(getContext());
            bottomSheetDialog.setContentView(view);
        }
    }

    void showProgressLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
        mProgressDialog = new ProgressDialog(getContext());
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.show();
    }

    void updateMessageProgressDialog(String message) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.setMessage(message);
        }
    }

    void dismissProgressLoading() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
