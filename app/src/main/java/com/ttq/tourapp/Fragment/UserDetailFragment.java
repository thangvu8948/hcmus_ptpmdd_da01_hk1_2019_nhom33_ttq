package com.ttq.tourapp.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.facebook.AccessToken;
import com.google.android.material.textfield.TextInputEditText;
import com.ttq.tourapp.Dialog.UpdatePasswordDialog;
import com.ttq.tourapp.EditInfoActivity;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.R;
import com.ttq.tourapp.Service.DownloadImageTask;
import com.ttq.tourapp.SignInActivity;
import com.ttq.tourapp.interfaces.APIServices;
import com.ttq.tourapp.models.ReadPathUtil;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.UpdatePasswordRequest;
import com.ttq.tourapp.models.UpdatePasswordResponse;
import com.ttq.tourapp.models.UserInfoResponse;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UserDetailFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UserDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public UserDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserDetailFragment newInstance(String param1, String param2) {
        UserDetailFragment fragment = new UserDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    Button mSignOutButton;
    Button mChangePasswordButton;
    ImageView mAvatarView;
    UserInfoResponse userInfo;
    private static final int requestCodeChangeInfo = 1;
    private static final int GALLERY_REQUEST_CODE = 2;

    TextInputEditText currentPasswordText;
    TextInputEditText newPasswordText;
    TextInputEditText getNewPasswordAgainText;
    APIServices service;
    Retrofit retrofit = APIClient.getInstance().getAdapter();
    Button mUpdateInfoButton;
    TextView name;
    TextView email;
    final int changeSuccess  = 1;
    final int changeFailed  = 0;
    UserService userService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        userInfo = SharedPrefs.getInstance().get(SharedPrefs.USER_INFO, UserInfoResponse.class);
        userService = APIClient.getInstance().getAdapter().create(UserService.class);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);

        View view = inflater.inflate(R.layout.fragment_user_detail, container, false);
        mAvatarView = view.findViewById(R.id.avatarImg);
        mSignOutButton = view.findViewById(R.id.btnSignOut);
        mChangePasswordButton = view.findViewById(R.id.btnChangePassword);
        mUpdateInfoButton = view.findViewById(R.id.btnUpdateInfo);
        mSignOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
            }
        });
        name = view.findViewById(R.id.nameText);
        email = view.findViewById(R.id.mailText);


        Log.d("User Info: ", userInfo.toString());
        if (userInfo != null) {
            try {
                name.setText(userInfo.getFullName());
                email.setText(userInfo.getEmail());

                if (userInfo.getAvatar() != null) {
                    new DownloadImageTask(mAvatarView).execute(userInfo.getAvatar());
                }
            } catch (Exception e) {
                Log.d("error in show detail", e.getMessage());
            }

        }

        mUpdateInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditInfoActivity.class);
                startActivityForResult(intent, requestCodeChangeInfo);
            }
        });

        mChangePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePassword(v);
            }
        });
        mAvatarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setAvatar(v);
            }
        });
        return view;
    }
    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {

        if (requestCode == requestCodeChangeInfo) {
            if(resultCode == Activity.RESULT_OK){
                UserInfoResponse userInfoResponse = SharedPrefs.getInstance().get(SharedPrefs.USER_INFO, UserInfoResponse.class);
                name.setText(userInfoResponse.getFullName());
                email.setText(userInfoResponse.getEmail());
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
        if (requestCode == GALLERY_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getData();

                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    // Log.d(TAG, String.valueOf(bitmap));

                    mAvatarView.setImageBitmap(bitmap);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    //    byte[] imageBytes = baos.toByteArray();
                    // String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                            /*UpdateAvatar updateAvatar = new UpdateAvatar(encodedImage, getContext());
                            updateAvatar.execute();*/

                    String IMAGE_PATH;
                    IMAGE_PATH = ReadPathUtil.getPath(getActivity().getApplicationContext(), uri);
                    // Load image
                    File file = new File(IMAGE_PATH);
                    // create RequestBody instance from file
                    RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

                    // MultipartBody.Part is used to send also the actual file name
                    MultipartBody.Part body =
                            MultipartBody.Part.createFormData("file", file.getName(), requestFile);
                    Toast.makeText(getContext(), IMAGE_PATH, Toast.LENGTH_LONG).show();

                    String token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);
                    Call<JSONObject> updateAvatar = userService.updateAvatar(token, body);
                    updateAvatar.enqueue(new Callback<JSONObject>() {
                        @Override
                        public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                            int responseCode = response.code();
                            if (responseCode == 200) {
                                Toast.makeText(getContext(), "Cập nhật avatar thành công", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), "Cập nhật avatar thất bại " + responseCode + "\n" + response.body(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<JSONObject> call, Throwable t) {
                            Toast.makeText(getContext(), "Cập nhật avatar thất bại. Lỗi kết nối" + t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }//onAct
    private void changePassword(View view) {

        UpdatePasswordDialog dialog = new UpdatePasswordDialog();
        FragmentManager fm = getActivity().getSupportFragmentManager();
        dialog.show(fm, null);
    }


    /*private void doChangePassword() {

        if (newPasswordText.getText().toString() == getNewPasswordAgainText.getText().toString()) {
            Toast.makeText(getActivity(), "Xác nhận mật khẩu không chính xác", Toast.LENGTH_LONG).show();
            return;
        }

        UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest();
        int userId = SharedPrefs.getInstance().get(SharedPrefs.USER_ID, Integer.class);
        updatePasswordRequest.setUserId(userId);
        updatePasswordRequest.setCurrentPassword(currentPasswordText.getText().toString());
        updatePasswordRequest.setNewPassword(currentPasswordText.getText().toString());
        service = retrofit.create(APIServices.class);
        String accesstoken = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);
        Log.d("token", accesstoken);
        Call<UpdatePasswordResponse> updatePasswordCall = service.updatePassword(SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class), updatePasswordRequest);
        updatePasswordCall.enqueue(new Callback<UpdatePasswordResponse>() {
            @Override
            public void onResponse(Call<UpdatePasswordResponse> call, Response<UpdatePasswordResponse> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    Toast.makeText(getActivity(), "đổi mật khẩu thành công", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), "đổi mật khẩu thất bại 1", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UpdatePasswordResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "đổi mật khẩu thất bại 2", Toast.LENGTH_LONG).show();
            }
        });

    }*/

    private void signOut() {
        SharedPrefs.getInstance().put(SharedPrefs.ACCESS_TOKEN, "");
        SharedPrefs.getInstance().put(SharedPrefs.USER_ID, "");
        SharedPrefs.getInstance().put(SharedPrefs.USER_INFO, null);
        AccessToken.setCurrentAccessToken(null);

        Intent intent = new Intent(getActivity(), SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().finish();
        return;
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_user_detail, menu);
    }

    private void checkPermistion() {
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);
            }
        }
    }

    public void setAvatar(View view) {
        //Create an Intent with action as ACTION_PICK
        checkPermistion();
        Intent intent = new Intent();
// Show only images, no videos or anything else
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
// Always show the chooser (if there are multiple options available)
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE);
    }



    /*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
