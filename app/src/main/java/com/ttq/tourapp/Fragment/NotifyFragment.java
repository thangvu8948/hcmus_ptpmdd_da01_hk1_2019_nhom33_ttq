package com.ttq.tourapp.Fragment;


import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.ttq.tourapp.Adapter.InviteAdapter;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.R;
import com.ttq.tourapp.models.Invitation;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.TourInviteInfo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotifyFragment extends Fragment {

    UserService service;
    ListView lvNotify;
    ArrayList<TourInviteInfo> list;

    public NotifyFragment() {
        // Required empty public constructor
    }

    void init(View view){
        service = APIClient.getInstance().getAdapter().create(UserService.class);
        lvNotify = view.findViewById(R.id.listNotify);
        list = new ArrayList<>();

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notify, container, false);
        init(view);
        ShowNotifies(view);

        return view;
    }

    private void HandleDecline() {
    }

    private void HandleAccept() {
        String token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);
    }

    public void ShowNotifies(View view) {
        String page = "1";
        String pageSize = "100";

        String token = SharedPrefs.getInstance().get("access_token", String.class);
        Call<Invitation> invitation = service.getInvitation(token, page, pageSize);

     //   Toast.makeText(getActivity(), invitation.request().header("authoriztion"), Toast.LENGTH_SHORT).show();
        invitation.enqueue(new Callback<Invitation>() {
            @Override
            public void onResponse(Call<Invitation> call, Response<Invitation> response) {
                if (response.code() == 200) {
                    list = response.body().getTours();
                    try {
                        InviteAdapter adapter = new InviteAdapter(
                                getActivity(),
                                R.layout.notify_item,
                                list
                        );
                        lvNotify.setAdapter(adapter);
                        lvNotify.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Show Tour
                            }
                        });
                    } catch (Exception e) {
                        Log.d("Lisst", "onResponse: " + e);
                    }

                }
            }

            @Override
            public void onFailure(Call<Invitation> call, Throwable t) {
                Log.d("Lis Tour Activity", "tonFailure" + t.getMessage());
            }
        });

    }

}
