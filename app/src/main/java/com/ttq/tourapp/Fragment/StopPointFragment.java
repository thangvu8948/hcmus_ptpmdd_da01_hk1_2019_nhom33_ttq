package com.ttq.tourapp.Fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ttq.tourapp.Adapter.AllStopPointAdapter;
import com.ttq.tourapp.AddStopPointActivity;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.R;
import com.ttq.tourapp.models.AllStopPointsRequest;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.StopPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class StopPointFragment extends Fragment {

    SearchView searchView;
    RecyclerView recyclerView;
    ArrayList<StopPoint> data;
    UserService service;
    AllStopPointAdapter adapter;
    Button showAllBtn;
    int pageIndex = 1;
    int perPage = 100;
    boolean isScrolling = false;
    boolean isSearching = false;
    ProgressBar progressBar;
    int totals = 0;
    String querySearch = "";
    public StopPointFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_stop_point, container, false);

        data = new ArrayList<>();
        searchView = view.findViewById(R.id.svSearchSP);
        recyclerView = view.findViewById(R.id.ListSP);
        showAllBtn = view.findViewById(R.id.btnShowallSP);
        progressBar = view.findViewById(R.id.spProgress);
        service = APIClient.getInstance().getAdapter().create(UserService.class);
        adapter = new AllStopPointAdapter(getContext(), data);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        ShowAllStopPoints();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int currentItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int lastVisibleItem = layoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItemCount + lastVisibleItem) == totalItemCount) {
                    isScrolling = false;
                    loadMore();
                }
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                isSearching = true;
                pageIndex = 1;
                querySearch = query;
                data.clear();
                Search(query);
                adapter.notifyDataSetChanged();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        showAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSearching = false;
                pageIndex = 1;
                ShowAllStopPoints();
            }
        });
        return view;
    }

    private void loadMore() {
        progressBar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isSearching) {
                    pageIndex++;
                    Search(querySearch);
                }
            }
        }, 2000);
    }

    private void Search(String query) {
        String token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN,String.class);

        Call<ResponseBody> call = service.searchStopPoints(token, query, pageIndex, perPage);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    handlingSuggestedStopPoints(response);
                } catch (IOException | JSONException e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void ShowAllStopPoints() {
        String token = SharedPrefs.getInstance().get(SharedPrefs.ACCESS_TOKEN, String.class);
        AllStopPointsRequest request = new AllStopPointsRequest();
        Call<ResponseBody> call = service.getStopPoints(token, request);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    handlingSuggestedStopPoints(response);
                } catch (IOException | JSONException e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.INVISIBLE);


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    private void handlingSuggestedStopPoints(Response<ResponseBody> response) throws IOException, JSONException {
        int statusCode = response.code();
        if(statusCode == 200){
            String body = response.body().string();
            JSONArray array = new JSONArray(new JSONObject(body).getString("stopPoints"));
            for (int i = 0; i < array.length(); i++){
                JSONObject object = array.getJSONObject(i);

                StopPoint stopPoint = new StopPoint();
                stopPoint.setId(null);
                stopPoint.setServiceId(object.getInt("id"));
                stopPoint.setName(object.getString("name"));
                stopPoint.setAddress(object.getString("address"));
                stopPoint.setProvinceId(object.getInt("provinceId"));
                stopPoint.setLat(object.getDouble("lat"));
                stopPoint.setLog(object.getDouble("long"));
                stopPoint.setMinCost(object.getLong("minCost"));
                stopPoint.setMaxCost(object.getLong("maxCost"));
                stopPoint.setServiceTypeId(object.getInt("serviceTypeId"));

                data.add(stopPoint);
                adapter.notifyDataSetChanged();
            }
            progressBar.setVisibility(View.INVISIBLE);


        }else{
            String errorBody = response.errorBody().string();
            if(statusCode == 400){
                JSONArray array = new JSONArray(new JSONObject(errorBody).getString("message"));
                if (array.length() > 0) {
                    JSONObject json = array.getJSONObject(0);
                    String errorMsg = json.getString("msg");
                    Toast.makeText(getContext(), errorMsg, Toast.LENGTH_SHORT).show();
                }
            }else{
                String errorMsg = new JSONObject(errorBody).getString("message");
                Toast.makeText(getContext(), errorMsg, Toast.LENGTH_SHORT).show();
            }
        }
        progressBar.setVisibility(View.INVISIBLE);

    }

}
