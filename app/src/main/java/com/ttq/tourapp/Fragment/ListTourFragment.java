package com.ttq.tourapp.Fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ttq.tourapp.CreateTourActivity;
import com.ttq.tourapp.R;
import com.ttq.tourapp.interfaces.APIServices;
import com.ttq.tourapp.models.ListTourRequest;
import com.ttq.tourapp.models.ListTourResponse;
import com.ttq.tourapp.models.SharedPrefs;
import com.ttq.tourapp.models.Tour;
import com.ttq.tourapp.models.TourAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListTourFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListTourFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListTourFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ListView listViewTour;
    APIServices service;
    ArrayList<Tour> list = new ArrayList<>();
    EditText rowperPage;
    EditText pageNumber;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private ArrayAdapter<Tour> adapter;

    public ListTourFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ListTourFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ListTourFragment newInstance(String param1, String param2) {
        ListTourFragment fragment = new ListTourFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_tour, container, false);

        Button show = view.findViewById(R.id.btnShow);
        FloatingActionButton mAddTourBtn = view.findViewById(R.id.floatingActionButton2);
        listViewTour = view.findViewById(R.id.ListTour);
        rowperPage = view.findViewById(R.id.rowPerPage);
        pageNumber = view.findViewById(R.id.pageNum);

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowListTour(v);
            }
        });

        mAddTourBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreateTourActivity.class);
                startActivity(intent);
                return;
            }
        });
        Toast.makeText(getActivity(), SharedPrefs.getInstance().get("access_token", String.class), Toast.LENGTH_SHORT).show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://35.197.153.192:3000")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(APIServices.class);


        return view;
    }


    public void ShowListTour(View view) {
        String RowPerPage = "";
        String PageNum = "";
        try {
            RowPerPage = rowperPage.getText().toString();
            PageNum = pageNumber.getText().toString();
        } catch (Exception e) {
            Toast.makeText(getActivity(), "Null param", Toast.LENGTH_SHORT).show();

        }

        String token = SharedPrefs.getInstance().get("access_token", String.class);
        final ListTourRequest listTourRequest = new ListTourRequest(RowPerPage, PageNum);
        Call<ListTourResponse> ListTourRequestCall = service.getListTour(token, RowPerPage, PageNum);

        Toast.makeText(getActivity(), ListTourRequestCall.request().header("authoriztion"), Toast.LENGTH_SHORT).show();
        ListTourRequestCall.enqueue(new Callback<ListTourResponse>() {
            @Override
            public void onResponse(Call<ListTourResponse> call, Response<ListTourResponse> response) {
                if (response.code() == 200) {
                    list = response.body().getTours();
                    try {
                        TourAdapter adapter = new TourAdapter(
                                getActivity(),
                                R.layout.tour_layout,
                                list
                        );
                        listViewTour.setAdapter(adapter);
                        listViewTour.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Show Tour
                            }
                        });
                    } catch (Exception e) {
                        Log.d("Lisst", "onResponse: " + e);
                    }

                }
            }

            @Override
            public void onFailure(Call<ListTourResponse> call, Throwable t) {
                Log.d("Lis Tour Activity", "tonFailure" + t.getMessage());
            }
        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
