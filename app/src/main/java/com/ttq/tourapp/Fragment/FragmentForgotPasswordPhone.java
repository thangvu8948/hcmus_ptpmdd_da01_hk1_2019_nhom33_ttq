package com.ttq.tourapp.Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;

import com.ttq.tourapp.R;
import com.ttq.tourapp.ResetPasswordActivity;
import com.ttq.tourapp.interfaces.APIServices;
import com.ttq.tourapp.models.ListUserResponse;
import com.ttq.tourapp.models.Users;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FragmentForgotPasswordPhone extends Fragment {
    EditText mForgotPasswordPhone;
    Button mSearchButton;
    APIServices service;
    ArrayList<Users> list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_forgot_password_phone, container, false);
        mForgotPasswordPhone = view.findViewById(R.id.edtForgotPasswordPhone);
        mSearchButton = view.findViewById(R.id.btnSearchPhoneButton);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://35.197.153.192:3000")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                String pageIndex = "1";
                String pageSize = "1";
                service = retrofit.create(APIServices.class);

                Call<ListUserResponse> usersCall = service.searchUsers(mForgotPasswordPhone.getText().toString(), pageIndex, pageSize);
                usersCall.request();
                usersCall.enqueue(new Callback<ListUserResponse>() {
                    @Override
                    public void onResponse(Call<ListUserResponse> call, Response<ListUserResponse> response) {
                        int statusCode = response.code();
                        if (statusCode == 200) {
                            //mForgotPasswordPhone.setText(""+statusCode);

                            list = response.body().getUsers();
                            String total = response.body().getTotal();
                            if (total.equals("0")) {
                                mForgotPasswordPhone.setText("");
                                mForgotPasswordPhone.setHint("Không tìm thấy số điện thoại.");
                            } else {
                                Intent intent = new Intent(view.getContext(), ResetPasswordActivity.class);
                                intent.putExtra("type", "phone");
                                intent.putExtra("value", list.get(0).getPhone());
                                intent.putExtra("userId", list.get(0).getId());
                                startActivity(intent);
                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<ListUserResponse> call, Throwable t) {

                    }
                });

            }
        });
        return view;
    }
}