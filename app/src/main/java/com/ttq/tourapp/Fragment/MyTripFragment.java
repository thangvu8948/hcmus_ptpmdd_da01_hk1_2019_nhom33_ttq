package com.ttq.tourapp.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ttq.tourapp.Adapter.HistoryTourAdapter;
import com.ttq.tourapp.CreateTourActivity;
import com.ttq.tourapp.Dialog.MyTripOptionsDialog;
import com.ttq.tourapp.NetWork.APIClient;
import com.ttq.tourapp.NetWork.UserService;
import com.ttq.tourapp.R;
import com.ttq.tourapp.models.HistoryTour;
import com.ttq.tourapp.models.HistoryTourResponse;
import com.ttq.tourapp.models.SearchMyTripResponse;
import com.ttq.tourapp.models.SharedPrefs;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyTripFragment extends Fragment implements MyTripOptionsDialog.ListenerMyTrip {

    RecyclerView mRecyclerView;
    UserService service;
    ArrayList<HistoryTour> data;
    HistoryTourAdapter adapter;
    FloatingActionButton fab;
    Button showAllBtn;
    SearchView searchView;
    int pageIndex = 1;
    int perPage = 100;
    boolean isScrolling = false;
    boolean isSearching = false;
    ProgressBar progressBar;
    int totals = 0;
    String querySearch = "";
    public MyTripFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_my_trip, container, false);

        init(view);
        adapter = new HistoryTourAdapter(getContext(), data, getActivity(), this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        progressBar = view.findViewById(R.id.mytripProgress);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int currentItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int lastVisibleItem = layoutManager.findFirstVisibleItemPosition();
                if (isScrolling && (currentItemCount + lastVisibleItem) == totalItemCount) {
                    isScrolling = false;
                    loadMore();
                }
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                isSearching = true;
                pageIndex = 1;
                querySearch = query;
                data.clear();
                Search(query);
                adapter.notifyDataSetChanged();

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        ShowListHistoryTour();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), CreateTourActivity.class);
                startActivity(intent);
                return;
            }
        });

        showAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSearching = false;
                searchView.setQuery("", true);
                data.clear();
                pageIndex = 1;
                ShowListHistoryTour();
            }
        });
        return view;
    }


    void init(View view) {
        data = new ArrayList<>();
        showAllBtn = view.findViewById(R.id.btnShowallMytrip);
        fab = view.findViewById(R.id.fabAdd);
        searchView = view.findViewById(R.id.svSearchMyTrip);
        mRecyclerView = view.findViewById(R.id.ListMyTrip);
        service = APIClient.getInstance().getAdapter().create(UserService.class);
    }

    private void loadMore() {
        progressBar.setVisibility(View.VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pageIndex++;        // Tắng số trang
                if (isSearching) {
                    Search(querySearch);
                } else {
                    ShowListHistoryTour();

                }
            }
        }, 2000);
    }


    public void ShowListHistoryTour() {
        String token = SharedPrefs.getInstance().get("access_token", String.class);
        Call<HistoryTourResponse> ListTourRequestCall = service.getHistoryTour(token, String.valueOf(pageIndex), String.valueOf(perPage));

        ListTourRequestCall.enqueue(new Callback<HistoryTourResponse>() {
            @Override
            public void onResponse(Call<HistoryTourResponse> call, Response<HistoryTourResponse> response) {
                if (response.code() == 200) {
                    totals = response.body().getTotal();
                    ArrayList<HistoryTour> tours = response.body().getTours();

                    int total = tours.size();
                    for (int i = 0; i < total; i++) {
                        if (tours.get(i).getStatus() != -1) {
                            data.add(tours.get(i));
                        }
                    }
                    adapter.notifyDataSetChanged();
                    try {

                    } catch (Exception e) {
                        Log.d("Lisst", "onResponse: " + e);
                    }

                } else {
                    Toast.makeText(getContext(), "Lỗi. Error: " + response.code() + "\n" + response.message(), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<HistoryTourResponse> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi kết nối", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.INVISIBLE);
            }
        });

    }

    public void Search(String searchKey) {
        String token = SharedPrefs.getInstance().get("access_token", String.class);
        Call<SearchMyTripResponse> ListTourRequestCall = service.searchMyTrip(token, searchKey, pageIndex, perPage);

        ListTourRequestCall.enqueue(new Callback<SearchMyTripResponse>() {
            @Override
            public void onResponse(Call<SearchMyTripResponse> call, Response<SearchMyTripResponse> response) {
                if (response.code() == 200) {
                    totals = response.body().getTotal();
                    ArrayList<HistoryTour> tours = response.body().getTours();

                    int total = tours.size();
                    for (int i = 0; i < total; i++) {
                        if (tours.get(i).getStatus() != -1) {
                            data.add(tours.get(i));
                        }
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(), "Lỗi. Error: " + response.code() + "\n" + response.message(), Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onFailure(Call<SearchMyTripResponse> call, Throwable t) {
                Toast.makeText(getContext(), "Lỗi kết nối", Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void updateRemove(boolean isDeleted, int position) {
        if (isDeleted) {
            data.remove(position);
            adapter.notifyDataSetChanged();
        }
    }
}
