package com.ttq.tourapp.Fragment;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ttq.tourapp.ForgotPasswordActivity;
import com.ttq.tourapp.R;
import com.ttq.tourapp.ResetPasswordActivity;
import com.ttq.tourapp.interfaces.APIServices;
import com.ttq.tourapp.models.ListUserResponse;
import com.ttq.tourapp.models.Users;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FragmentForgotPasswordEmail extends Fragment {

    EditText mForgotPasswordEmail;
    Button mSearchButton;
    APIServices service;
    ArrayList<Users> list = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_forgot_password_email,container,false);
        mForgotPasswordEmail =view.findViewById(R.id.edtForgotPasswordEmail);
        mSearchButton = view.findViewById(R.id.btnSearchEmailButton);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("http://35.197.153.192:3000")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                String pageIndex = "1";
                String pageSize = "1";
                service = retrofit.create(APIServices.class);

                Call<ListUserResponse> usersCall = service.searchUsers(mForgotPasswordEmail.getText().toString(),pageIndex,pageSize);
                usersCall.request();
                usersCall.enqueue(new Callback<ListUserResponse>() {
                    @Override
                    public void onResponse(Call<ListUserResponse> call, Response<ListUserResponse> response) {
                        int statusCode=response.code();
                        if(statusCode==200){
                            //mForgotPasswordEmail.setText(""+statusCode);

                            list = response.body().getUsers();
                            String total = response.body().getTotal();
                            if(total.equals("0"))
                            {
                                mForgotPasswordEmail.setText("");
                                mForgotPasswordEmail.setHint("Không tìm thấy email.");
                            }
                            else
                            {
                                Intent intent = new Intent(view.getContext(), ResetPasswordActivity.class);
                                intent.putExtra("type","email");
                                intent.putExtra("value",list.get(0).getEmail());
                                intent.putExtra("userId",list.get(0).getId());
                                startActivity(intent);
                            }

                        }

                    }

                    @Override
                    public void onFailure(Call<ListUserResponse> call, Throwable t) {

                    }
                });
            }
        });
        return view;
    }
}
